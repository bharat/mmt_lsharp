import os
import pandas as pd
import json
import subprocess

model_names = [
    x + ".dot"
    for x in [
        "AKM",
        "CAS",
        "Light",
        "PC",
        "TCP",
        "Train",
        "example1_paper",
        "FDDI1",
        "small_Oven",
        "small_WSN",
    ]
]
expr_directory = "./benchmarks-m1mt/our-benchmarks"
learn_cmd = ["./target/release/mmt-lsharp", "-a", "-m"]
metric_strings = ["Number of OQs", "Number of WQs", "Time[ms]", "Model file"]


### Run all the experiments in our-benchmarks directory.
def learn_all_models():
    for model in model_names:
        full_path = os.path.join(expr_directory, model)
        lsharp_cmd = learn_cmd + [full_path]
        process = subprocess.Popen(lsharp_cmd)
        if process.wait() != 0:
            print("Failed to run cmd" + full_path)
            exit(-1)
    return


### Delete old log files, if any.
def delete_old_logs():
    log_dir = "./log"
    if os.path.exists(log_dir):
        for f in os.listdir(log_dir):
            if f.endswith("log"):
                full_f = os.path.join(log_dir, f)
                os.remove(full_f)
    else:
        os.makedirs([log_dir])
    return


### Helper function.
def clean_name(full_path: str) -> str:
    name_with_ext = os.path.basename(full_path)
    return name_with_ext.replace(".dot", "")


### Helper to transform the results.
def to_models_dict(all_metrics_dict):
    models_dict = {}
    for general_dict in all_metrics_dict:
        model_name = general_dict.pop("name")
        models_dict[model_name] = general_dict
    return models_dict


### Parse all the log files from the experiments.
def parse_logs():
    all_metrics = []
    for file in os.listdir("./log"):
        file = os.path.join("./log", file)
        with open(file) as f:
            metrics = f.readlines()[-1]
            (_, metrics) = metrics.split("|")
            metrics = metrics.strip()
            metrics = json.loads(metrics)
            correct_metric = {}
            for k, v in metrics.items():
                if k == "name":
                    base_name = clean_name(v)
                    correct_metric[k] = base_name
                else:
                    v = int(v)
                    correct_metric[k] = v
            all_metrics.append(correct_metric)
    metrics_by_model = to_models_dict(all_metrics)
    return metrics_by_model


if __name__ == "__main__":
    delete_old_logs()
    learn_all_models()
    print("Finished all experiments.")
    metrics = parse_logs()
    df = pd.DataFrame.from_dict(metrics, orient="index")
    df_latex = df.to_latex()

    df_lsharp = df
    df_lsharp["algorithm"] = "\lsharpMMT"
    df_lsharp = df_lsharp.reset_index()
    df_lsharp = df_lsharp.rename(columns={"index": "model", "time": "msecs"})
    df_lsharp["model"] = df_lsharp["model"].apply(lambda x: x.upper())
    df_lsharp["wait_qs"] = df_lsharp.wait_qs.astype("string")
    df_lsharp["output_qs"] = df_lsharp.output_qs.astype("string")
    cols = ["output_qs", "wait_qs"]
    result = df_lsharp
    result.sort_values(by=["model", "algorithm"], inplace=True)
    table_col_order = [
        "model",
        "states",
        "inputs",
        "timers",
        "wait_qs",
        "output_qs",
        "rounds",
        "msecs",
    ]
    result = result[table_col_order]
    col_name_dict = {
        "model": "Model",
        "wait_qs": "|WQs|",
        "output_qs": "|OQs|",
        "rounds": "|EQs|",
        "msecs": "Time[msecs]",
        "states": "|Q|",
        "inputs": "|I|",
    }
    result = result.rename(columns=col_name_dict)

    with open("all_results.tex", "w") as f:
        results_tex = result.to_latex(index=False)
        f.write(results_tex)
