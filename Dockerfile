# Setup stuff 
FROM rust:1.76

RUN apt-get -y update
RUN apt-get install -y sudo

RUN adduser --disabled-password --gecos '' sws
RUN adduser sws sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Install all the dependencies (excl python deps).
RUN sudo apt-get install -y cmake gcc python3 make build-essential git python3-pip clang

# Intsall python deps.
RUN sudo apt install -y python3-pandas

USER sws

ENV HOME /home/sws
ENV CARGO_ROOT /usr/local/cargo
ENV PATH $CARGO_ROOT/bin:$PATH

# Copy experiment files.
COPY . ${HOME}

RUN sudo chown -R sws /home/sws

# Prepare things for execution.
WORKDIR ${HOME}
RUN cargo b -r

# Execute the experiments.
RUN python3 experiments.py
RUN cat all_results.tex

