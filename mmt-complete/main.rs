use std::fs::File;

use bimap::BiHashMap;
use clap::{command, Parser};
use fnv::FnvHashMap;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol};
use mmt_lsharp_ru::timed_machine::{
    machine::{TimedMachine, TransInput},
    parser::MMTIO,
};
use rustc_hash::FxHashSet;

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::parse();
    let infile = args.infile();
    let outfile = args.outfile();
    let model_str = std::fs::read_to_string(infile)?;
    let mmtio = MMTIO::from_dot(model_str);
    let mmt_parse = mmtio.try_build();
    let mmt = mmt_parse.get_mmt();

    let input_map = mmt_parse.input_map();
    let output_map = mmt_parse.output_map();
    let timer_map = mmt_parse.timer_map();

    let new_output_map = {
        if output_map.get_by_left("void").is_some() {
            output_map.clone()
        } else {
            let new_out = output_map.len() + 1;
            let void_output = OutputSymbol::new(new_out as u16);
            let mut new_map = output_map.clone();
            new_map.insert("void".to_owned(), void_output);
            new_map
        }
    };
    let void_output = *new_output_map.get_by_left("void").expect("Safe");
    let c_mmt = complete(&mmt, input_map, void_output);

    let mut cmmt_file = File::create(outfile).expect("Could not create hypothesis file");
    c_mmt
        .write(input_map, &new_output_map, timer_map, &mut cmmt_file)
        .expect("Could not write completed MMT file.");
    Ok(())
}

#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
struct Cli {
    /// Path to file containing the original model.
    #[arg(required = true, short = 'i')]
    infile: String,
    /// Path to output file for dot.
    #[arg(required = true, short = 'o')]
    outfile: String,
}

type InternalTransInput = TransInput<InputSymbol, u16>;

fn complete(
    original: &TimedMachine,
    input_map: &BiHashMap<String, InputSymbol>,
    void_output: OutputSymbol,
) -> TimedMachine {
    let initial_state = original.initial_state();
    let all_states: FxHashSet<_> = original.active_timers().keys().copied().collect();

    let mut transitions_map = FnvHashMap::default();
    for s in &all_states {
        let existing_s_transitions = original.all_transitions_at(*s);
        let orginial_trans_iter = existing_s_transitions
            .iter()
            .map(|(i, trans)| (**i, (*trans).clone()))
            .map(|(i, trans)| ((*s, i), trans));
        transitions_map.extend(orginial_trans_iter);
        for (_, i) in input_map {
            let conc_i = InternalTransInput::Symbol(*i);
            if !transitions_map.contains_key(&(*s, conc_i)) {
                let dest = *s;
                let up = None;
                let trans = (dest, void_output, up);
                transitions_map.insert((*s, conc_i), trans);
            }
        }
    }
    let inputs = FxHashSet::default();
    let outputs = FxHashSet::default();
    let complete_mmt =
        TimedMachine::new(inputs, outputs, all_states, initial_state, transitions_map);
    complete_mmt
}
