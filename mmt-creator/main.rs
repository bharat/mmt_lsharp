use std::fs::File;
use std::io::Write;
use std::num::NonZeroUsize;
use std::str::FromStr;

use clap::Parser;
use itertools::Itertools;

use mmt_lsharp_ru::timed_machine::machine::TransInput;
use mmt_lsharp_ru::timed_machine::parser::{Transition, MMTIO};

type TransitionString = TransInput<String, String>;

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::parse();
    let infile = args.infile();
    let transitions_string = std::fs::read_to_string(infile)?;
    let (transitions, initial_state) = read_transitions_from_string(&transitions_string);
    let inputs = get_inputs(&transitions);
    let outputs = get_outputs(&transitions);
    let states = get_states(&transitions);
    let mmtio = MMTIO::new(inputs, outputs, states, initial_state, transitions);
    let mmtio_dot = mmtio.to_dot();

    let outfile = args.outfile();
    let mut outfile = File::create(outfile)?;
    outfile.write_all(mmtio_dot.as_bytes())?;
    Ok(())
}

fn get_states(transitions: &[Transition]) -> Vec<String> {
    let mut src: Vec<_> = transitions.iter().map(|tr| tr.src()).unique().collect();
    let dest_iter = transitions.iter().map(|tr| tr.dest()).unique();
    for dest in dest_iter {
        if !src.contains(&dest) {
            src.push(dest);
        }
    }
    src.into_iter().cloned().collect()
}

fn get_outputs(transitions: &[Transition]) -> Vec<String> {
    transitions
        .iter()
        .map(|tr| tr.output().clone())
        .unique()
        .collect()
}

fn get_inputs(transitions: &[Transition]) -> Vec<String> {
    transitions
        .iter()
        .filter_map(|tr| tr.get_input())
        .unique()
        .collect()
}

fn read_transitions_from_string(lines: &str) -> (Vec<Transition>, String) {
    let mut initial_state = None;
    let mut transitions = Vec::new();

    for trans_str in lines.lines() {
        // Each transition is 5 words (i.e., 4 commas)
        let mut trans_str = trans_str.split(',');
        let (src_str, dest_str, input_str, output_str, update_str) =
            trans_str.next_tuple().expect("Incorrect transition.");
        let src = src_str.trim().to_string();
        let dest = dest_str.trim().to_string();
        let output = output_str.trim().to_string();

        let input = {
            let input_str = input_str.trim().to_ascii_lowercase();
            // We first want to check if it is a symbol or a timeout.
            // We require at least 4 characters for a timeout:
            // to x
            let is_timeout = if input_str.len() >= 4 {
                "to" == &input_str[..=1]
            } else {
                false
            };
            if is_timeout {
                // It is a timeout.
                let timer = &input_str[3..];
                TransitionString::TimeOut(timer.to_string())
            } else {
                // Basic symbol, just read it in.
                TransitionString::Symbol(input_str)
            }
        };

        let update = {
            let update = update_str.trim();
            if update.is_empty() {
                None
            } else {
                let mut updates = update.split('=');

                // We must have only two words.
                let timer_str = updates.next().expect("Timer missing?");
                let dur_str = updates.next().expect("Duration missing?");
                assert!(updates.next().is_none(), "Too many parameters for update!");
                let dur = NonZeroUsize::from_str(dur_str)
                    .expect("Duration should be a positive whole number.");
                Some((timer_str.to_string(), dur))
            }
        };

        if initial_state.is_none() {
            initial_state = Some(src.clone());
        }

        transitions.push(Transition::new(src, dest, input, output, update))
    }
    let initial_state = initial_state.expect("No transitions entered?");
    (transitions, initial_state)
}

#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
struct Cli {
    /// Path to file containing the list of transitions.
    #[arg(required = true, short = 'i')]
    infile: String,
    /// Path to output file for dot.
    #[arg(required = true, short = 'o')]
    outfile: String,
}
