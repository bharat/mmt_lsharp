use std::result::Result::Ok;
use std::{fs::File, io::Write};

use bimap::BiHashMap;
use clap::Parser;
use itertools::Itertools;
use log4rs::append::console::ConsoleAppender;
use log4rs::config::Logger;
use log4rs::{
    append::file::FileAppender,
    config::{Appender, Root},
    encode::pattern::PatternEncoder,
    Config,
};
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use mmt_lsharp_ru::{
    learner::interactive::{Learner, LearnerCmds},
    obs_tree::ObservationTree,
    teachers::SymbolicTeacher,
    timed_machine::parser::MMTIO,
};
use serde::Serialize;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let start_time = std::time::Instant::now();
    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {M} | {m}{n}",
        )))
        .build(format!("log/{}.log", chrono::Utc::now()))?;
    let other_logfile = ConsoleAppender::builder().build();
    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .appender(Appender::builder().build("other-logs", Box::new(other_logfile)))
        .logger(
            Logger::builder()
                .appender("logfile")
                .additive(true)
                .build("mmt_lsharp_ru", log::LevelFilter::Debug),
        )
        .logger(
            Logger::builder()
                .appender("logfile")
                .additive(true)
                .build("mmt_lsharp", log::LevelFilter::Info),
        )
        // We do not want to log anything else, so set the root logger to Warn.
        // .logger(
        //     Logger::builder()
        //         .appender("other-logs")
        //         .additive(true)
        //         .build("z3", log::LevelFilter::Debug),
        // )
        .build(Root::builder().build(log::LevelFilter::Warn))?;
    log4rs::init_config(config)?;

    // Just reading in the arguments and parsing.
    let args = Cli::parse();
    let model_file = args.model_file();
    log::info!("Model file: {}", model_file);
    let auto = *args.auto();
    let model_str = std::fs::read_to_string(model_file)?;
    let mmtio = MMTIO::from_dot(model_str);
    let mmt_parse = mmtio.try_build();

    // We're done with parsing.
    let mmt = mmt_parse.get_mmt();
    let zone_mmt = mmt.zone_mmt();

    let input_map = mmt_parse.input_map();
    let output_map = mmt_parse.output_map();
    let timer_map = mmt_parse.timer_map();
    log::info!("Input map: {:?}", input_map);
    log::info!("Output map: {:?}", output_map);
    log::info!("Timer map: {:?}", timer_map);
    let mut zone_mmt_file = File::create("trees/zone_mmt.dot")?;
    zone_mmt.write(input_map, output_map, timer_map, &mut zone_mmt_file)?;

    let num_input_symbols = input_map.len();
    let mut observation_tree = ObservationTree::default();
    let symb_teacher = SymbolicTeacher::new(&mmt);
    let mut learner = Learner::new(&mut observation_tree, &symb_teacher, num_input_symbols);

    if auto {
        let _ = learner.learn(input_map, output_map);
        let (num_oqs, num_wqs) = learner.num_queries();
        let num_rounds = learner.num_rounds();
        log::info!("Number of OQs: {}", num_oqs);
        log::info!("Number of WQs: {}", num_wqs);
        log::info!("Number of EQs: {}", num_rounds);
        println!("Number of OQs: {}", num_oqs);
        println!("Number of WQs: {}", num_wqs);
        println!("Number of EQs: {}", num_rounds);
        let learning_time = start_time.elapsed().as_millis();
        log::info!("Time[ms]: {}", learning_time);
        let metrics = Metrics {
            name: model_file.clone(),
            output_qs: num_oqs,
            wait_qs: num_wqs,
            time: learning_time,
            rounds: num_rounds,
            inputs: input_map.len(),
            states: mmt.active_timers().len(),
            z_states: zone_mmt.active_timers().len(),
            timers: zone_mmt.active_timers().values().flatten().unique().count(),
        };
        let metrics_json = serde_json::to_string(&metrics)?;
        log::info!("{}", metrics_json);
    } else {
        run(learner, input_map, output_map);
    }
    let otree_size = observation_tree.len();
    let len_leaves = (0..otree_size)
        .into_iter()
        .map(|idx| State::new(idx as u32))
        .filter(|idx| observation_tree.all_transitions_at(*idx).is_empty())
        .map(|leaf_state| observation_tree.get_access_seq(leaf_state).len())
        .collect_vec();
    println!("All path lengths: {:?}", len_leaves);
    let total_cost: usize = len_leaves.iter().map(|x| x * x).sum();
    println!("Total est cost: {}", total_cost);
    Ok(())
}

fn run(
    mut learner: Learner<'_>,
    input_map: &BiHashMap<String, InputSymbol>,
    output_map: &BiHashMap<String, OutputSymbol>,
) {
    println!(
        "Commands are:\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n",
        OQ_CMD,
        WQ_CMD,
        BASIS_CMD,
        APART_CMD,
        WIT_CMD,
        ACT_CMD,
        HYP_CMD,
        EQ_CMD,
        QUIT_CMD,
        LEARN_CMD
    );
    let _ = std::io::stdout().flush();
    loop {
        let mut buf = String::new();
        std::io::stdin().read_line(&mut buf).expect("Safe");
        if let Ok(cmd) = LearnerCmds::try_from(buf) {
            learner.run(cmd, input_map, output_map);
        } else {
            println!("Could not parse command. Try again.");
            let _ = std::io::stdout().flush();
        }
    }
}

#[derive(Parser, derive_getters::Getters)]
#[command(author, version, about)]
struct Cli {
    /// Path to file containing the list of transitions.
    #[arg(required = true, short = 'm')]
    model_file: String,
    /// Run in automatic learning mode.
    #[arg(required = false, short = 'a')]
    auto: bool,
}

#[derive(Serialize)]
struct Metrics {
    name: String,
    output_qs: usize,
    wait_qs: usize,
    time: u128,
    rounds: usize,
    inputs: usize,
    states: usize,
    z_states: usize,
    timers: usize,
}

const OQ_CMD: &str = "oq t<state-id> <input-symbol>";
const ACT_CMD: &str = "act t<state-id>";
const WQ_CMD: &str = "wq t<state-id>";
const EQ_CMD: &str = "eq";
const HYP_CMD: &str = "hyp";
const BASIS_CMD: &str = "basis";
const LEARN_CMD: &str = "learn";
const APART_CMD: &str = "apart";
const WIT_CMD: &str = "wit t<higher-state-id> t<lower-state-id>";
const QUIT_CMD: &str = "q[uit]";
