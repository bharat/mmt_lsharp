# L# Learning Library for Mealy Machines with Timers

## Building
We require the latest version of stable Rust (currently 1.76), although older versions may work.
Additionally, we require Python 3.10+ with the pandas package.
You can compile the code with `cargo b -r` to build the release version.
Note, compilation may take a while, as we compile the Z3 SMT solver too (5 minutes on our machine).
The experiments have been performed on an AMD Ryzen 3700X (8 cores) with 32GB of memory running Linux.
You can use the included Dockerfile to run the experiments.

## Using the Dockerfile
Ensure that you have Docker installed on your system, and then from a terminal with the project root
as the current directory, run
```bash
# Build and run the experiments
docker build -t lsharp_mmt_img . 
# Access the image
docker run -i -t lsharp_mmt_img:latest
# Get the results
cat all_results.tex
```
The experiment result table (minus some formatting/labeling) is exported as `all_results.tex` in 
the current directory.
And that's it!

## Running Benchmark Experiments
You can run the benchmark experiments using the provided `experiments.py` script.
Assuming you are in the root directory of the project (the directory with `Cargo.toml` in it):
```bash
# Build the project 
cargo b -r
# Run the experiments
python3 experiments.py 
```
The experiment result table (minus some formatting/labeling) is exported as `all_results.tex` in 
the current directory.


## Developer Information
Using one of the example DOT models under the ''tests'' directory, 
the executable can be run on the terminal as follows:

```./target/release/mmt-lsharp -m ./tests/apart_ex3.dot```

Each command that is run updates the current observation tree, which is located at `trees/current.dot`.
In the initial run, the observation tree file is missing, however, *the root node is always `t0`*, so you can run an output query to generate the tree file.

## Licensing
The Oven and WSN models are under AGPL v3 (see the [MMLT Learning repository](https://git.tu-berlin.de/pkogel/mmlt-learning)).
For our code, you can pick MIT or Apache-2.0.
