use derive_getters::{Dissolve, Getters};
use derive_more::From;
use std::num::NonZeroUsize;

use fraction::Fraction;

#[derive(Debug, PartialEq, Eq, Clone, Dissolve, Getters)]
pub struct Update {
    id: u16,
    value: NonZeroUsize,
}

impl Update {
    pub fn new(id: u16, value: NonZeroUsize) -> Self {
        Self { id, value }
    }
}

pub(crate) fn both_updates_exist_and_are_different(
    update_1: Option<&Update>,
    update_2: Option<&Update>,
) -> bool {
    let cons = |u: &Update| *u.value();
    match (update_1, update_2) {
        (Some(x), Some(y)) => cons(x) != cons(y),
        (_, _) => false,
    }
}

/// A basic timer.
///
#[derive(Debug, PartialEq, Eq, From, Clone, Getters)]
pub struct Timer {
    #[getter(rename = "remaining")]
    count: Fraction,
}

impl From<NonZeroUsize> for Timer {
    fn from(value: NonZeroUsize) -> Self {
        Self::new(value)
    }
}

#[derive(Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub enum TimeLeft {
    Zero,
    #[default]
    Pos,
}

impl Timer {
    pub fn new(duration: NonZeroUsize) -> Self {
        let count: usize = duration.into();
        Timer {
            count: count.into(),
        }
    }

    /// Check if the timer is zero.
    /// # Panics
    /// Never.
    pub fn must_timeout(&self) -> bool {
        let num = *self
            .count
            .numer()
            .expect("We never let a numerator not exist.");
        0 == num
    }

    /// Check if the timer can tick for `time` without timing out.
    pub fn can_tick_by<T: Into<Fraction>>(&self, time: T) -> TimeLeft {
        let timer = self.count;
        let time_left = timer > time.into();
        if time_left {
            TimeLeft::Pos
        } else {
            TimeLeft::Zero
        }
    }

    /// Tick the timer for `duration`.
    pub fn tick_by<T: Into<Fraction>>(&mut self, duration: T) {
        let dur = duration.into();
        self.count -= dur;
    }

    /// We can only update timers with whole numbers.
    /// We can never update it with zero either, but for now I'll ignore that.
    pub fn update(&mut self, dur: usize) {
        self.count = dur.into();
    }
}
