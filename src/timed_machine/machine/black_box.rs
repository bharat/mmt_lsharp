use std::fmt::Display;

use derive_more::{Constructor, From, IsVariant};
use fraction::Fraction;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use rustc_hash::FxHashMap;

use crate::timed_machine::timers::{TimeLeft, Timer};

use super::{InternalTransInput, TimedMachine, TransInput};

#[derive(Debug, IsVariant, Clone, PartialEq, Eq)]
pub enum Output {
    Base(OutputSymbol),
    Delay(Fraction),
}

impl Display for Output {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Output::Base(o) => {
                write!(f, "O{}", o.raw())
            }
            Output::Delay(d) => {
                write!(f, "D({})", d)
            }
        }
    }
}

#[derive(Debug, IsVariant, Clone, PartialEq, Eq)]
pub enum Input {
    Base(InputSymbol),
    Delay(Fraction),
}

impl Display for Input {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Base(i) => {
                write!(f, "I{}", i.raw())
            }
            Self::Delay(d) => {
                write!(f, "D({})", d)
            }
        }
    }
}

impl From<(u16, u16)> for Input {
    fn from((num, den): (u16, u16)) -> Self {
        let frac = Fraction::new(num, den);
        Self::Delay(frac)
    }
}

impl From<InputSymbol> for Input {
    fn from(i: InputSymbol) -> Self {
        Self::Base(i)
    }
}

impl TimedMachine {
    /// Take a single step in the MMT.
    fn step(
        &self,
        input: InternalTransInput,
        mut src: Configuration,
    ) -> (OutputSymbol, Configuration) {
        let trans = self.transitions.get(&(src.state, input)).expect("Safe");
        let (dest, out, update) = trans;
        src.state = *dest;
        if let Some(update) = update {
            let (id, timer) = {
                let (id, dur) = update.clone().dissolve();
                (id, Timer::from(dur))
            };
            src.timers.insert(id, timer);
        }
        (*out, src)
    }

    /// Run `timed_input_word` over the MMT and get the response.
    pub fn trace(&self, timed_input_word: &TimedInputWord) -> TimedOutputWord {
        // Let's gogogogogogogo!
        let mut curr_config = Configuration::new(self.initial_state);
        let mut outputs = Vec::new();
        let mut out;
        for i in timed_input_word {
            match i {
                Input::Base(i) => {
                    // For a simple input symbol, we must first check if there is
                    // any timeout. If there isn't, we can directly do the transition and updates.
                    if let Some(tim_id) = curr_config.any_timeout() {
                        (out, curr_config) = self.step(TransInput::TimeOut(tim_id), curr_config);
                        outputs.push(Output::Base(out));
                    }
                    (out, curr_config) = self.step(TransInput::Symbol(*i), curr_config);
                    outputs.push(Output::Base(out));
                }
                Input::Delay(dur) => {
                    let mut dur = *dur;
                    // We must now make sure that the *entire* delay is consumed,
                    // unlike the case for a simple input i. So we loop and break
                    // only when `dur` is zero.
                    loop {
                        if let Some(tim_id) = curr_config.any_timeout() {
                            (out, curr_config) =
                                self.step(TransInput::TimeOut(tim_id), curr_config);
                            outputs.push(Output::Base(out));
                        }
                        // We have no timers, just exit the loop.
                        if curr_config.timers.is_empty() {
                            outputs.push(Output::Delay(dur));
                            break;
                        }
                        if curr_config.can_delay(dur) {
                            curr_config.tick_by(dur);
                            outputs.push(Output::Delay(dur));
                            break;
                        } else {
                            let tim = curr_config
                                .timers
                                .iter()
                                .find(|(_, tim)| tim.can_tick_by(dur) == TimeLeft::Zero)
                                .map(|(id, tim)| (*id, tim.clone()))
                                .expect("Safe");
                            let time_spent = tim.1.remaining();
                            curr_config.timers.remove(&tim.0);
                            curr_config.tick_by(*time_spent);
                            outputs.push(Output::Delay(*time_spent));
                            (out, curr_config) = self.step(TransInput::TimeOut(tim.0), curr_config);
                            outputs.push(Output::Base(out));
                            dur -= time_spent;
                        }
                        if !dur.is_normal() {
                            break;
                        }
                    }
                }
            }
        }
        outputs.into()
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Constructor, From)]
pub struct TimedInputWord {
    word: Vec<Input>,
}

impl<const N: usize> From<[Input; N]> for TimedInputWord {
    fn from(value: [Input; N]) -> Self {
        Self::from(value.to_vec())
    }
}

impl IntoIterator for TimedInputWord {
    type Item = Input;

    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.word.into_iter()
    }
}

impl<'a> IntoIterator for &'a TimedInputWord {
    type Item = &'a Input;

    type IntoIter = std::slice::Iter<'a, Input>;

    fn into_iter(self) -> Self::IntoIter {
        self.word.as_slice().iter()
    }
}

#[derive(From, PartialEq, Eq, Debug)]
pub struct TimedOutputWord {
    word: Vec<Output>,
}

impl Display for TimedInputWord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let out_word = self.word.iter().map(|x| x.to_string()).join(",");
        write!(f, "[{}]", out_word)
    }
}
impl Display for TimedOutputWord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let out_word = self.word.iter().map(|x| x.to_string()).join(",");
        write!(f, "[{}]", out_word)
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Configuration {
    state: State,
    timers: FxHashMap<u16, Timer>,
}

impl Configuration {
    fn new(state: State) -> Self {
        Self {
            state,
            timers: FxHashMap::default(),
        }
    }

    fn can_delay(&self, dur: Fraction) -> bool {
        let mut can_delay = true;
        for timer in self.timers.values() {
            if !can_delay {
                break;
            }
            let is_time_left = timer.can_tick_by(dur);
            let is_zero = is_time_left == TimeLeft::Zero;
            if is_zero {
                can_delay = false;
            }
        }
        can_delay
    }

    /// Returns the `id` of the timer which timed out.
    fn any_timeout(&mut self) -> Option<u16> {
        let timeout_timers = self.timers.iter().filter(|(_, tim)| tim.must_timeout());
        let tim = timeout_timers
            .at_most_one()
            .expect("Multiple timers timed out.");
        let tim = tim.map(|(id, _)| *id);
        if let Some(id) = tim {
            self.timers.remove(&id);
        }
        tim
    }

    /// Tick all timers by `dur`.
    fn tick_by(&mut self, dur: Fraction) {
        for tim in self.timers.values_mut() {
            tim.tick_by(dur);
        }
    }
}
