use bimap::BiHashMap;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use thiserror::Error;

use crate::timed_machine::timers::Update;

use super::{TimedMachine, TransInput};

#[derive(Debug, Error)]
pub enum WriteError {
    #[error("Missing input symbol {0} in input_map.")]
    MissingInput(InputSymbol),
    #[error("Missing timer id {0} in timer_map.")]
    MissingTimer(usize),
    #[error("Missing output symbol {0} in output_map.")]
    MissingOutput(OutputSymbol),
    #[error("Could not write to writer `f`.")]
    WriteErr,
}

impl TimedMachine {
    pub fn write<W: std::io::Write>(
        &self,
        input_map: &BiHashMap<String, InputSymbol>,
        output_map: &BiHashMap<String, OutputSymbol>,
        timer_map: &BiHashMap<String, usize>,
        f: &mut W,
    ) -> Result<(), WriteError> {
        let states = self.states.iter().sorted().copied();

        let digraph_decl = "digraph g {".to_string();
        let start_node = "__start0 [label=\"\" shape=\"none\"];".to_string();
        let node_decl = "node [margin=0 width=0.5 shape=circle];".to_string();
        let initial_decls = vec![digraph_decl, start_node, node_decl];

        let mut all_transitions = Vec::new();
        for s in states {
            let s_trans = self.state_transitions(s, input_map, output_map, timer_map)?;
            all_transitions.push(s_trans);
        }
        let all_transitions = all_transitions.join("\n");
        let initial_state_decl = format!("__start0 -> {};", self.initial_state);
        let digraph_end = "}".to_string();
        let final_decls = vec![initial_state_decl, digraph_end];
        let machine_str = initial_decls
            .into_iter()
            .chain([all_transitions])
            .chain(final_decls)
            .join("\n");

        write!(f, "{machine_str}").map_err(|_x| WriteError::WriteErr)
    }

    fn state_transitions(
        &self,
        s: State,
        input_map: &BiHashMap<String, InputSymbol>,
        output_map: &BiHashMap<String, OutputSymbol>,
        timer_map: &BiHashMap<String, usize>,
    ) -> Result<String, WriteError> {
        let src = s.to_string();

        let mut string_transitions = Vec::new();
        for (i, (dest, out, up)) in self.all_transitions_at(s) {
            let i_string = input_string(input_map, timer_map, *i)?;
            let o_string = output_string(output_map, *out)?;
            let dest = dest.to_string();
            let up = update_string(timer_map, up.as_ref())?;
            let trans = format!("{src} -> {dest} [label=\"{i_string} / {o_string} / {up}\"];");
            string_transitions.push(trans);
        }
        Ok(string_transitions.join("\n"))
    }
}

fn update_string(
    timer_map: &BiHashMap<String, usize>,
    up: Option<&Update>,
) -> Result<String, WriteError> {
    if let Some(up) = up {
        let id = timer_map
            .get_by_right(&(*up.id() as usize))
            .ok_or(WriteError::MissingTimer((*up.id()).into()))?;
        let val = *up.value();
        Ok(format!("{id} := {val}"))
    } else {
        Ok(String::from("⊥"))
    }
}

fn output_string(
    output_map: &BiHashMap<String, OutputSymbol>,
    out: OutputSymbol,
) -> Result<String, WriteError> {
    output_map
        .get_by_right(&out)
        .ok_or(WriteError::MissingOutput(out))
        .cloned()
}

fn input_string(
    input_map: &BiHashMap<String, InputSymbol>,
    timer_map: &BiHashMap<String, usize>,
    i: TransInput<InputSymbol, u16>,
) -> Result<String, WriteError> {
    if i.is_symbol() {
        let i = i.unwrap_symbol();
        input_map
            .get_by_right(&i)
            .ok_or(WriteError::MissingInput(i))
            .cloned()
    } else {
        let i = i.unwrap_time_out().into();
        timer_map
            .get_by_right(&i)
            .ok_or(WriteError::MissingTimer(i))
            .map(|x| format!("to[{x}]"))
    }
}
