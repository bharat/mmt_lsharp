use std::collections::VecDeque;
use std::num::NonZeroUsize;

use bimap::BiHashMap;
use derive_more::{Constructor, Display, IsVariant, Unwrap};
use fnv::FnvHashMap;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use rustc_hash::{FxHashMap, FxHashSet};
use serde::{Deserialize, Serialize};
use z3::ast::{self, Ast};
use z3::{Config, Context, Solver};

use crate::teachers::SymInput;
use crate::timed_machine::zones::{Constraint, Expr, Term};

use super::timers::Update;
use super::zones::Zone;

/// Black-box implementations for OQs and WQs of the MMT.
mod black_box;
mod writer;

type TransitionProperty = (State, OutputSymbol, Option<Update>);
type InternalTransInput = TransInput<InputSymbol, u16>; // Inputsymbol and timer id.

#[derive(Debug, Constructor, Clone)]
pub struct TimedMachine {
    _inputs: FxHashSet<InputSymbol>,
    _outputs: FxHashSet<OutputSymbol>,
    states: FxHashSet<State>,
    initial_state: State,
    // We do not need to store the updates themselves, as we can just get all the updates from the
    // transitions field.
    transitions: FnvHashMap<(State, InternalTransInput), TransitionProperty>,
}

impl TimedMachine {
    pub fn initial_state(&self) -> State {
        self.initial_state
    }

    /// A symbolic output query with access sequence `seq` and next input `input`.
    pub(crate) fn symbolic_output_query(&self, seq: &[SymInput], input: SymInput) -> OutputSymbol {
        let mut idxs_with_timer_ids = FxHashMap::default();
        let mut current_state = self.initial_state;
        let mut output = OutputSymbol::new(u16::MAX);
        for (i, idx) in seq.iter().chain([&input]).zip(1usize..) {
            // We can either have a base symbol or a timeout.
            // If we have a base symbol, we just keep moving forward.
            match i {
                SymInput::Sym(i) => {
                    let (dest_state, out, update) = self
                        .transitions
                        .get(&(current_state, TransInput::Symbol(*i)))
                        .expect("TimedMachine is incomplete.");
                    current_state = *dest_state;
                    output = *out;
                    if let Some(update) = update {
                        idxs_with_timer_ids.insert(idx, *update.id());
                    }
                }
                SymInput::TimeOut(x) => {
                    // Which timer went off?
                    let x = *x as usize;
                    let tim_id = *idxs_with_timer_ids.get(&x).expect(TIMER_NOT_SET);
                    let (dest_state, out, update) = self
                        .transitions
                        .get(&(current_state, TransInput::TimeOut(tim_id)))
                        .expect("TimedMachine is incomplete.");
                    output = *out;
                    current_state = *dest_state;
                    if let Some(update) = update {
                        idxs_with_timer_ids.insert(idx, *update.id());
                    }
                }
            }
        }
        assert_ne!(output.raw(), u16::MAX, "Symbolic OQ did not run.");
        output
    }

    /// We return a vector of timers that can go off.
    ///
    /// Each tuple is a (transition_idx, val):
    /// 1. transition_idx is which transition started a timer, and
    /// 2. val is the value of the timer.
    pub(crate) fn symbolic_wait_query(&self, seq: &[SymInput]) -> Vec<(usize, NonZeroUsize)> {
        //TODO: Eventually, this code should reuse items from the zone construction.
        // ----------------- Z3 stuff --------------------- //
        let mut cfg = Config::new();
        cfg.set_model_generation(true);
        let ctx = Context::new(&cfg);
        let solver = Solver::new(&ctx);

        let mut delay_z3ids_map = FxHashMap::default();
        // Indexing begins at 1 and ends with the length of the sequence.
        for d_id in 1..=seq.len() {
            let var = ast::Real::new_const(&ctx, format!("d{}", d_id));
            delay_z3ids_map.insert(d_id, var);
        }
        let delay_z3ids_map = delay_z3ids_map;
        let zero = ast::Real::from_real(&ctx, 0, 1);
        for delay in delay_z3ids_map.values() {
            let delays_non_neg = delay.ge(&zero);
            solver.assert(&delays_non_neg);
        }
        // ----------------- End Z3 stuff ----------------- //

        // This keeps track of the *latest* transition to set a timer.
        // We forget about the previous transitions, so we can simply
        // not bother remembering which one is redundant/wrong.
        // Transition idx -> Timer id
        let mut idxs_with_timer_ids = BiHashMap::new();
        // Timer id -> value of the timer.
        let mut update_map = FxHashMap::<u16, NonZeroUsize>::default();

        let mut current_state = self.initial_state;
        let mut outputs = Vec::new();
        for (idx, i) in (1usize..).zip(seq) {
            match i {
                SymInput::Sym(i_symb) => {
                    // We need to take a basic input transition.
                    // First, we need to disable all the timeouts in the current state.
                    let timer_ids = self
                        .transitions
                        .keys()
                        .filter(|k| k.0 == current_state)
                        .map(|k| &k.1)
                        .filter(|input| input.is_time_out())
                        .map(|x| (*x).unwrap_time_out())
                        .collect_vec();
                    // In which transition was each of these timers set?
                    let timer_set_idxs: Vec<_> = timer_ids
                        .iter()
                        .filter_map(|id| Some(id).zip(idxs_with_timer_ids.get_by_right(id)))
                        .collect();
                    // We now have (timer_id, idx_set) pairs for timers which were set.
                    // The value of the delays must sum up to less than the value set
                    // by the timer.
                    // Vec of triples : (id-of-timer, index-of-transition, value-of-timer)
                    let id_index_value: Vec<_> = timer_set_idxs
                        .into_iter()
                        .map(|(id, index)| {
                            let val = update_map.get(id).expect(TIMER_NOT_SET);
                            (*id, *index, *val)
                        })
                        .collect();

                    // We can add delays from the idx directly after transition which
                    // updated the timer to the delay just before the current transition.
                    for (_tim_id, index, tim_val) in id_index_value {
                        let sum_delays = (index..idx)
                            .filter_map(|di| delay_z3ids_map.get(&di))
                            .collect_vec();
                        if !sum_delays.is_empty() {
                            let d_sum = ast::Real::add(&ctx, &sum_delays);
                            let val = ast::Real::from_real(&ctx, (tim_val.get()) as i32, 1);
                            let constraint = ast::Real::lt(&d_sum, &val);
                            solver.assert(&constraint);
                        }
                    }

                    // We have an input symbol, so we must take that transition and
                    // store the update we have, if any.
                    let (dest_state, output, update) = {
                        let k = (current_state, TransInput::Symbol(*i_symb));
                        self.transitions.get(&k).expect(MACHINE_INCOMPLETE)
                    };
                    current_state = *dest_state;
                    outputs.push(*output);
                    // Any constraints that must be done before we reach the
                    // destination state for the current transition must be
                    // added before we finish inserting the update into the
                    // BiMap.
                    if let Some(update) = update {
                        idxs_with_timer_ids.insert(idx, *update.id());
                        let val = *update.value();
                        update_map.insert(*update.id(), val);
                    }
                }
                SymInput::TimeOut(timeout_idx) => {
                    // In order to take a timeout of the transition at
                    // `timeout_idx`, we need to check *which* timer was set
                    // at this index.
                    let timeout_idx = *timeout_idx as usize;
                    let timer_to_timeout = *idxs_with_timer_ids
                        .get_by_left(&timeout_idx)
                        .expect("Timer was not set from idx.");
                    let val_timer = *update_map
                        .get(&timer_to_timeout)
                        .expect("Timer was not set.");

                    // Next, we have to ensure that the sum of delays from
                    // the moment that the `timer_to_timeout` was set is equal
                    // to the value of the timer.
                    let sum_delays = (timeout_idx..idx)
                        // let sum_delays = (timeout_idx + 1..=idx)
                        .filter_map(|di| delay_z3ids_map.get(&di))
                        .collect_vec();
                    if !sum_delays.is_empty() {
                        let sum_delays = ast::Real::add(&ctx, &sum_delays);
                        let val = ast::Real::from_real(&ctx, (val_timer.get()) as i32, 1);
                        let val_eq_sum_delays = val._eq(&sum_delays);
                        solver.assert(&val_eq_sum_delays);
                    }

                    let (dest_state, output, update) = {
                        let k = (current_state, TransInput::TimeOut(timer_to_timeout));
                        self.transitions.get(&k).expect(MACHINE_INCOMPLETE)
                    };
                    current_state = *dest_state;
                    outputs.push(*output);
                    if let Some(update) = update {
                        idxs_with_timer_ids.insert(idx, *update.id());
                        let val = *update.value();
                        update_map.insert(*update.id(), val);
                    } else {
                        // timer was not restarted, throw it out.
                        update_map.remove(&timer_to_timeout);
                        idxs_with_timer_ids.remove_by_right(&timer_to_timeout);
                    }
                }
            }
        }
        // Now, we have to check which timers can go off given the previous constraints.
        let timer_candidates = self
            .transitions
            .keys()
            .filter(|k| k.0 == current_state)
            .map(|k| &k.1)
            .filter(|input| input.is_time_out())
            .map(|x| x.unwrap_time_out())
            .collect_vec();

        // For each timer, we want to know *when* it was set and
        // the *value* of the timer.
        let idx_id_val_s = timer_candidates
            .into_iter()
            .filter_map(|id| {
                let idx = idxs_with_timer_ids.get_by_right(&id).copied();
                idx.zip(Some(id))
            })
            .map(|(idx, id)| {
                let val = *update_map.get(&id).expect(TIMER_NOT_SET);
                (idx, id, val)
            })
            .collect_vec();
        let mut can_timeout = Vec::new();
        for current_timer_details in &idx_id_val_s {
            solver.push();
            let (idx, _, val) = *current_timer_details;
            // For each timer, we must check with the previous constraints
            // the following two conditions:
            // 1. The sum of delays for the selected timer must be equal to its value,
            // 2. AND, the other timers must not go off before our timer.
            let sum_delays = (idx..=seq.len())
                .filter_map(|di| delay_z3ids_map.get(&di))
                .collect_vec();
            if !sum_delays.is_empty() {
                let sum_delays = ast::Real::add(&ctx, &sum_delays);
                let z3_val = ast::Real::from_real(&ctx, (val.get()) as i32, 1);
                let val_eq_sum_delays = z3_val._eq(&sum_delays);
                solver.assert(&val_eq_sum_delays);
            }
            // Condition 2.
            // Get the sum of delays for the other timers.
            {
                let other_timers = idx_id_val_s
                    .iter()
                    .filter(|x| *x != current_timer_details)
                    .collect_vec();
                for other_timer in other_timers {
                    let (idx, _, val) = other_timer;
                    let (idx, val) = (*idx, *val);
                    let sum_delays = (idx..=seq.len())
                        .filter_map(|di| delay_z3ids_map.get(&di))
                        .collect_vec();
                    if !sum_delays.is_empty() {
                        let sum_delays = ast::Real::add(&ctx, &sum_delays);
                        let z3_val = ast::Real::from_real(&ctx, (val.get()) as i32, 1);
                        let val_eq_sum_delays = z3_val.gt(&sum_delays);
                        solver.assert(&val_eq_sum_delays);
                    }
                }
            }
            match solver.check() {
                z3::SatResult::Unsat => {}
                z3::SatResult::Unknown => unreachable!("Z3 gave an error!"),
                z3::SatResult::Sat => {
                    let pair = (idx, val);
                    can_timeout.push(pair)
                }
            }
            solver.pop(1);
        }
        can_timeout
    }
}

impl TimedMachine {
    /// Compute the map of active timers per state in the machine.
    pub fn active_timers(&self) -> FxHashMap<State, FxHashSet<u16>> {
        let states = &self.states;
        let all_transitions: FxHashMap<_, _> = states
            .iter()
            .map(|s| (*s, self.all_transitions_at(*s)))
            .collect();
        let mut curr_timers: FxHashMap<_, _> =
            states.iter().map(|s| (*s, FxHashSet::default())).collect();
        loop {
            let mut new_timers = FxHashMap::default();
            for &s in states {
                if s == self.initial_state {
                    new_timers.insert(s, FxHashSet::default());
                    continue;
                }

                let s_trans = all_transitions.get(&s).expect("Safe");
                let mut s_timers: FxHashSet<_> = s_trans
                    .keys()
                    .filter(|i| i.is_time_out())
                    .map(|i| i.unwrap_time_out())
                    .collect();

                for (i, (t, _, up)) in s_trans {
                    if let Some(up) = up {
                        let t_active = curr_timers.get(t).expect("Safe");
                        s_timers.extend(t_active.iter().filter(|x| **x != *up.id()));
                    } else {
                        let t_active = curr_timers.get(t).expect("Safe");
                        if i.is_time_out() {
                            let tim_id = i.unwrap_time_out();
                            let err_str = "Extra timeout in set of active timers!";
                            assert!(!t_active.contains(&tim_id), "{}", err_str);
                        }
                        s_timers.extend(t_active.iter());
                    }
                }
                new_timers.insert(s, s_timers);
            }
            if new_timers == curr_timers {
                break;
            }
            curr_timers = new_timers;
        }
        curr_timers
    }

    pub fn all_transitions_at(
        &self,
        q: State,
    ) -> FxHashMap<&TransInput<InputSymbol, u16>, &(State, OutputSymbol, Option<Update>)> {
        let transitions = self
            .transitions
            .iter()
            .filter(|((s, _), _)| *s == q)
            .map(|((_, i), prop)| (i, prop))
            .collect();
        transitions
    }

    /// Construct the zone MMT of this machine.
    pub fn zone_mmt(&self) -> Self {
        let active_timers = self.active_timers();
        let basic_inputs: Vec<_> = self
            .transitions
            .keys()
            .map(|(_, i)| *i)
            .filter(|i| i.is_symbol())
            .unique()
            .collect();
        // Map to store transitions of the Z-MMT.
        let mut z_transitions: FnvHashMap<
            (State, InternalTransInput),
            (State, OutputSymbol, Option<Update>),
        > = FnvHashMap::default();

        // Map to store all current access sequences in the Z-MMT.
        let mut access_seq_map: FxHashMap<State, Vec<InternalTransInput>> = FxHashMap::default();
        // Initial state has empty access sequence.
        access_seq_map.insert(State::new(0), vec![]);
        // Zone map
        let mut zone_map: FxHashMap<State, Zone> = FxHashMap::default();
        zone_map.insert(State::new(0), Zone::initial_state());
        // We map states of the z-mmt to the original MMT using this map.
        let mut zmmt_to_mmt_map: FxHashMap<_, _> =
            [(State::new(0), State::new(0))].into_iter().collect();
        // Any new state added to the Z-MMT will be assigned this id.
        let mut curr_zmmt_sid = 1;

        let mut work_list = VecDeque::from([State::new(0)]);
        while let Some(curr_state_z) = work_list.pop_front() {
            let access_z = access_seq_map.get(&curr_state_z).expect("Safe").clone();
            let sym_access_z = self.to_symbolic_sequence(&access_z);
            let tim_ids_to_explore = {
                let tr_idxs_of_timers: FxHashSet<_> = self
                    .symbolic_wait_query(&sym_access_z)
                    .into_iter()
                    .map(|(tr_id, _)| tr_id)
                    .collect();
                self.get_timer_ids_from_timer_indexes(&access_z, &tr_idxs_of_timers)
            };
            let unexplored_inputs = {
                let mut inputs = Vec::new();
                // If the timeouts exist in the map, we don't want to explore them.
                let all_inputs_iter = basic_inputs.clone().into_iter().chain(
                    tim_ids_to_explore
                        .into_iter()
                        .map(InternalTransInput::TimeOut),
                );
                for i in all_inputs_iter {
                    if !z_transitions.contains_key(&(curr_state_z, i)) {
                        inputs.push(i);
                    }
                }
                inputs
            };

            for &i in &unexplored_inputs {
                let new_seq = {
                    let mut seq = access_z.clone();
                    seq.push(i);
                    seq
                };
                let mmt_dest = self.walk_along(&new_seq);
                let new_seq_zone =
                    self.zone_of(&new_seq, active_timers.get(&mmt_dest).expect("Safe"));
                let possible_cand = zmmt_to_mmt_map
                    .iter()
                    .filter(|(_, mmt_s)| **mmt_s == mmt_dest)
                    .map(|(zs, _)| zs)
                    .map(|zs| (*zs, zone_map.get(zs).expect("Safe")))
                    .filter(|(_, zone_s)| new_seq_zone.equivalent_to(zone_s))
                    .map(|(zs, _)| zs)
                    .at_most_one()
                    .expect("We have multiple states in the Z-MMT with the same zone.");
                let zmmt_dest = if let Some(x) = possible_cand {
                    x
                } else {
                    let x = State::new(curr_zmmt_sid);
                    curr_zmmt_sid += 1;
                    let mut new_access_seq = access_z.clone();
                    new_access_seq.push(i);
                    access_seq_map.insert(x, new_access_seq);
                    x
                };
                zmmt_to_mmt_map.insert(zmmt_dest, mmt_dest);
                let trans_prop = {
                    let src = self.walk_along(&access_z);
                    let trans_prop = self.all_transitions_at(src).get(&i).cloned();
                    let (_, out, up) = trans_prop.expect("Safe").clone();
                    (zmmt_dest, out, up)
                };
                z_transitions.insert((curr_state_z, i), trans_prop);
                if zone_map.get(&zmmt_dest).is_none() {
                    zone_map.insert(zmmt_dest, new_seq_zone);
                    work_list.push_back(zmmt_dest);
                }
            }

            let zmmt_to_mmt_keys: FxHashSet<_> = zmmt_to_mmt_map.keys().collect();
            let zmmt_to_zone_keys: FxHashSet<_> = zone_map.keys().collect();
            assert_eq!(zmmt_to_mmt_keys, zmmt_to_zone_keys);
        }
        let states = z_transitions.keys().map(|(s, _)| s).copied().collect();
        let mut mmt = Self {
            states,
            initial_state: State::new(0),
            transitions: z_transitions,
            _inputs: FxHashSet::default(),
            _outputs: FxHashSet::default(),
        };
        mmt.remove_erroneous_updates();
        mmt
    }

    /// Remove invalid timer updates from the ZMMT.
    fn remove_erroneous_updates(&mut self) {
        // First, active timers of all states.
        let active_timers = self.active_timers();

        // We don't care about the source, input, or output.
        for ((_, _), (dest, _, update)) in &mut self.transitions {
            // We need to change the update to None if the dest state doesn't have the
            // timer (re)started in the update as active.
            if let Some(tim_id) = update.as_ref().map(|up| up.id()) {
                if !active_timers.get(dest).expect("Safe").contains(tim_id) {
                    *update = None;
                }
            }
        }
    }

    fn get_timer_ids_from_timer_indexes(
        &self,
        input_seq: &[InternalTransInput],
        tr_idxs: &FxHashSet<usize>,
    ) -> Vec<u16> {
        let mut timer_ids = Vec::new();
        let mut curr_state = self.initial_state;
        let id_iter = 1..;
        for (i, idx) in input_seq.iter().zip(id_iter) {
            let (dest, _, update) = self.transitions.get(&(curr_state, *i)).expect("Safe");
            if tr_idxs.contains(&idx) {
                let up = update.as_ref().expect("Safe");
                let tim_id = *up.id();
                timer_ids.push(tim_id);
            }
            curr_state = *dest;
        }
        timer_ids
    }

    /// Construct the zone (from `Constraint`) reachable from the concrete sequence.
    fn zone_of(&self, conc_seq: &[InternalTransInput], active_timers: &FxHashSet<u16>) -> Zone {
        let sym_acc = self.to_symbolic_sequence(conc_seq);
        let mut constraints = Vec::new();

        // This keeps track of the *latest* transition to set a timer.
        // We forget about the previous transitions, so we can simply
        // not bother remembering which one is redundant/wrong.
        // Transition idx -> Timer id
        let mut idxs_with_timer_ids = BiHashMap::new();
        // Timer id -> value of the timer.
        let mut update_map = FxHashMap::<u16, NonZeroUsize>::default();

        let mut current_state = self.initial_state;
        let mut outputs = Vec::new();
        for (idx, i) in (1usize..).zip(&sym_acc) {
            match i {
                SymInput::Sym(i_symb) => {
                    // We need to take a basic input transition.
                    // First, we need to disable all the timeouts in the current state.
                    let timer_index_val_s = self
                        .all_transitions_at(current_state)
                        .keys()
                        .filter(|input| input.is_time_out())
                        // Get all timeouts of the current state
                        .map(|x| (*x).unwrap_time_out())
                        // When where the timers for these timeouts set and what are their values?
                        .map(|id| {
                            let index = idxs_with_timer_ids.get_by_right(&id).expect(TIMER_NOT_SET);
                            let val = update_map.get(&id).expect(TIMER_NOT_SET);
                            (id, *index, *val)
                        })
                        .collect_vec();
                    // We can add delays from the idx directly after transition which
                    // updated the timer to the delay just before the current transition.
                    for (_tim_id, index, tim_val) in timer_index_val_s {
                        let sum_delays = (index..idx).map(|x| Term::Delay(x as u16)).collect_vec();
                        if !sum_delays.is_empty() {
                            let d_sum = Expr::ADD(sum_delays);
                            let val = Term::CONS((tim_val.get()) as i32);
                            let constraint = Constraint::new_le(d_sum, Expr::T(val));
                            constraints.push(constraint);
                        }
                    }

                    // We have an input symbol, so we must take that transition and
                    // store the update we have, if any.
                    let (dest_state, output, update) = {
                        let k = (current_state, TransInput::Symbol(*i_symb));
                        self.transitions.get(&k).expect(MACHINE_INCOMPLETE)
                    };
                    current_state = *dest_state;
                    outputs.push(*output);
                    // Any constraints that must be done before we reach the
                    // destination state for the current transition must be
                    // added before we finish inserting the update into the
                    // BiMap.
                    if let Some(update) = update {
                        idxs_with_timer_ids.insert(idx, *update.id());
                        let val = *update.value();
                        update_map.insert(*update.id(), val);
                    }
                }
                SymInput::TimeOut(timeout_idx) => {
                    // In order to take a timeout of the transition at
                    // `timeout_idx`, we need to check *which* timer was set
                    // at this index.
                    let timeout_idx = *timeout_idx as usize;
                    let timer_to_timeout = *idxs_with_timer_ids
                        .get_by_left(&timeout_idx)
                        .expect("Timer was not set from idx.");
                    let val_timer = *update_map
                        .get(&timer_to_timeout)
                        .expect("Timer was not set.");

                    // Next, we have to ensure that the sum of delays from
                    // the moment that the `timer_to_timeout` was set is equal
                    // to the value of the timer.
                    let sum_delays = (timeout_idx..idx)
                        .map(|x| Term::Delay(x as u16))
                        .collect_vec();
                    if !sum_delays.is_empty() {
                        let d_sum = Expr::ADD(sum_delays);
                        let val = Term::CONS((val_timer.get()) as i32);
                        let constraint = Constraint::new_eq(Expr::T(val), d_sum);
                        constraints.push(constraint);
                    }

                    let (dest_state, output, update) = {
                        let k = (current_state, TransInput::TimeOut(timer_to_timeout));
                        self.transitions.get(&k).expect(MACHINE_INCOMPLETE)
                    };
                    current_state = *dest_state;
                    outputs.push(*output);
                    if let Some(update) = update {
                        idxs_with_timer_ids.insert(idx, *update.id());
                        let val = *update.value();
                        update_map.insert(*update.id(), val);
                    } else {
                        // timer was not restarted, throw it out.
                        update_map.remove(&timer_to_timeout);
                        idxs_with_timer_ids.remove_by_right(&timer_to_timeout);
                    }
                }
            }
        }

        // For each timer, we want to know *when* it was set and
        // the *value* of the timer.
        let idx_id_val_s = active_timers
            .iter()
            .filter_map(|id| {
                let idx = idxs_with_timer_ids.get_by_right(id).copied();
                idx.zip(Some(id))
            })
            .map(|(idx, id)| {
                let val = *update_map.get(id).expect(TIMER_NOT_SET);
                (idx, id, val)
            })
            .collect_vec();

        for current_timer_details in &idx_id_val_s {
            let (idx, id, val) = *current_timer_details;
            if !active_timers.contains(id) {
                continue;
            }
            // For each timer, we must check with the previous constraints
            // the following two conditions:
            // 1. The sum of delays for the selected timer must be equal to its value,
            // 2. AND, the other timers must not go off before our timer.
            let sum_delays = (idx..=sym_acc.len())
                .map(|x| Term::Delay(x as u16))
                .collect_vec();
            if !sum_delays.is_empty() {
                let d_sum = Expr::ADD(sum_delays);
                let val = Term::CONS((val.get()) as i32);
                let tim_id = Term::Timer(*id);
                let timer_valuation = Expr::SUB(val, Box::new(d_sum));
                let constraint = Constraint::new_eq(Expr::T(tim_id), timer_valuation);
                constraints.push(constraint);
            }
            let zero = Expr::T(Term::CONS(0));
            let constraint = Constraint::new_ge(Expr::T(Term::Timer(*id)), zero);
            constraints.push(constraint);
        }

        Zone::new(active_timers, constraints)
    }

    /// This function shares way too many similarities with the same function defined for the
    /// observation tree, but I shall keep it separate for now.
    pub fn to_symbolic_sequence(&self, original: &[InternalTransInput]) -> Vec<SymInput> {
        // As we walk along the sequence, we can keep a list of where
        // we set which timer. Same as the MMT symbolic stuff.

        let mut curr_state = self.initial_state;
        let mut symbolic_seq = Vec::new();
        let dest_and_update = |curr, i| self.transitions.get(&(curr, i)).expect("Safe");

        let mut idx_to_timer_bimap = BiHashMap::new();
        for (i, idx) in original.iter().zip(1usize..) {
            match i {
                TransInput::Symbol(i) => {
                    let i_symb = SymInput::Sym(*i);
                    symbolic_seq.push(i_symb);
                    // We must now step through the tree.
                    let (dest_state, _, update) =
                        dest_and_update(curr_state, TransInput::Symbol(*i));
                    curr_state = *dest_state;
                    if let Some(update) = update {
                        idx_to_timer_bimap.insert(idx, *update.id());
                    }
                }
                TransInput::TimeOut(x) => {
                    // We have to get the index of the sequence which set this timer.
                    let idx_for_x = *idx_to_timer_bimap.get_by_right(x).expect(TIMER_NOT_SET);
                    let t_symb = SymInput::TimeOut(idx_for_x as u16);
                    symbolic_seq.push(t_symb);
                    let (dest_state, _, update) =
                        dest_and_update(curr_state, TransInput::TimeOut(*x));
                    curr_state = *dest_state;
                    if let Some(update) = update {
                        idx_to_timer_bimap.insert(idx, *update.id());
                    } else {
                        // If no update then the timer died, it cannot be
                        // restarted anymore, so remove the entry: the timer id may be reused later.
                        // (by a different transition.)
                        idx_to_timer_bimap.remove_by_right(x);
                    }
                }
            }
        }
        symbolic_seq
    }

    fn walk_along(&self, input_seq: &[InternalTransInput]) -> State {
        let mut curr_state = self.initial_state;
        for i in input_seq {
            curr_state = self
                .transitions
                .get(&(curr_state, *i))
                .map(|x| x.0)
                .expect("Safe");
        }
        curr_state
    }
}

#[derive(
    Debug, PartialEq, Eq, Hash, Serialize, Deserialize, Clone, Copy, Unwrap, IsVariant, Display,
)]
pub enum TransInput<T, U> {
    #[display(fmt = "{}", _0)]
    Symbol(T),
    #[display(fmt = "to[x{}]", _0)]
    TimeOut(U),
}

const MACHINE_INCOMPLETE: &str = "MMT is incomplete?";
const TIMER_NOT_SET: &str = "Timer should have been set in a previous transition.";

#[cfg(test)]
mod tests {
    use std::num::NonZeroUsize;

    use lsharp_ru::definitions::mealy::State;
    use rstest::rstest;
    use rustc_hash::FxHashMap;

    use crate::timed_machine::parser::MMTIO;

    use super::SymInput;

    #[test]
    fn active_timers() {
        let path = "./tests/apart_ex3.dot";
        let machine_str = std::fs::read_to_string(path).expect("Could not read file.");
        let mmtio = MMTIO::from_dot(machine_str);
        let mmt_parse = mmtio.try_build();
        let mmt = mmt_parse.get_mmt();
        let timer_map = mmt_parse.timer_map();
        let active_timers = mmt.active_timers();
        let active_timers: FxHashMap<State, Vec<_>> = active_timers
            .into_iter()
            .map(|(s, timers)| {
                let named_timers: Vec<_> = timers
                    .into_iter()
                    .map(|x| timer_map.get_by_right(&(x as usize)).expect("Safe").clone())
                    .collect();
                (s, named_timers)
            })
            .collect();
        println!("Active timers: {:?}", active_timers);
    }

    #[rstest]
    #[case("./tests/basic_symbolic_machine.dot")]
    fn symbolic_oqs(#[case] path: &str) {
        let machine_str = std::fs::read_to_string(path).expect("Could not read file.");
        let mmtio = MMTIO::from_dot(machine_str);
        let mmt_parse = mmtio.try_build();
        let (mmt, input_map, _output_map) = (
            mmt_parse.get_mmt(),
            mmt_parse.input_map(),
            mmt_parse.output_map(),
        );
        // println!("{:?}", mmt);

        let i = *input_map.get_by_left("i").expect("Safe");
        let sym_i = SymInput::Sym(i);

        let mk_timeout_at = |idx: u16| SymInput::TimeOut(idx);

        let to_1 = mk_timeout_at(1);
        let out = mmt.symbolic_output_query(&[sym_i], to_1);
        assert_eq!(out.raw(), 0)
    }

    #[rstest]
    #[case("./tests/basic_symbolic_machine.dot")]
    fn symbolic_wait_query(#[case] path: &str) {
        let machine_str = std::fs::read_to_string(path).expect("Could not read file.");
        let mmtio = MMTIO::from_dot(machine_str);
        let mmt_parse = mmtio.try_build();
        let (mmt, input_map, _output_map) = (
            mmt_parse.get_mmt(),
            mmt_parse.input_map(),
            mmt_parse.output_map(),
        );

        let i = *input_map.get_by_left("i").expect("Safe");
        let sym_i = SymInput::Sym(i);

        let mk_timeout_at = |idx: u16| SymInput::TimeOut(idx);
        let mk_nonzero = |x: usize| NonZeroUsize::try_from(x).expect("Safe");

        let wq_1 = [sym_i, sym_i, mk_timeout_at(2)];
        let exp_1 = vec![(1, mk_nonzero(2))];
        let act_1 = mmt.symbolic_wait_query(&wq_1);
        assert_eq!(exp_1, act_1);

        let wq2 = [sym_i, mk_timeout_at(1), sym_i];
        let exp_2 = vec![(2, mk_nonzero(2)), (3, mk_nonzero(1))];
        let act_2 = mmt.symbolic_wait_query(&wq2);
        assert_eq!(exp_2, act_2);

        let wq3 = [sym_i, sym_i];
        let exp_3 = vec![(1, mk_nonzero(2)), (2, mk_nonzero(1))];
        let act_3 = mmt.symbolic_wait_query(&wq3);
        assert_eq!(exp_3, act_3);
    }
}
