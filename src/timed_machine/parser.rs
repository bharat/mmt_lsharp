use bimap::BiHashMap;
use derive_getters::Getters;
use derive_more::Constructor;
use fnv::FnvHashMap;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use regex::Regex;
use serde::{Deserialize, Serialize};

use std::num::NonZeroUsize;

use super::machine::{TimedMachine, TransInput};
use super::timers::Update;

#[derive(Debug, Getters)]
pub struct MMTParseResult {
    mmt: TimedMachine,
    input_map: BiHashMap<String, InputSymbol>,
    output_map: BiHashMap<String, OutputSymbol>,
    timer_map: BiHashMap<String, usize>, // timer name <-> id
}

impl MMTParseResult {
    pub fn get_mmt(&self) -> TimedMachine {
        self.mmt.clone()
    }
}

#[derive(Debug, Serialize, Deserialize, Default, Constructor)]
pub struct MMTIO {
    inputs: Vec<String>,
    outputs: Vec<String>,
    states: Vec<String>,
    initial_state: String,
    transitions: Vec<Transition>,
}

impl MMTIO {
    pub fn from_dot(machine_str: String) -> Self {
        let transitions: Vec<_> = machine_str
            .lines()
            // Get only transition lines.
            .filter(|line| line.contains("label") && line.contains("->"))
            .map(|line| line.trim())
            .map(Transition::from_dot_edge)
            .collect();

        let states = {
            let mut states: Vec<_> = transitions
                .iter()
                .map(|tr| (tr.src()))
                .unique()
                .cloned()
                .collect();
            let dest_iter = transitions.iter().map(|tr| (tr.dest()));
            for dest in dest_iter {
                if !states.contains(dest) {
                    states.push(dest.clone());
                }
            }
            states
        };
        let inputs: Vec<_> = transitions
            .iter()
            .map(Transition::input)
            .filter(|x| x.is_symbol())
            .cloned()
            .map(TransInput::<String, String>::unwrap_symbol)
            .unique()
            .collect();
        let outputs: Vec<_> = transitions
            .iter()
            .map(Transition::output)
            .cloned()
            .unique()
            .collect();
        let initial_state = states.first().expect("Safe").clone();
        Self::new(inputs, outputs, states, initial_state, transitions)
    }

    pub fn to_dot(&self) -> String {
        let digraph_decl = "digraph g {".to_string();
        let start_node = "__start0 [label=\"\" shape=\"none\"];".to_string();
        let node_decl = "node [margin=0 width=0.5 shape=circle];".to_string();
        let initial_decls = vec![digraph_decl, start_node, node_decl];
        let all_transitions: Vec<_> = self
            .transitions
            .iter()
            .map(Transition::to_dot_edge)
            .collect();
        let initial_state_decl = format!("__start0 -> {};", self.initial_state);
        let digraph_end = "}".to_string();
        let final_decls = vec![initial_state_decl, digraph_end];
        initial_decls
            .into_iter()
            .chain(all_transitions)
            .chain(final_decls)
            .join("\n")
    }

    pub fn try_build(&self) -> MMTParseResult {
        assert!(
            self.states.contains(&self.initial_state),
            "Initial state not present in any transitions."
        );

        let input_map: BiHashMap<_, _> = self
            .inputs
            .iter()
            .enumerate()
            .map(|(i, s)| (s.clone(), InputSymbol::from(i)))
            .collect();
        let output_map: BiHashMap<_, _> = self
            .outputs
            .iter()
            .enumerate()
            .map(|(i, s)| (s.clone(), OutputSymbol::from(i as u16)))
            .collect();
        let timer_map: BiHashMap<_, _> = self
            .transitions
            .iter()
            .filter_map(|tr| tr.update().as_ref())
            .map(|x| x.0.clone())
            .unique()
            .enumerate()
            .map(|(i, s)| (s.clone(), i))
            .collect();
        let state_map: BiHashMap<_, _> = self
            .states
            .iter()
            .enumerate()
            .map(|(i, s)| (s.clone(), State::new(i as u32)))
            .collect();

        let mmt_inputs = input_map.right_values().copied().collect();
        let mmt_outputs = output_map.right_values().copied().collect();
        let mmt_states = state_map.right_values().copied().collect();
        let initial_state = *state_map.get_by_left(&self.initial_state).expect("Safe");

        let transitions = Self::get_transitions(
            &input_map,
            &output_map,
            &state_map,
            &timer_map,
            &self.transitions,
        );

        let mmt = TimedMachine::new(
            mmt_inputs,
            mmt_outputs,
            mmt_states,
            initial_state,
            transitions,
        );

        MMTParseResult {
            mmt,
            input_map,
            output_map,
            timer_map,
        }
    }

    fn get_transitions(
        input_map: &BiHashMap<String, InputSymbol>,
        output_map: &BiHashMap<String, OutputSymbol>,
        state_map: &BiHashMap<String, State>,
        timer_map: &BiHashMap<String, usize>,
        transitions: &Vec<Transition>,
    ) -> FnvHashMap<(State, TransInput<InputSymbol, u16>), (State, OutputSymbol, Option<Update>)>
    {
        let mut ret = FnvHashMap::default();
        for trans in transitions {
            let src = *state_map.get_by_left(trans.src()).expect("Safe");
            let dest = *state_map.get_by_left(trans.dest()).expect("Safe");
            let output = *output_map.get_by_left(trans.output()).expect("Safe");

            // Parse update
            let update = if let Some((tim_name, dur)) = trans.update() {
                let tim_id = *timer_map
                    .get_by_left(tim_name)
                    .expect("Timer not present in map.");
                let update = Update::new(tim_id as u16, *dur);
                Some(update)
            } else {
                None
            };

            // Parse input
            let input = match trans.input() {
                TransInput::Symbol(sym_str) => {
                    let input = *input_map
                        .get_by_left(sym_str)
                        .expect("Input symbol not present in map.");
                    TransInput::Symbol(input)
                }
                TransInput::TimeOut(tim_name) => {
                    let tim_id = *timer_map
                        .get_by_left(tim_name)
                        .expect("Timer not present in map.");
                    TransInput::TimeOut(tim_id as u16)
                }
            };

            ret.insert((src, input), (dest, output, update));
        }
        ret
    }
}

#[derive(Debug, Serialize, Deserialize, Constructor, Getters)]
pub struct Transition {
    src: String,
    dest: String,
    input: TransInput<String, String>,
    output: String,
    update: Option<(String, NonZeroUsize)>,
}

impl Transition {
    /// Get the input string if it is not a timeout.
    pub fn get_input(&self) -> Option<String> {
        if let TransInput::Symbol(x) = &self.input {
            Some(x.clone())
        } else {
            None
        }
    }

    pub fn is_src(&self, state: &str) -> bool {
        self.src == state
    }
}

// DOT conversions.
impl Transition {
    fn from_dot_edge(edge: &str) -> Self {
        static RE: once_cell::sync::Lazy<Regex> = once_cell::sync::Lazy::new(|| {
            Regex::new(
                r#"\s*(?P<src>\w+)\s*->\s*(?P<dest>\w+)\s*\[label(?:\s)*=(?:\s)*"(?P<input>[^ ]+)\s*/\s*(?P<output>[^ ]+)\s*/\s*(?P<update>.*)"[;]*\];"#,
            ).unwrap()
        });
        let Some((_, [src, dest, input, output, update])) = RE.captures(edge).map(|e| e.extract())
        else {
            panic!("Could not read transition {}", edge);
        };
        let update = update.trim();
        let update = {
            if update == "⊥" || update.is_empty() {
                None
            } else {
                let Some((tim_name, _, val)) = update.split_whitespace().next_tuple() else {
                    panic!("Could not read update. Update string: {}", update);
                };
                let val: NonZeroUsize = val.parse().expect(NONZERO_EXPECTED);
                Some((tim_name.to_string(), val))
            }
        };

        let input = {
            // We first want to check if it is a symbol or a timeout.
            // We require at least 4 characters for a timeout:
            // to x
            let is_timeout = if input.len() >= 4 {
                "to" == &input[..=1]
            } else {
                false
            };
            if is_timeout {
                // It is a timeout.
                let timer = &input[3..input.len() - 1];
                TransInput::<String, String>::TimeOut(timer.to_string())
            } else {
                // Basic symbol, just read it in.
                TransInput::<String, String>::Symbol(input.to_string())
            }
        };
        let src = src.to_string();
        let dest = dest.to_string();
        let output = output.to_string();
        Self::new(src, dest, input, output, update)
    }

    fn to_dot_edge(&self) -> String {
        let input = match &self.input {
            TransInput::Symbol(i) => i.to_string(),
            TransInput::TimeOut(x) => {
                format!("to[{}]", x)
            }
        };
        let update = if let Some((tim_name, val)) = &self.update {
            format!("{} := {}", tim_name, val)
        } else {
            "⊥".to_string()
        };
        let trans = format!(
            "{} -> {} [label=\"{} / {} / {}\"];",
            self.src(),
            self.dest(),
            input,
            self.output(),
            update
        );
        trans
    }
}

const NONZERO_EXPECTED: &str = "Expected a non-zero number for update.";
