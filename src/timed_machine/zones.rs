use std::collections::VecDeque;

use derive_more::Unwrap;
use itertools::Itertools;
use rustc_hash::{FxHashMap, FxHashSet};
use z3::ast::{self, Ast, Bool, Real};
use z3::{Config, Context, SatResult, Solver};

#[derive(Debug)]
pub(super) struct Zone {
    /// Each zone has a set number of timers.
    timers: FxHashSet<Term>,
    /// The set of constraints that a zone must satisfy.
    constraints: Vec<Constraint>,
}

impl Zone {
    pub(super) fn new(active_timers: &FxHashSet<u16>, constraints: Vec<Constraint>) -> Self {
        let timers = active_timers.iter().copied().map(Term::Timer).collect();
        Self {
            timers,
            constraints,
        }
    }

    /// `true` if `self` and `other` are equivalent.
    pub(super) fn equivalent_to(&self, other: &Self) -> bool {
        // If we don't have the same timers, we're clearly in different zones.
        if self.timers != other.timers {
            unreachable!("We should never have different active timers for the same state.")
        }
        let all_timers = &self.timers;
        if all_timers.is_empty() {
            return true;
        }
        self.equivalent_timers(other)
    }

    fn equivalent_timers(&self, other: &Self) -> bool {
        let mut cfg = Config::new();
        cfg.set_model_generation(true);
        cfg.set_model_generation(true);
        let ctx = Context::new(&cfg);
        let solver = Solver::new(&ctx);
        let mut self_z3info = FxHashMap::default();
        self.extend_z3_mapper(&ctx, &mut self_z3info);
        let self_constraints = {
            let mut non_neg = self.all_terms_non_neg(&ctx, &self_z3info);
            let left_constraints = self
                .constraints
                .iter()
                .map(|e| e.get_z3_constraints(&ctx, &self_z3info));
            non_neg.extend(left_constraints);
            non_neg
        };
        let self_ref_constraints = self_constraints.iter().collect_vec();

        let self_equation = ast::exists_const(
            &ctx,
            &[
                &ast::Real::new_const(&ctx, "d1"),
                &ast::Real::new_const(&ctx, "d2"),
                &ast::Real::new_const(&ctx, "d3"),
                &ast::Real::new_const(&ctx, "d4"),
                &ast::Real::new_const(&ctx, "d5"),
                &ast::Real::new_const(&ctx, "d6"),
                &ast::Real::new_const(&ctx, "d7"),
                &ast::Real::new_const(&ctx, "d8"),
                &ast::Real::new_const(&ctx, "d9"),
                &ast::Real::new_const(&ctx, "d10"),
                &ast::Real::new_const(&ctx, "d11"),
                &ast::Real::new_const(&ctx, "d12"),
                &ast::Real::new_const(&ctx, "d13"),
                &ast::Real::new_const(&ctx, "d14"),
                &ast::Real::new_const(&ctx, "d15"),
                &ast::Real::new_const(&ctx, "d16"),
                &ast::Real::new_const(&ctx, "d17"),
                &ast::Real::new_const(&ctx, "d18"),
                &ast::Real::new_const(&ctx, "d19"),
                &ast::Real::new_const(&ctx, "d20"),
                &ast::Real::new_const(&ctx, "d21"),
                &ast::Real::new_const(&ctx, "d22"),
                &ast::Real::new_const(&ctx, "d23"),
                &ast::Real::new_const(&ctx, "d24"),
                &ast::Real::new_const(&ctx, "d25"),
                &ast::Real::new_const(&ctx, "d26"),
                &ast::Real::new_const(&ctx, "d27"),
                &ast::Real::new_const(&ctx, "d28"),
                &ast::Real::new_const(&ctx, "d29"),
                &ast::Real::new_const(&ctx, "d30"),
                &ast::Real::new_const(&ctx, "d31"),
                &ast::Real::new_const(&ctx, "d32"),
                &ast::Real::new_const(&ctx, "d33"),
                &ast::Real::new_const(&ctx, "d34"),
                &ast::Real::new_const(&ctx, "d35"),
                &ast::Real::new_const(&ctx, "d36"),
                &ast::Real::new_const(&ctx, "d37"),
                &ast::Real::new_const(&ctx, "d38"),
                &ast::Real::new_const(&ctx, "d39"),
                &ast::Real::new_const(&ctx, "d40"),
                &ast::Real::new_const(&ctx, "d41"),
                &ast::Real::new_const(&ctx, "d42"),
                &ast::Real::new_const(&ctx, "d43"),
                &ast::Real::new_const(&ctx, "d44"),
                &ast::Real::new_const(&ctx, "d45"),
                &ast::Real::new_const(&ctx, "d46"),
                &ast::Real::new_const(&ctx, "d47"),
                &ast::Real::new_const(&ctx, "d48"),
                &ast::Real::new_const(&ctx, "d49"),
                &ast::Real::new_const(&ctx, "d50"),
                &ast::Real::new_const(&ctx, "d51"),
                &ast::Real::new_const(&ctx, "d52"),
                &ast::Real::new_const(&ctx, "d53"),
                &ast::Real::new_const(&ctx, "d54"),
                &ast::Real::new_const(&ctx, "d55"),
                &ast::Real::new_const(&ctx, "d56"),
                &ast::Real::new_const(&ctx, "d57"),
                &ast::Real::new_const(&ctx, "d58"),
                &ast::Real::new_const(&ctx, "d59"),
                &ast::Real::new_const(&ctx, "d60"),
                &ast::Real::new_const(&ctx, "d61"),
                &ast::Real::new_const(&ctx, "d62"),
                &ast::Real::new_const(&ctx, "d63"),
                &ast::Real::new_const(&ctx, "d64"),
                &ast::Real::new_const(&ctx, "d65"),
                &ast::Real::new_const(&ctx, "d66"),
                &ast::Real::new_const(&ctx, "d67"),
                &ast::Real::new_const(&ctx, "d68"),
                &ast::Real::new_const(&ctx, "d69"),
                &ast::Real::new_const(&ctx, "d70"),
                &ast::Real::new_const(&ctx, "d71"),
                &ast::Real::new_const(&ctx, "d72"),
                &ast::Real::new_const(&ctx, "d73"),
                &ast::Real::new_const(&ctx, "d74"),
                &ast::Real::new_const(&ctx, "d75"),
                &ast::Real::new_const(&ctx, "d76"),
                &ast::Real::new_const(&ctx, "d77"),
                &ast::Real::new_const(&ctx, "d78"),
                &ast::Real::new_const(&ctx, "d79"),
                &ast::Real::new_const(&ctx, "d80"),
                &ast::Real::new_const(&ctx, "d81"),
                &ast::Real::new_const(&ctx, "d82"),
                &ast::Real::new_const(&ctx, "d83"),
                &ast::Real::new_const(&ctx, "d84"),
                &ast::Real::new_const(&ctx, "d85"),
                &ast::Real::new_const(&ctx, "d86"),
                &ast::Real::new_const(&ctx, "d87"),
                &ast::Real::new_const(&ctx, "d88"),
                &ast::Real::new_const(&ctx, "d89"),
                &ast::Real::new_const(&ctx, "d90"),
                &ast::Real::new_const(&ctx, "d91"),
                &ast::Real::new_const(&ctx, "d92"),
                &ast::Real::new_const(&ctx, "d93"),
                &ast::Real::new_const(&ctx, "d94"),
                &ast::Real::new_const(&ctx, "d95"),
                &ast::Real::new_const(&ctx, "d96"),
                &ast::Real::new_const(&ctx, "d97"),
                &ast::Real::new_const(&ctx, "d98"),
                &ast::Real::new_const(&ctx, "d99"),
                &ast::Real::new_const(&ctx, "d100"),
                &ast::Real::new_const(&ctx, "d101"),
                &ast::Real::new_const(&ctx, "d102"),
                &ast::Real::new_const(&ctx, "d103"),
                &ast::Real::new_const(&ctx, "d104"),
                &ast::Real::new_const(&ctx, "d105"),
                &ast::Real::new_const(&ctx, "d106"),
                &ast::Real::new_const(&ctx, "d107"),
                &ast::Real::new_const(&ctx, "d108"),
                &ast::Real::new_const(&ctx, "d109"),
                &ast::Real::new_const(&ctx, "d110"),
                &ast::Real::new_const(&ctx, "d111"),
                &ast::Real::new_const(&ctx, "d112"),
                &ast::Real::new_const(&ctx, "d113"),
                &ast::Real::new_const(&ctx, "d114"),
                &ast::Real::new_const(&ctx, "d115"),
                &ast::Real::new_const(&ctx, "d116"),
                &ast::Real::new_const(&ctx, "d117"),
                &ast::Real::new_const(&ctx, "d118"),
                &ast::Real::new_const(&ctx, "d119"),
                &ast::Real::new_const(&ctx, "d120"),
                &ast::Real::new_const(&ctx, "d121"),
                &ast::Real::new_const(&ctx, "d122"),
                &ast::Real::new_const(&ctx, "d123"),
                &ast::Real::new_const(&ctx, "d124"),
                &ast::Real::new_const(&ctx, "d125"),
                &ast::Real::new_const(&ctx, "d126"),
                &ast::Real::new_const(&ctx, "d127"),
                &ast::Real::new_const(&ctx, "d128"),
                &ast::Real::new_const(&ctx, "d129"),
                &ast::Real::new_const(&ctx, "d130"),
                &ast::Real::new_const(&ctx, "d131"),
                &ast::Real::new_const(&ctx, "d132"),
                &ast::Real::new_const(&ctx, "d133"),
                &ast::Real::new_const(&ctx, "d134"),
                &ast::Real::new_const(&ctx, "d135"),
                &ast::Real::new_const(&ctx, "d136"),
                &ast::Real::new_const(&ctx, "d137"),
                &ast::Real::new_const(&ctx, "d138"),
                &ast::Real::new_const(&ctx, "d139"),
                &ast::Real::new_const(&ctx, "d140"),
                &ast::Real::new_const(&ctx, "d141"),
                &ast::Real::new_const(&ctx, "d142"),
                &ast::Real::new_const(&ctx, "d143"),
                &ast::Real::new_const(&ctx, "d144"),
                &ast::Real::new_const(&ctx, "d145"),
                &ast::Real::new_const(&ctx, "d146"),
                &ast::Real::new_const(&ctx, "d147"),
                &ast::Real::new_const(&ctx, "d148"),
                &ast::Real::new_const(&ctx, "d149"),
                &ast::Real::new_const(&ctx, "d150"),
                &ast::Real::new_const(&ctx, "d151"),
                &ast::Real::new_const(&ctx, "d152"),
                &ast::Real::new_const(&ctx, "d153"),
                &ast::Real::new_const(&ctx, "d154"),
                &ast::Real::new_const(&ctx, "d155"),
                &ast::Real::new_const(&ctx, "d156"),
                &ast::Real::new_const(&ctx, "d157"),
                &ast::Real::new_const(&ctx, "d158"),
                &ast::Real::new_const(&ctx, "d159"),
                &ast::Real::new_const(&ctx, "d160"),
                &ast::Real::new_const(&ctx, "d161"),
                &ast::Real::new_const(&ctx, "d162"),
                &ast::Real::new_const(&ctx, "d163"),
                &ast::Real::new_const(&ctx, "d164"),
                &ast::Real::new_const(&ctx, "d165"),
                &ast::Real::new_const(&ctx, "d166"),
                &ast::Real::new_const(&ctx, "d167"),
                &ast::Real::new_const(&ctx, "d168"),
                &ast::Real::new_const(&ctx, "d169"),
                &ast::Real::new_const(&ctx, "d170"),
                &ast::Real::new_const(&ctx, "d171"),
                &ast::Real::new_const(&ctx, "d172"),
                &ast::Real::new_const(&ctx, "d173"),
                &ast::Real::new_const(&ctx, "d174"),
                &ast::Real::new_const(&ctx, "d175"),
                &ast::Real::new_const(&ctx, "d176"),
                &ast::Real::new_const(&ctx, "d177"),
                &ast::Real::new_const(&ctx, "d178"),
                &ast::Real::new_const(&ctx, "d179"),
                &ast::Real::new_const(&ctx, "d180"),
                &ast::Real::new_const(&ctx, "d181"),
                &ast::Real::new_const(&ctx, "d182"),
                &ast::Real::new_const(&ctx, "d183"),
                &ast::Real::new_const(&ctx, "d184"),
                &ast::Real::new_const(&ctx, "d185"),
                &ast::Real::new_const(&ctx, "d186"),
                &ast::Real::new_const(&ctx, "d187"),
                &ast::Real::new_const(&ctx, "d188"),
                &ast::Real::new_const(&ctx, "d189"),
                &ast::Real::new_const(&ctx, "d190"),
                &ast::Real::new_const(&ctx, "d191"),
                &ast::Real::new_const(&ctx, "d192"),
                &ast::Real::new_const(&ctx, "d193"),
                &ast::Real::new_const(&ctx, "d194"),
                &ast::Real::new_const(&ctx, "d195"),
                &ast::Real::new_const(&ctx, "d196"),
                &ast::Real::new_const(&ctx, "d197"),
                &ast::Real::new_const(&ctx, "d198"),
                &ast::Real::new_const(&ctx, "d199"),
                &ast::Real::new_const(&ctx, "d200"),
                &ast::Real::new_const(&ctx, "d201"),
                &ast::Real::new_const(&ctx, "d202"),
                &ast::Real::new_const(&ctx, "d203"),
                &ast::Real::new_const(&ctx, "d204"),
                &ast::Real::new_const(&ctx, "d205"),
                &ast::Real::new_const(&ctx, "d206"),
                &ast::Real::new_const(&ctx, "d207"),
                &ast::Real::new_const(&ctx, "d208"),
                &ast::Real::new_const(&ctx, "d209"),
                &ast::Real::new_const(&ctx, "d210"),
                &ast::Real::new_const(&ctx, "d211"),
                &ast::Real::new_const(&ctx, "d212"),
                &ast::Real::new_const(&ctx, "d213"),
                &ast::Real::new_const(&ctx, "d214"),
                &ast::Real::new_const(&ctx, "d215"),
                &ast::Real::new_const(&ctx, "d216"),
                &ast::Real::new_const(&ctx, "d217"),
                &ast::Real::new_const(&ctx, "d218"),
                &ast::Real::new_const(&ctx, "d219"),
                &ast::Real::new_const(&ctx, "d220"),
                &ast::Real::new_const(&ctx, "d221"),
                &ast::Real::new_const(&ctx, "d222"),
                &ast::Real::new_const(&ctx, "d223"),
                &ast::Real::new_const(&ctx, "d224"),
                &ast::Real::new_const(&ctx, "d225"),
                &ast::Real::new_const(&ctx, "d226"),
                &ast::Real::new_const(&ctx, "d227"),
                &ast::Real::new_const(&ctx, "d228"),
                &ast::Real::new_const(&ctx, "d229"),
                &ast::Real::new_const(&ctx, "d230"),
                &ast::Real::new_const(&ctx, "d231"),
                &ast::Real::new_const(&ctx, "d232"),
                &ast::Real::new_const(&ctx, "d233"),
                &ast::Real::new_const(&ctx, "d234"),
                &ast::Real::new_const(&ctx, "d235"),
                &ast::Real::new_const(&ctx, "d236"),
                &ast::Real::new_const(&ctx, "d237"),
                &ast::Real::new_const(&ctx, "d238"),
                &ast::Real::new_const(&ctx, "d239"),
                &ast::Real::new_const(&ctx, "d240"),
                &ast::Real::new_const(&ctx, "d241"),
                &ast::Real::new_const(&ctx, "d242"),
                &ast::Real::new_const(&ctx, "d243"),
                &ast::Real::new_const(&ctx, "d244"),
                &ast::Real::new_const(&ctx, "d245"),
                &ast::Real::new_const(&ctx, "d246"),
                &ast::Real::new_const(&ctx, "d247"),
                &ast::Real::new_const(&ctx, "d248"),
                &ast::Real::new_const(&ctx, "d249"),
                &ast::Real::new_const(&ctx, "d250"),
                &ast::Real::new_const(&ctx, "d251"),
                &ast::Real::new_const(&ctx, "d252"),
                &ast::Real::new_const(&ctx, "d253"),
                &ast::Real::new_const(&ctx, "d254"),
                &ast::Real::new_const(&ctx, "d255"),
                &ast::Real::new_const(&ctx, "d256"),
                &ast::Real::new_const(&ctx, "d257"),
                &ast::Real::new_const(&ctx, "d258"),
                &ast::Real::new_const(&ctx, "d259"),
                &ast::Real::new_const(&ctx, "d260"),
                &ast::Real::new_const(&ctx, "d261"),
                &ast::Real::new_const(&ctx, "d262"),
                &ast::Real::new_const(&ctx, "d263"),
                &ast::Real::new_const(&ctx, "d264"),
                &ast::Real::new_const(&ctx, "d265"),
                &ast::Real::new_const(&ctx, "d266"),
                &ast::Real::new_const(&ctx, "d267"),
                &ast::Real::new_const(&ctx, "d268"),
                &ast::Real::new_const(&ctx, "d269"),
                &ast::Real::new_const(&ctx, "d270"),
                &ast::Real::new_const(&ctx, "d271"),
                &ast::Real::new_const(&ctx, "d272"),
                &ast::Real::new_const(&ctx, "d273"),
                &ast::Real::new_const(&ctx, "d274"),
                &ast::Real::new_const(&ctx, "d275"),
                &ast::Real::new_const(&ctx, "d276"),
                &ast::Real::new_const(&ctx, "d277"),
                &ast::Real::new_const(&ctx, "d278"),
                &ast::Real::new_const(&ctx, "d279"),
                &ast::Real::new_const(&ctx, "d280"),
                &ast::Real::new_const(&ctx, "d281"),
                &ast::Real::new_const(&ctx, "d282"),
                &ast::Real::new_const(&ctx, "d283"),
                &ast::Real::new_const(&ctx, "d284"),
                &ast::Real::new_const(&ctx, "d285"),
                &ast::Real::new_const(&ctx, "d286"),
                &ast::Real::new_const(&ctx, "d287"),
                &ast::Real::new_const(&ctx, "d288"),
                &ast::Real::new_const(&ctx, "d289"),
                &ast::Real::new_const(&ctx, "d290"),
                &ast::Real::new_const(&ctx, "d291"),
                &ast::Real::new_const(&ctx, "d292"),
                &ast::Real::new_const(&ctx, "d293"),
                &ast::Real::new_const(&ctx, "d294"),
                &ast::Real::new_const(&ctx, "d295"),
                &ast::Real::new_const(&ctx, "d296"),
                &ast::Real::new_const(&ctx, "d297"),
                &ast::Real::new_const(&ctx, "d298"),
                &ast::Real::new_const(&ctx, "d299"),
                &ast::Real::new_const(&ctx, "d300"),
            ],
            &[],
            &ast::Bool::and(&ctx, &self_ref_constraints),
        );

        let mut other_z3info = FxHashMap::default();
        other.extend_z3_mapper_other(&ctx, &mut other_z3info);

        let other_constraints = {
            let mut non_neg = other.all_terms_non_neg(&ctx, &other_z3info);
            let other_constraints = other
                .constraints
                .iter()
                .map(|e| e.get_z3_constraints(&ctx, &other_z3info));
            non_neg.extend(other_constraints);
            non_neg
        };
        let other_ref_constraints = other_constraints.iter().collect_vec();

        let other_equation = ast::exists_const(
            &ctx,
            &[
                &ast::Real::new_const(&ctx, "e1"),
                &ast::Real::new_const(&ctx, "e2"),
                &ast::Real::new_const(&ctx, "e3"),
                &ast::Real::new_const(&ctx, "e4"),
                &ast::Real::new_const(&ctx, "e5"),
                &ast::Real::new_const(&ctx, "e6"),
                &ast::Real::new_const(&ctx, "e7"),
                &ast::Real::new_const(&ctx, "e8"),
                &ast::Real::new_const(&ctx, "e9"),
                &ast::Real::new_const(&ctx, "e10"),
                &ast::Real::new_const(&ctx, "e11"),
                &ast::Real::new_const(&ctx, "e12"),
                &ast::Real::new_const(&ctx, "e13"),
                &ast::Real::new_const(&ctx, "e14"),
                &ast::Real::new_const(&ctx, "e15"),
                &ast::Real::new_const(&ctx, "e16"),
                &ast::Real::new_const(&ctx, "e17"),
                &ast::Real::new_const(&ctx, "e18"),
                &ast::Real::new_const(&ctx, "e19"),
                &ast::Real::new_const(&ctx, "e20"),
                &ast::Real::new_const(&ctx, "e21"),
                &ast::Real::new_const(&ctx, "e22"),
                &ast::Real::new_const(&ctx, "e23"),
                &ast::Real::new_const(&ctx, "e24"),
                &ast::Real::new_const(&ctx, "e25"),
                &ast::Real::new_const(&ctx, "e26"),
                &ast::Real::new_const(&ctx, "e27"),
                &ast::Real::new_const(&ctx, "e28"),
                &ast::Real::new_const(&ctx, "e29"),
                &ast::Real::new_const(&ctx, "e30"),
                &ast::Real::new_const(&ctx, "e31"),
                &ast::Real::new_const(&ctx, "e32"),
                &ast::Real::new_const(&ctx, "e33"),
                &ast::Real::new_const(&ctx, "e34"),
                &ast::Real::new_const(&ctx, "e35"),
                &ast::Real::new_const(&ctx, "e36"),
                &ast::Real::new_const(&ctx, "e37"),
                &ast::Real::new_const(&ctx, "e38"),
                &ast::Real::new_const(&ctx, "e39"),
                &ast::Real::new_const(&ctx, "e40"),
                &ast::Real::new_const(&ctx, "e41"),
                &ast::Real::new_const(&ctx, "e42"),
                &ast::Real::new_const(&ctx, "e43"),
                &ast::Real::new_const(&ctx, "e44"),
                &ast::Real::new_const(&ctx, "e45"),
                &ast::Real::new_const(&ctx, "e46"),
                &ast::Real::new_const(&ctx, "e47"),
                &ast::Real::new_const(&ctx, "e48"),
                &ast::Real::new_const(&ctx, "e49"),
                &ast::Real::new_const(&ctx, "e50"),
                &ast::Real::new_const(&ctx, "e51"),
                &ast::Real::new_const(&ctx, "e52"),
                &ast::Real::new_const(&ctx, "e53"),
                &ast::Real::new_const(&ctx, "e54"),
                &ast::Real::new_const(&ctx, "e55"),
                &ast::Real::new_const(&ctx, "e56"),
                &ast::Real::new_const(&ctx, "e57"),
                &ast::Real::new_const(&ctx, "e58"),
                &ast::Real::new_const(&ctx, "e59"),
                &ast::Real::new_const(&ctx, "e60"),
                &ast::Real::new_const(&ctx, "e61"),
                &ast::Real::new_const(&ctx, "e62"),
                &ast::Real::new_const(&ctx, "e63"),
                &ast::Real::new_const(&ctx, "e64"),
                &ast::Real::new_const(&ctx, "e65"),
                &ast::Real::new_const(&ctx, "e66"),
                &ast::Real::new_const(&ctx, "e67"),
                &ast::Real::new_const(&ctx, "e68"),
                &ast::Real::new_const(&ctx, "e69"),
                &ast::Real::new_const(&ctx, "e70"),
                &ast::Real::new_const(&ctx, "e71"),
                &ast::Real::new_const(&ctx, "e72"),
                &ast::Real::new_const(&ctx, "e73"),
                &ast::Real::new_const(&ctx, "e74"),
                &ast::Real::new_const(&ctx, "e75"),
                &ast::Real::new_const(&ctx, "e76"),
                &ast::Real::new_const(&ctx, "e77"),
                &ast::Real::new_const(&ctx, "e78"),
                &ast::Real::new_const(&ctx, "e79"),
                &ast::Real::new_const(&ctx, "e80"),
                &ast::Real::new_const(&ctx, "e81"),
                &ast::Real::new_const(&ctx, "e82"),
                &ast::Real::new_const(&ctx, "e83"),
                &ast::Real::new_const(&ctx, "e84"),
                &ast::Real::new_const(&ctx, "e85"),
                &ast::Real::new_const(&ctx, "e86"),
                &ast::Real::new_const(&ctx, "e87"),
                &ast::Real::new_const(&ctx, "e88"),
                &ast::Real::new_const(&ctx, "e89"),
                &ast::Real::new_const(&ctx, "e90"),
                &ast::Real::new_const(&ctx, "e91"),
                &ast::Real::new_const(&ctx, "e92"),
                &ast::Real::new_const(&ctx, "e93"),
                &ast::Real::new_const(&ctx, "e94"),
                &ast::Real::new_const(&ctx, "e95"),
                &ast::Real::new_const(&ctx, "e96"),
                &ast::Real::new_const(&ctx, "e97"),
                &ast::Real::new_const(&ctx, "e98"),
                &ast::Real::new_const(&ctx, "e99"),
                &ast::Real::new_const(&ctx, "e100"),
                &ast::Real::new_const(&ctx, "e101"),
                &ast::Real::new_const(&ctx, "e102"),
                &ast::Real::new_const(&ctx, "e103"),
                &ast::Real::new_const(&ctx, "e104"),
                &ast::Real::new_const(&ctx, "e105"),
                &ast::Real::new_const(&ctx, "e106"),
                &ast::Real::new_const(&ctx, "e107"),
                &ast::Real::new_const(&ctx, "e108"),
                &ast::Real::new_const(&ctx, "e109"),
                &ast::Real::new_const(&ctx, "e110"),
                &ast::Real::new_const(&ctx, "e111"),
                &ast::Real::new_const(&ctx, "e112"),
                &ast::Real::new_const(&ctx, "e113"),
                &ast::Real::new_const(&ctx, "e114"),
                &ast::Real::new_const(&ctx, "e115"),
                &ast::Real::new_const(&ctx, "e116"),
                &ast::Real::new_const(&ctx, "e117"),
                &ast::Real::new_const(&ctx, "e118"),
                &ast::Real::new_const(&ctx, "e119"),
                &ast::Real::new_const(&ctx, "e120"),
                &ast::Real::new_const(&ctx, "e121"),
                &ast::Real::new_const(&ctx, "e122"),
                &ast::Real::new_const(&ctx, "e123"),
                &ast::Real::new_const(&ctx, "e124"),
                &ast::Real::new_const(&ctx, "e125"),
                &ast::Real::new_const(&ctx, "e126"),
                &ast::Real::new_const(&ctx, "e127"),
                &ast::Real::new_const(&ctx, "e128"),
                &ast::Real::new_const(&ctx, "e129"),
                &ast::Real::new_const(&ctx, "e130"),
                &ast::Real::new_const(&ctx, "e131"),
                &ast::Real::new_const(&ctx, "e132"),
                &ast::Real::new_const(&ctx, "e133"),
                &ast::Real::new_const(&ctx, "e134"),
                &ast::Real::new_const(&ctx, "e135"),
                &ast::Real::new_const(&ctx, "e136"),
                &ast::Real::new_const(&ctx, "e137"),
                &ast::Real::new_const(&ctx, "e138"),
                &ast::Real::new_const(&ctx, "e139"),
                &ast::Real::new_const(&ctx, "e140"),
                &ast::Real::new_const(&ctx, "e141"),
                &ast::Real::new_const(&ctx, "e142"),
                &ast::Real::new_const(&ctx, "e143"),
                &ast::Real::new_const(&ctx, "e144"),
                &ast::Real::new_const(&ctx, "e145"),
                &ast::Real::new_const(&ctx, "e146"),
                &ast::Real::new_const(&ctx, "e147"),
                &ast::Real::new_const(&ctx, "e148"),
                &ast::Real::new_const(&ctx, "e149"),
                &ast::Real::new_const(&ctx, "e150"),
                &ast::Real::new_const(&ctx, "e151"),
                &ast::Real::new_const(&ctx, "e152"),
                &ast::Real::new_const(&ctx, "e153"),
                &ast::Real::new_const(&ctx, "e154"),
                &ast::Real::new_const(&ctx, "e155"),
                &ast::Real::new_const(&ctx, "e156"),
                &ast::Real::new_const(&ctx, "e157"),
                &ast::Real::new_const(&ctx, "e158"),
                &ast::Real::new_const(&ctx, "e159"),
                &ast::Real::new_const(&ctx, "e160"),
                &ast::Real::new_const(&ctx, "e161"),
                &ast::Real::new_const(&ctx, "e162"),
                &ast::Real::new_const(&ctx, "e163"),
                &ast::Real::new_const(&ctx, "e164"),
                &ast::Real::new_const(&ctx, "e165"),
                &ast::Real::new_const(&ctx, "e166"),
                &ast::Real::new_const(&ctx, "e167"),
                &ast::Real::new_const(&ctx, "e168"),
                &ast::Real::new_const(&ctx, "e169"),
                &ast::Real::new_const(&ctx, "e170"),
                &ast::Real::new_const(&ctx, "e171"),
                &ast::Real::new_const(&ctx, "e172"),
                &ast::Real::new_const(&ctx, "e173"),
                &ast::Real::new_const(&ctx, "e174"),
                &ast::Real::new_const(&ctx, "e175"),
                &ast::Real::new_const(&ctx, "e176"),
                &ast::Real::new_const(&ctx, "e177"),
                &ast::Real::new_const(&ctx, "e178"),
                &ast::Real::new_const(&ctx, "e179"),
                &ast::Real::new_const(&ctx, "e180"),
                &ast::Real::new_const(&ctx, "e181"),
                &ast::Real::new_const(&ctx, "e182"),
                &ast::Real::new_const(&ctx, "e183"),
                &ast::Real::new_const(&ctx, "e184"),
                &ast::Real::new_const(&ctx, "e185"),
                &ast::Real::new_const(&ctx, "e186"),
                &ast::Real::new_const(&ctx, "e187"),
                &ast::Real::new_const(&ctx, "e188"),
                &ast::Real::new_const(&ctx, "e189"),
                &ast::Real::new_const(&ctx, "e190"),
                &ast::Real::new_const(&ctx, "e191"),
                &ast::Real::new_const(&ctx, "e192"),
                &ast::Real::new_const(&ctx, "e193"),
                &ast::Real::new_const(&ctx, "e194"),
                &ast::Real::new_const(&ctx, "e195"),
                &ast::Real::new_const(&ctx, "e196"),
                &ast::Real::new_const(&ctx, "e197"),
                &ast::Real::new_const(&ctx, "e198"),
                &ast::Real::new_const(&ctx, "e199"),
                &ast::Real::new_const(&ctx, "e200"),
                &ast::Real::new_const(&ctx, "e201"),
                &ast::Real::new_const(&ctx, "e202"),
                &ast::Real::new_const(&ctx, "e203"),
                &ast::Real::new_const(&ctx, "e204"),
                &ast::Real::new_const(&ctx, "e205"),
                &ast::Real::new_const(&ctx, "e206"),
                &ast::Real::new_const(&ctx, "e207"),
                &ast::Real::new_const(&ctx, "e208"),
                &ast::Real::new_const(&ctx, "e209"),
                &ast::Real::new_const(&ctx, "e210"),
                &ast::Real::new_const(&ctx, "e211"),
                &ast::Real::new_const(&ctx, "e212"),
                &ast::Real::new_const(&ctx, "e213"),
                &ast::Real::new_const(&ctx, "e214"),
                &ast::Real::new_const(&ctx, "e215"),
                &ast::Real::new_const(&ctx, "e216"),
                &ast::Real::new_const(&ctx, "e217"),
                &ast::Real::new_const(&ctx, "e218"),
                &ast::Real::new_const(&ctx, "e219"),
                &ast::Real::new_const(&ctx, "e220"),
                &ast::Real::new_const(&ctx, "e221"),
                &ast::Real::new_const(&ctx, "e222"),
                &ast::Real::new_const(&ctx, "e223"),
                &ast::Real::new_const(&ctx, "e224"),
                &ast::Real::new_const(&ctx, "e225"),
                &ast::Real::new_const(&ctx, "e226"),
                &ast::Real::new_const(&ctx, "e227"),
                &ast::Real::new_const(&ctx, "e228"),
                &ast::Real::new_const(&ctx, "e229"),
                &ast::Real::new_const(&ctx, "e230"),
                &ast::Real::new_const(&ctx, "e231"),
                &ast::Real::new_const(&ctx, "e232"),
                &ast::Real::new_const(&ctx, "e233"),
                &ast::Real::new_const(&ctx, "e234"),
                &ast::Real::new_const(&ctx, "e235"),
                &ast::Real::new_const(&ctx, "e236"),
                &ast::Real::new_const(&ctx, "e237"),
                &ast::Real::new_const(&ctx, "e238"),
                &ast::Real::new_const(&ctx, "e239"),
                &ast::Real::new_const(&ctx, "e240"),
                &ast::Real::new_const(&ctx, "e241"),
                &ast::Real::new_const(&ctx, "e242"),
                &ast::Real::new_const(&ctx, "e243"),
                &ast::Real::new_const(&ctx, "e244"),
                &ast::Real::new_const(&ctx, "e245"),
                &ast::Real::new_const(&ctx, "e246"),
                &ast::Real::new_const(&ctx, "e247"),
                &ast::Real::new_const(&ctx, "e248"),
                &ast::Real::new_const(&ctx, "e249"),
                &ast::Real::new_const(&ctx, "e250"),
                &ast::Real::new_const(&ctx, "e251"),
                &ast::Real::new_const(&ctx, "e252"),
                &ast::Real::new_const(&ctx, "e253"),
                &ast::Real::new_const(&ctx, "e254"),
                &ast::Real::new_const(&ctx, "e255"),
                &ast::Real::new_const(&ctx, "e256"),
                &ast::Real::new_const(&ctx, "e257"),
                &ast::Real::new_const(&ctx, "e258"),
                &ast::Real::new_const(&ctx, "e259"),
                &ast::Real::new_const(&ctx, "e260"),
                &ast::Real::new_const(&ctx, "e261"),
                &ast::Real::new_const(&ctx, "e262"),
                &ast::Real::new_const(&ctx, "e263"),
                &ast::Real::new_const(&ctx, "e264"),
                &ast::Real::new_const(&ctx, "e265"),
                &ast::Real::new_const(&ctx, "e266"),
                &ast::Real::new_const(&ctx, "e267"),
                &ast::Real::new_const(&ctx, "e268"),
                &ast::Real::new_const(&ctx, "e269"),
                &ast::Real::new_const(&ctx, "e270"),
                &ast::Real::new_const(&ctx, "e271"),
                &ast::Real::new_const(&ctx, "e272"),
                &ast::Real::new_const(&ctx, "e273"),
                &ast::Real::new_const(&ctx, "e274"),
                &ast::Real::new_const(&ctx, "e275"),
                &ast::Real::new_const(&ctx, "e276"),
                &ast::Real::new_const(&ctx, "e277"),
                &ast::Real::new_const(&ctx, "e278"),
                &ast::Real::new_const(&ctx, "e279"),
                &ast::Real::new_const(&ctx, "e280"),
                &ast::Real::new_const(&ctx, "e281"),
                &ast::Real::new_const(&ctx, "e282"),
                &ast::Real::new_const(&ctx, "e283"),
                &ast::Real::new_const(&ctx, "e284"),
                &ast::Real::new_const(&ctx, "e285"),
                &ast::Real::new_const(&ctx, "e286"),
                &ast::Real::new_const(&ctx, "e287"),
                &ast::Real::new_const(&ctx, "e288"),
                &ast::Real::new_const(&ctx, "e289"),
                &ast::Real::new_const(&ctx, "e290"),
                &ast::Real::new_const(&ctx, "e291"),
                &ast::Real::new_const(&ctx, "e292"),
                &ast::Real::new_const(&ctx, "e293"),
                &ast::Real::new_const(&ctx, "e294"),
                &ast::Real::new_const(&ctx, "e295"),
                &ast::Real::new_const(&ctx, "e296"),
                &ast::Real::new_const(&ctx, "e297"),
                &ast::Real::new_const(&ctx, "e298"),
                &ast::Real::new_const(&ctx, "e299"),
                &ast::Real::new_const(&ctx, "e300"),
            ],
            &[],
            &ast::Bool::and(&ctx, &other_ref_constraints),
        );

        let eqations_equal = &ast::Bool::_eq(&self_equation, &other_equation);
        let equations_not_eqal = &ast::Bool::not(eqations_equal);
        solver.assert(equations_not_eqal);
        solver.check() == SatResult::Unsat
    }

    fn all_terms_non_neg<'b, 'a: 'b>(
        &'a self,
        ctx: &'a Context,
        z3info: &'b FxHashMap<Term, Real<'a>>,
    ) -> Vec<Bool<'_>> {
        let all_terms = self
            .constraints
            .iter()
            .flat_map(|e| e.get_terms())
            .collect_vec();
        // Initialise all terms and put them in a map for later access.
        let z3_zero = ast::Real::from_real(ctx, 0, 1);
        let mut non_negs = vec![];
        for t in &all_terms {
            let x = z3info.get(t).expect("All terms should be in z3info");
            match t {
                Term::Timer(_) | Term::Delay(_) => {
                    // Assert that all the variables must be non-negative.
                    let x = ast::Real::ge(x, &z3_zero);
                    non_negs.push(x);
                }
                Term::CONS(_) => {}
            }
        }
        non_negs
    }

    fn extend_z3_mapper<'b, 'a: 'b>(
        &'a self,
        ctx: &'a Context,
        z3info: &'b mut FxHashMap<Term, Real<'a>>,
    ) {
        let all_terms = self
            .constraints
            .iter()
            .flat_map(|e| e.get_terms())
            .collect_vec();
        // Initialise all terms and put them in a map for later access.
        for t in &all_terms {
            let x = t.to_z3(ctx);
            z3info.insert(*t, x.clone());
        }
    }

    fn extend_z3_mapper_other<'b, 'a: 'b>(
        &'a self,
        ctx: &'a Context,
        z3info: &'b mut FxHashMap<Term, Real<'a>>,
    ) {
        let all_terms = self
            .constraints
            .iter()
            .flat_map(|e| e.get_terms())
            .collect_vec();
        // Initialise all terms and put them in a map for later access.
        for t in &all_terms {
            let x = t.to_z3_other(ctx);
            z3info.insert(*t, x.clone());
        }
    }

    pub(super) fn initial_state() -> Zone {
        Self {
            timers: FxHashSet::default(),
            constraints: Vec::default(),
        }
    }
}

/// A Z3 term can either be a variable or a constant value.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub(super) enum Term {
    /// Names for the variables.
    Timer(u16),
    /// Names for the delays.
    Delay(u16),
    /// Since we can have numbers in the range [0..]
    CONS(i32),
}

impl Term {
    #[must_use]
    fn to_z3_other<'b, 'a: 'b>(&'b self, ctx: &'a Context) -> Real<'a> {
        match self {
            Term::Timer(i) => {
                let x = ast::Real::new_const(ctx, format!("x{}", i));
                x
            }
            Term::Delay(i) => {
                let x = ast::Real::new_const(ctx, format!("e{}", i));
                x
            }
            Term::CONS(x) => {
                let x = ast::Real::from_real(ctx, *x, 1);
                x
            }
        }
    }

    #[must_use]
    fn to_z3<'b, 'a: 'b>(&'b self, ctx: &'a Context) -> Real<'a> {
        match self {
            Term::Timer(i) => {
                let x = ast::Real::new_const(ctx, format!("x{}", i));
                x
            }
            Term::Delay(i) => {
                let x = ast::Real::new_const(ctx, format!("d{}", i));
                x
            }
            Term::CONS(x) => {
                let x = ast::Real::from_real(ctx, *x, 1);
                x
            }
        }
    }
}

#[derive(Debug, Clone, Unwrap)]
/// A Z3 expression can be of the following forms.
pub(super) enum Expr {
    T(Term),
    ADD(Vec<Term>),
    SUB(Term, Box<Expr>),
}

impl Expr {
    fn get_terms(&self) -> Vec<Term> {
        let mut terms = Vec::new();
        let mut work_list = VecDeque::from([self]);
        while let Some(e) = work_list.pop_front() {
            match e {
                Expr::T(t) => {
                    terms.push(t.clone());
                }
                Expr::ADD(add_terms) => {
                    terms.extend_from_slice(add_terms);
                }
                Expr::SUB(l, r) => {
                    terms.push(l.clone());
                    work_list.push_back(r);
                }
            }
        }
        terms
    }

    #[must_use]
    fn get_z3_expr<'b, 'a: 'b>(
        &'b self,
        ctx: &'a Context,
        z3info: &'a FxHashMap<Term, Real<'_>>,
    ) -> ast::Real<'_> {
        match self {
            Expr::T(t) => z3info.get(t).expect("Safe").clone(),
            Expr::ADD(terms) => {
                let values = terms
                    .iter()
                    .map(|x| z3info.get(x).expect("Safe"))
                    .collect_vec();
                ast::Real::add(ctx, &values)
            }
            Expr::SUB(l, r) => {
                let l_z3 = z3info.get(l).expect("Safe");
                let r_z3 = &r.get_z3_expr(ctx, z3info);
                ast::Real::sub(ctx, &[l_z3, r_z3])
            }
        }
    }
}

#[derive(Debug)]
/// A Z3 constraint.
pub(super) struct Constraint {
    left: Expr,
    right: Expr,
    op: ConstraintType,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum ConstraintType {
    LE,
    GE,
    EQ,
}

impl ConstraintType {
    #[must_use]
    fn add_z3_constraint<'b, 'a: 'b>(&'b self, l_z3: Real<'a>, r_z3: Real<'a>) -> Bool {
        match self {
            ConstraintType::LE => ast::Real::le(&l_z3, &r_z3),
            ConstraintType::GE => ast::Real::ge(&l_z3, &r_z3),
            ConstraintType::EQ => ast::Real::_eq(&l_z3, &r_z3),
        }
    }
}

impl Constraint {
    pub(super) fn new_le(left: Expr, right: Expr) -> Self {
        Self {
            left,
            right,
            op: ConstraintType::LE,
        }
    }

    pub(super) fn new_ge(left: Expr, right: Expr) -> Self {
        Self {
            left,
            right,
            op: ConstraintType::GE,
        }
    }

    pub(super) fn new_eq(left: Expr, right: Expr) -> Self {
        Self {
            left,
            right,
            op: ConstraintType::EQ,
        }
    }

    fn get_terms(&self) -> Vec<Term> {
        [&self.left, &self.right]
            .into_iter()
            .flat_map(|t| t.get_terms())
            .collect()
    }

    #[must_use]
    fn get_z3_constraints<'b, 'a: 'b>(
        &'b self,
        ctx: &'a Context,
        z3info: &'a FxHashMap<Term, Real<'_>>,
    ) -> Bool<'_> {
        let l_z3 = self.left.get_z3_expr(ctx, z3info);
        let r_z3 = self.right.get_z3_expr(ctx, z3info);
        self.op.add_z3_constraint(l_z3, r_z3)
    }
}
