use std::num::NonZeroUsize;

use bimap::BiHashMap;
use fnv::FnvHashMap;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use rustc_hash::FxHashSet;

use crate::teachers::SymInput;
use crate::timed_machine::{machine::TransInput, timers::Update};

use self::arena::ArenaTree;

mod arena;

type InternalTransInput = TransInput<InputSymbol, u16>; // Inputsymbol and timer id.
type TransitionProperty = (State, OutputSymbol, Option<Update>);

#[derive(Debug, Default)]
pub struct ObservationNode {
    /// We only support up to 2^31 - 1 states in the observation tree.
    transitions: FnvHashMap<InternalTransInput, TransitionProperty>,
}

#[derive(Debug)]
pub struct ObservationTree {
    inner: ArenaTree<ObservationNode, InternalTransInput>,
}

impl Default for ObservationTree {
    fn default() -> Self {
        let node = ObservationNode::default();
        let mut inner = ArenaTree::default();
        inner.node(node);
        Self { inner }
    }
}

// ------------------- PUBLIC IMPLS ---------------------- //
impl ObservationTree {
    /// Number of states in the tree.
    pub fn len(&self) -> usize {
        self.inner.size()
    }

    pub fn is_empty(&self) -> bool {
        false
    }

    pub fn get_access_seq(&self, target: State) -> Vec<InternalTransInput> {
        self.get_transfer_seq(State::new(0), target)
    }

    pub fn insert_observation_from(
        &mut self,
        src: State,
        input: InternalTransInput,
        output: OutputSymbol,
        update: Option<Update>,
    ) -> State {
        let dest_raw = self.inner.size() as u32;
        let dest_node = ObservationNode::default();
        let dest_state = State::new(dest_raw);
        let dest_node_idx = self
            .inner
            .node_with_parent(dest_node, src.raw() as usize, input);

        assert_eq!(dest_node_idx, dest_raw as usize);
        let trans_prop = (dest_state, output, update);

        let src_node = &mut self.inner.arena[src.raw() as usize].val;
        src_node.transitions.insert(input, trans_prop);
        State::new(dest_raw)
    }

    pub fn to_symbolic_sequence(&self, original: &[TransInput<InputSymbol, u16>]) -> Vec<SymInput> {
        // As we walk along the sequence, we can keep a list of where
        // we set which timer. Same as the MMT symbolic stuff.

        let mut curr_state = State::new(0);
        let mut symbolic_seq = Vec::new();

        let mut idx_to_timer_bimap = BiHashMap::new();
        for (i, idx) in original.iter().zip(1usize..) {
            match i {
                TransInput::Symbol(i) => {
                    let i_symb = SymInput::Sym(*i);
                    symbolic_seq.push(i_symb);
                    // We must now step through the tree.
                    let (dest_state, _output, update) = self
                        .get_output_and_succ(curr_state, TransInput::Symbol(*i))
                        .expect(OBS_TREE_FORGOT_SEQ);
                    curr_state = dest_state;
                    if let Some(update) = update {
                        idx_to_timer_bimap.insert(idx, *update.id());
                    }
                }
                TransInput::TimeOut(x) => {
                    // We have to get the index of the sequence which set this timer.
                    let idx_for_x = *idx_to_timer_bimap.get_by_right(x).expect(TIMER_NOT_SET);
                    let t_symb = SymInput::TimeOut(idx_for_x as u16);
                    symbolic_seq.push(t_symb);
                    let (dest_state, _output, update) = self
                        .get_output_and_succ(curr_state, TransInput::TimeOut(*x))
                        .expect(OBS_TREE_FORGOT_SEQ);
                    curr_state = dest_state;
                    if let Some(update) = update {
                        idx_to_timer_bimap.insert(idx, *update.id());
                    } else {
                        // If no update then the timer died, it cannot be
                        // restarted anymore, so there's no point in remembering the index.
                        idx_to_timer_bimap.remove_by_right(x);
                    }
                }
            }
        }
        symbolic_seq
    }

    pub fn get_output_and_succ(
        &self,
        src: State,
        input: InternalTransInput,
    ) -> Option<TransitionProperty> {
        let key = input;
        let trans_prop = self.inner.arena[src.raw() as usize]
            .val
            .transitions
            .get(&key);
        trans_prop.cloned()
    }

    pub fn which_timer_set_at(&self, seq: &[InternalTransInput], idx: usize) -> u16 {
        let mut curr_state = State::new(0);
        let mut cnt = 1;
        for i in seq {
            let (dest_state, _, update) = self.inner.arena[curr_state.raw() as usize]
                .val
                .transitions
                .get(i)
                .expect(OBS_TREE_FORGOT_SEQ);
            curr_state = *dest_state;
            if idx == cnt {
                let timer_id = update.as_ref().map(Update::id).expect(TIMER_NOT_SET);
                return *timer_id;
            }
            cnt += 1;
        }
        unreachable!("");
    }

    pub fn update_timer_at_idx_with_val(
        &mut self,
        access_seq: &[InternalTransInput],
        tim_idx: usize,
        tim_val: NonZeroUsize,
    ) {
        let mut cnt = 1;
        let mut curr_state = State::new(0);
        for i in access_seq {
            if cnt != tim_idx {
                let k = i;
                if let TransInput::TimeOut(_tr_idx) = k {
                    //
                }
                let (dest, _, _) = self
                    .all_transitions_at(curr_state)
                    .get(k)
                    .expect(OBS_TREE_FORGOT_SEQ);
                curr_state = *dest;
            } else {
                match i {
                    TransInput::Symbol(i) => {
                        let k = TransInput::Symbol(*i);
                        let (dest, _, update) = self
                            .inner
                            .mut_ref_at(curr_state.raw() as usize)
                            .transitions
                            .get_mut(&k)
                            .expect(OBS_TREE_FORGOT_SEQ);
                        let new_update = Update::new(dest.raw() as u16, tim_val);
                        let _old_update = update.replace(new_update);
                        return;
                    }
                    TransInput::TimeOut(id) => {
                        let k = TransInput::TimeOut(*id);
                        let (_, _, update) = self
                            .inner
                            .mut_ref_at(curr_state.raw() as usize)
                            .transitions
                            .get_mut(&k)
                            .expect(OBS_TREE_FORGOT_SEQ);
                        let new_update = Update::new(*id, tim_val);
                        let _old_update = update.replace(new_update);
                    }
                }
            }
            cnt += 1;
        }
    }

    pub fn all_transitions_at(
        &self,
        state: State,
    ) -> &FnvHashMap<InternalTransInput, TransitionProperty> {
        &self.inner.ref_at(state.raw() as usize).transitions
    }

    pub fn create_update_between(&mut self, (src, dest): (State, State), val: NonZeroUsize) {
        // A timer can only be started at the transition leading to the state.
        let (input, trans_prop) = self
            .inner
            .mut_ref_at(src.raw() as usize)
            .transitions
            .iter_mut()
            .filter(|(_, (d, _, _))| *d == dest)
            .exactly_one()
            .expect("A transition must have only one destination state.");
        let (_, _, update) = trans_prop;
        let new_update = match input {
            TransInput::Symbol(_) => Update::new(dest.raw() as u16, val),
            TransInput::TimeOut(x) => Update::new(*x, val),
        };
        if update.is_some() {
            assert_eq!(
                update.clone().unwrap(),
                new_update,
                "{}",
                UPDATE_ALREADY_SET
            );
        }
        *update = Some(new_update);
    }

    pub(crate) fn get_seq_states(
        &self,
        access_seq: &[TransInput<InputSymbol, u16>],
    ) -> Vec<(State, State)> {
        let mut curr_state = State::new(0);
        let mut ret = vec![(curr_state, curr_state)]; // The first entry is just useless.
        for i in access_seq {
            let d = self
                .inner
                .ref_at(curr_state.raw() as usize)
                .transitions
                .get(i)
                .expect("Safe")
                .0;
            ret.push((curr_state, d));
            curr_state = d;
        }
        ret
    }
}

// ------------------- TIMER STUFF ---------------------- //
impl ObservationTree {
    pub fn get_active_timers_at(&self, s: State) -> FxHashSet<u16> {
        // Initial state never has any active timers.
        if s == State::new(0) {
            return FxHashSet::default();
        }

        // Get all local timeouts, they will always be in the set of active timers.
        let s_trans = self.all_transitions_at(s);
        let mut active_timers: FxHashSet<_> = s_trans
            .keys()
            .filter(|i| i.is_time_out())
            .map(|i| i.unwrap_time_out())
            .collect();

        for (i, (t, _, up)) in s_trans {
            if let Some(up) = up {
                let mut t_active = self.get_active_timers_at(*t);
                t_active.remove(up.id());
                active_timers.extend(t_active.into_iter());
            } else {
                let t_active = self.get_active_timers_at(*t);
                if i.is_time_out() {
                    let tim_id = i.unwrap_time_out();
                    let err_str = "Extra timeout in set of active timers!";
                    assert!(!t_active.contains(&tim_id), "{}", err_str);
                }
                active_timers.extend(t_active.into_iter());
            }
        }
        active_timers
    }
}

// ------------------- UTILITY IMPLS ---------------------- //
impl ObservationTree {
    fn get_transfer_seq(&self, source: State, target: State) -> Vec<InternalTransInput> {
        let mut access_seq = Vec::new();
        if target == source {
            return access_seq;
        }
        let dest_parent_idx = source.raw() as usize;
        let mut curr_state = target.raw() as usize;
        loop {
            let (i, parent_idx) = self.inner.arena[curr_state].parent.unwrap();
            access_seq.push(i);
            if parent_idx == dest_parent_idx {
                break;
            }
            curr_state = parent_idx;
        }
        access_seq.reverse();
        access_seq
    }
}

// ------------------- VISUALISATION IMPLS ---------------------- //
impl ObservationTree {
    pub fn to_dot(
        &self,
        input_map: &BiHashMap<String, InputSymbol>,
        output_map: &BiHashMap<String, OutputSymbol>,
        basis: &FxHashSet<State>,
        frontier: &FxHashSet<State>,
    ) -> String {
        self.interal_to_dot(input_map, output_map, basis, frontier)
    }
}

const OBS_TREE_FORGOT_SEQ: &str = "Observation tree forgot a previously inserted sequence.";
const UPDATE_ALREADY_SET: &str = "Attempted to replace an existing update.";
const TIMER_NOT_SET: &str = "Timer timed out without being set?";

mod dot {
    use super::*;
    impl ObservationTree {
        pub fn interal_to_dot(
            &self,
            input_map: &BiHashMap<String, InputSymbol>,
            output_map: &BiHashMap<String, OutputSymbol>,
            basis: &FxHashSet<State>,
            frontier: &FxHashSet<State>,
        ) -> String {
            let header = DOT_HEADER.to_string();
            let footer = DOT_FOOTER.to_string();
            let closer = GRAPH_END_BRACE.to_string();
            let all_states = self.states_to_dot(basis, frontier);
            let all_transitions = self.transitions_to_dot(input_map, output_map);
            [header, all_states, all_transitions, footer, closer].join("\n")
        }

        fn states_to_dot(&self, basis: &FxHashSet<State>, frontier: &FxHashSet<State>) -> String {
            let num_states = self.inner.arena.len() as u32;
            let states_range = (0..num_states).map(State::new);
            let basis_iter = states_range.clone().filter(|s| basis.contains(s));
            let frontier_iter = states_range.clone().filter(|s| frontier.contains(s));
            let normal_states = states_range
                .clone()
                .filter(|s| !frontier.contains(s) && !basis.contains(s));
            let basis_states = basis_iter.map(|s| format!("t{}", s.raw())).join("; ");
            let frontier_states = frontier_iter.map(|s| format!("t{}", s.raw())).join("; ");

            let basis_subgraph = r#"subgraph basis
        {"#
            .to_string();
            let basis_subgraph = [
                basis_subgraph,
                BASIS_NODE.to_string(),
                basis_states,
                GRAPH_END_BRACE.to_string(),
            ]
            .join("\n");
            let frontier_subgraph = r#"subgraph frontier
        {"#
            .to_string();
            let frontier_subgraph = [
                frontier_subgraph,
                FRONTIER_NODE.to_string(),
                frontier_states,
                GRAPH_END_BRACE.to_string(),
            ]
            .join("\n");

            let state_stringify = |t| format!("t{t} [shape = \"circle\" label=\"t{t}\"];");
            let normal_states = normal_states
                .map(State::raw)
                .map(state_stringify)
                .join("\n");

            [basis_subgraph, frontier_subgraph, normal_states].join("\n")
        }

        fn transitions_to_dot(
            &self,
            input_map: &BiHashMap<String, InputSymbol>,
            output_map: &BiHashMap<String, OutputSymbol>,
        ) -> String {
            let num_states = self.inner.arena.len() as u32;
            let states_range = 0..num_states;
            // What we want to do now is to visit every state and
            // print each transition in it.
            let transitions = |s| {
                let trans = &self.inner.ref_at(s as usize).transitions;
                Iterator::zip(std::iter::repeat(s), trans)
            };
            #[allow(clippy::type_complexity)]
            let transition_stringify = |(src, (i, (dest, out, update))): (
                u32,
                (
                    &TransInput<InputSymbol, u16>,
                    &(State, OutputSymbol, Option<Update>),
                ),
            )| {
                let input_string = match i {
                    TransInput::Symbol(i) => input_map.get_by_right(i).expect("Safe").clone(),
                    TransInput::TimeOut(x) => format!("to[x{x}]"),
                };
                let output_string = output_map.get_by_right(out).expect("Safe");
                let update_string = if let Some(update) = update {
                    let id = *update.id();
                    let val = *update.value();
                    format!("x{} := {}", id, val)
                } else {
                    String::from("⊥")
                };
                format!(
                    "t{} -> t{} [label=\"{} / {} / {}\"];",
                    src,
                    dest.raw(),
                    input_string,
                    output_string,
                    update_string
                )
            };
            states_range
                .flat_map(transitions)
                .map(transition_stringify)
                .join("\n")
        }
    }

    const DOT_HEADER: &str = "digraph g {\n";
    const DOT_FOOTER: &str = r#"__start0 [label="" shape="none" width="0" height="0"];
__start0 -> t0;"#;
    const GRAPH_END_BRACE: &str = "}";
    const BASIS_NODE: &str = "node[shape=circle; color=red;]";
    const FRONTIER_NODE: &str = "node[shape=circle; color=blue;]";
}
