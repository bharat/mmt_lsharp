use datasize::DataSize;
use serde::Serialize;

#[derive(Debug, DataSize, Serialize, Clone)]
pub struct Node<T, P> {
    pub parent: Option<(P, usize)>,
    pub val: T,
}

impl<T, P> Node<T, P> {
    pub fn new(val: T) -> Self {
        Self { val, parent: None }
    }
}

#[derive(Debug, DataSize, Serialize, Clone)]
pub struct ArenaTree<T, P> {
    pub arena: Vec<Node<T, P>>,
}

impl<T, P> Default for ArenaTree<T, P> {
    fn default() -> Self {
        let arena = Vec::with_capacity(1000);
        Self { arena }
    }
}

impl<T, P> ArenaTree<T, P> {
    /// Insert the new node `val` and return its index.
    pub fn node(&mut self, val: T) -> usize {
        let idx = self.size();
        self.arena.push(Node::new(val));
        idx
    }

    /// Insert the new node `val` with the parent index as `p_idx` and return its index.
    pub fn node_with_parent(&mut self, val: T, p_idx: usize, input: P) -> usize {
        let idx = self.arena.len();
        self.arena.push(Node {
            val,
            parent: Some((input, p_idx)),
        });
        idx
    }

    pub fn mut_ref_at(&mut self, idx: impl Into<usize>) -> &mut T {
        &mut self.arena[idx.into()].val
    }

    /// Get borrow for node at `idx`.
    pub fn ref_at(&self, idx: impl Into<usize>) -> &T {
        &self.arena[idx.into()].val
    }

    /// Returns the number of nodes in the tree.
    pub fn size(&self) -> usize {
        self.arena.len()
    }
}
