use crate::timed_machine::{machine::TimedMachine, parser::MMTIO};

use super::find_shortest_separating_sequence;

#[test]
fn should_be_equal() -> Result<(), std::io::Error> {
    let hyp = read_mmt_from_file("./src/equivalence/tests/apart_ex1_hyp.dot")?;
    let sul = read_mmt_from_file("./src/equivalence/tests/apart_ex1.dot")?;
    let cex = find_shortest_separating_sequence(&hyp, &sul);
    assert!(cex.is_none());
    Ok(())
}

fn read_mmt_from_file(p: &str) -> Result<TimedMachine, std::io::Error> {
    let model_str = std::fs::read_to_string(p)?;
    let mmtio = MMTIO::from_dot(model_str);
    let mmt_parse = mmtio.try_build();
    let mmt = mmt_parse.get_mmt();
    Ok(mmt)
}
