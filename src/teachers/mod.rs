mod symbolic;

pub use self::symbolic::SymInput;
pub use self::symbolic::Teacher as SymbolicTeacher;
