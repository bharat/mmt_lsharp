use std::num::NonZeroUsize;

use derive_more::{Constructor, IsVariant, Unwrap};
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol};

use crate::{equivalence::find_shortest_separating_sequence, timed_machine::machine::TimedMachine};

/// A symbolic teacher, directly answering queries
/// using an MMT.
#[derive(Debug, Constructor)]
pub struct Teacher<'machine> {
    mmt: &'machine TimedMachine,
}

#[derive(Debug, IsVariant, Unwrap, Clone, Copy)]
pub enum SymInput {
    /// Basic input symbol.
    Sym(InputSymbol),
    /// Indicates the **index** of the symbol in a sequence of `SymInput`s which set this timer.
    ///
    /// That is, TimeOut(2) indicates that the timer set in the transition taken by the second input
    /// timed out.
    TimeOut(u16),
}

impl<'a> Teacher<'a> {
    /// For the state reached by `access_seq`,
    /// return the output symbol for input `symbol`.
    pub fn output_query(&self, access_seq: &[SymInput], symbol: SymInput) -> OutputSymbol {
        self.mmt.symbolic_output_query(access_seq, symbol)
    }

    /// Return the timers active at the state reachable by `access_seq`.
    pub fn wait_query(&self, access_seq: &[SymInput]) -> Vec<(usize, NonZeroUsize)> {
        self.mmt.symbolic_wait_query(access_seq)
    }

    /// Find a CE for the `hypothesis`, if one exists.
    pub fn equivalence_query(&self, hypothesis: &TimedMachine) -> Option<Vec<SymInput>> {
        find_shortest_separating_sequence(hypothesis, self.mmt)
    }
}
