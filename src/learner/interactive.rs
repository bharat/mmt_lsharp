mod commands;
mod hypothesis;
mod process_ce;
mod replay;
mod timer_equivalence;

use core::fmt;
use std::fs::File;
use std::process::exit;

use bimap::BiHashMap;
use delegate::delegate;
use fnv::FnvHashSet;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use lsharp_ru::util::toolbox;
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use rustc_hash::{FxHashMap, FxHashSet};

use crate::apartness::{spanning_sequence, states_are_apart_via};
use crate::matching::{AllMatches, Matching};
use crate::obs_tree::ObservationTree;
use crate::teachers::{SymInput, SymbolicTeacher};
use crate::timed_machine::machine::{TimedMachine, TransInput};

pub use self::commands::LearnerCmds;

#[derive(Debug)]
pub struct Learner<'machine> {
    obs_tree: &'machine mut ObservationTree,
    sul: &'machine SymbolicTeacher<'machine>,
    basis: FxHashSet<State>,
    num_input_symbols: usize,
    /// Corresponds to the set `R` in the paper.
    /// Its a set of states for which we (think we) know all immediate timers.
    enabled_timers_known: FxHashSet<State>,
    /// Map from a state to the set of timer ids which are active in that state.
    active_timers: FxHashMap<State, FxHashSet<u16>>,
    /// Frontier -> Basis's -> [Matching]s map.
    frontier_basis_map: LoopbackMap,

    // Utilities
    /// Record of the previous matchings used.
    ///
    /// During the construction of a hypothesis, for each
    /// frontier state, we store its corresponding basis state
    /// and the matching used to map to the basis state.
    /// This is useful when we have multiple matchings to the
    /// same basis state, and also for hypothesis construction.
    hyp_matchings: FxHashMap<State, (State, Matching)>,

    num_oqs: usize,
    num_rounds: usize,
}

impl<'machine> Learner<'machine> {
    pub fn new(
        obs_tree: &'machine mut ObservationTree,
        sul: &'machine SymbolicTeacher<'machine>,
        num_input_symbols: usize,
    ) -> Self {
        let basis = [State::new(0)].into_iter().collect();
        let enabled_timers_known = [State::new(0)].into_iter().collect();
        let active_timers = FxHashMap::default();
        let frontier_basis_map = LoopbackMap::default();
        let hyp_matchings = FxHashMap::default();
        let num_oqs = 0;
        Self {
            obs_tree,
            sul,
            basis,
            enabled_timers_known,
            active_timers,
            frontier_basis_map,
            num_input_symbols,
            hyp_matchings,
            num_oqs,
            num_rounds: 1, //Number of eqs.
        }
    }

    /// Return the number of EQs by the Learner.
    pub fn num_rounds(&self) -> usize {
        self.num_rounds
    }

    /// Return the number of output queries and wait queries performed
    /// by the Learner.
    pub fn num_queries(&self) -> (usize, usize) {
        let num_wqs = self.enabled_timers_known.len() - 1;
        (self.num_oqs, num_wqs)
    }

    pub fn symbolic_output_query(&mut self, state: State, input: SymInput) {
        // We need to get the access sequence of this state and then construct a
        // symbolic query for the Teacher.
        let access_seq = self.obs_tree.get_access_seq(state);
        let symbolic_acc = self.to_symbolic_sequence(&access_seq);
        // We also need to convert the symbolic input to a normal input.
        let normal_input = {
            match input {
                SymInput::Sym(i) => TransInput::Symbol(i),
                SymInput::TimeOut(idx) => {
                    // We need to check which timer was started in transition `idx`.
                    let x = self.obs_tree.which_timer_set_at(&access_seq, idx.into());
                    TransInput::TimeOut(x)
                }
            }
        };
        if self
            .observation_tree()
            .all_transitions_at(state)
            .contains_key(&normal_input)
        {
            return;
        }
        self.num_oqs += 1;
        let output = self.sul.output_query(&symbolic_acc, input);
        self.obs_tree
            .insert_observation_from(state, normal_input, output, None);
        self.update_active_timers();
    }

    pub fn symbolic_wait_query(&mut self, state: State) {
        // Don't do anything if we know all the enabled timers at this state.
        if self.enabled_timers_known.contains(&state) {
            return;
        }
        let access_seq = self.obs_tree.get_access_seq(state);
        let seq_states = self.obs_tree.get_seq_states(&access_seq);
        let symbolic_acc = self.to_symbolic_sequence(&access_seq);
        let timers = self.sul.wait_query(&symbolic_acc);

        timers
            .iter()
            .map(|(tr_num, val)| {
                let spair = seq_states.get(*tr_num).expect("Safe");
                (*spair, *val)
            })
            .for_each(|(spair, val)| {
                self.obs_tree.create_update_between(spair, val);
            });

        for (tim_idx, _) in timers {
            self.symbolic_output_query(state, SymInput::TimeOut(tim_idx as u16));
            // I've taken the lazy approach here for the trick for collecting the
            // outputs of the WQ, so I'm correcting for that here.
            // A WQ can be optimised to provide outputs as well, but I don't do that
            // so we need to correct for it here.
            self.num_oqs -= 1;
        }
        self.enabled_timers_known.insert(state);
        self.update_active_timers();
    }

    pub fn observation_tree(&self) -> &ObservationTree {
        &self.obs_tree
    }

    /// Update the set of active timers in all states of the observation tree.
    pub fn update_active_timers(&mut self) {
        let all_states = {
            let num_states = self.observation_tree().len();
            (0..num_states).map(|x| State::new(x as u32)).collect_vec()
        };
        self.active_timers = all_states
            .into_par_iter()
            .map(|s| (s, self.observation_tree().get_active_timers_at(s)))
            .collect();
    }

    /// Update the apartness relation for the *entire* basis and frontier.
    fn update_apartness_relation(&mut self) {
        self.update_active_timers();
        self.frontier_basis_map.inner.clear();
        let mut new_basis = FxHashSet::default();
        loop {
            // Add all non-basis successors to the frontier-basis-map.
            'basis_discovered: for bs in &self.basis {
                // Successors for bs.
                let suckers: Vec<_> = self
                    .obs_tree
                    .all_transitions_at(*bs)
                    .values()
                    .map(|(d, _, _)| *d)
                    .collect();
                for fs in suckers {
                    if self.basis.contains(&fs) || !self.enabled_timers_known.contains(&fs) {
                        continue;
                    }
                    let fs_timers = self.active_timers.get(&fs).expect("Safe");
                    // Now we need to construct the apartness relation between `fs` and the basis.
                    // This will let us make the candidate mapping.
                    let cand_map: FxHashMap<_, _> = self
                        .basis
                        .iter()
                        .filter_map(|cand| {
                            let cand_timers = self.active_timers.get(cand).expect("Safe");
                            let all_matchings =
                                AllMatches::new(fs_timers.clone(), cand_timers.clone());
                            let wit_for_matching = |matching| {
                                states_are_apart_via(
                                    &self.obs_tree,
                                    fs,
                                    *cand,
                                    &self.active_timers,
                                    matching,
                                    &self.enabled_timers_known,
                                )
                            };
                            let cand_matchings: FnvHashSet<_> = all_matchings
                                .maximal_matchings()
                                .filter(|m| wit_for_matching(m).is_none())
                                .cloned()
                                .collect();
                            if !cand_matchings.is_empty() {
                                Some((*cand, cand_matchings))
                            } else {
                                None
                            }
                        })
                        .collect();
                    if cand_map.is_empty() {
                        // We know that this is now a new basis state.
                        new_basis.insert(fs);
                        break 'basis_discovered;
                    }
                    self.frontier_basis_map.add_frontier(fs, cand_map);
                }
            }
            if new_basis.is_empty() {
                break;
            }
            self.basis.extend(new_basis.drain());
        }
        for s in &self.basis {
            self.frontier_basis_map.remove(s);
        }
    }

    /// For each basis state, ensure that we have all the successor states
    /// and that we have ran WQs for them.
    fn extend_basis(&mut self) {
        let all_basis = self.basis.clone();
        for bs in all_basis {
            // Run all the OQs.
            for i in toolbox::inputs_iterator(self.num_input_symbols).map(InputSymbol::from) {
                self.symbolic_output_query(bs, SymInput::Sym(i));
            }

            for i in toolbox::inputs_iterator(self.num_input_symbols).map(InputSymbol::from) {
                let (fs, _, _) = self
                    .observation_tree()
                    .all_transitions_at(bs)
                    .get(&TransInput::Symbol(i))
                    .expect("Safe");
                let fs = *fs;
                self.symbolic_wait_query(fs);
            }
        }
        let old_basis_size = self.basis.len();
        self.update_apartness_relation();
        if self.basis.len() > old_basis_size {
            self.extend_basis();
        }

        let all_basis = self.basis.clone();
        for bs in all_basis {
            let all_timeout_fss = self
                .observation_tree()
                .all_transitions_at(bs)
                .iter()
                .filter(|(k, _)| k.is_time_out())
                .map(|(_, (fs, _, _))| *fs)
                .collect_vec();

            for fs in all_timeout_fss {
                self.symbolic_wait_query(fs);
            }
        }
        let old_basis_size = self.basis.len();
        self.update_apartness_relation();
        if self.basis.len() > old_basis_size {
            self.extend_basis();
        }
    }

    fn replay(&mut self) {
        // Now, what we want to do is that if there exists a frontier state with
        // matchings to two basis states s.t. the witness between the basis states
        // is a non-timer witness (i.e., has a witness consisting of at least one input)
        // we want to replay that witness on the frontier state.
        let fss = self
            .frontier_basis_map
            .frontier_iter()
            .copied()
            .collect_vec();

        for fs in fss {
            let cands_matchings = self
                .frontier_basis_map
                .get_candidates_mut(&fs)
                .expect("Frontier state should have at least one basis candidate.")
                .clone();
            if cands_matchings.len() == 1 {
                continue;
            }
            let timers_at = |timers: &FxHashMap<_, FxHashSet<_>>, s| {
                timers.get(s).expect("Safe").iter().copied().collect_vec()
            };
            let cands_with_witnesses = cands_matchings
                .keys()
                .tuple_combinations()
                .filter_map(|(q, r)| {
                    let q_timers = timers_at(&self.active_timers, q);
                    let r_timers = timers_at(&self.active_timers, r);
                    let matching_generator = AllMatches::new(q_timers, r_timers);
                    let all_matches = matching_generator.maximal_matchings().collect_vec();
                    let wit = all_matches
                        .into_iter()
                        .filter_map(|matching| {
                            states_are_apart_via(
                                &self.obs_tree,
                                *q,
                                *r,
                                &self.active_timers,
                                matching,
                                &self.enabled_timers_known,
                            )
                        })
                        .find(|wit| !wit.is_timer_witness());
                    wit.map(|wit| (*q, *r, wit))
                })
                .collect_vec();
            for (q, r, wit) in &cands_with_witnesses {
                log::info!("Witness between {q} and {r} : {}", wit);
            }

            for (q, _, wit) in cands_with_witnesses {
                // Now we want to replay our witness here.
                let q_matchings = cands_matchings.get(&q).expect("Safe").clone();
                for matching in q_matchings {
                    log::info!("Replay {wit} from {q} at {fs} under {matching}");
                    self.replay_witness(q, fs, &wit, matching);
                }
            }
        }
        self.update_apartness_relation();
    }

    /// If a frontier state has fewer timers than a basis state (or vice-versa), we
    /// try to replay the missing timeout(s) in the former state.
    fn ensure_bijective_matching(&mut self) {
        loop {
            // First, build a collection of the states for which we are missing timeouts,
            // and in relation to which state.
            self.update_active_timers(); // Sanity.

            // We must pick timers which are not present in the matching.
            // Note, our matchings go from frontier to basis timers.
            let mut timer_mismatch = None;
            'new_timeout: for (fs, cand_matchings) in self.frontier_basis_map.iter() {
                let active_fs = self.active_timers.get(fs).expect("Safe");
                for (cand, matchings) in cand_matchings {
                    let active_cand = self.active_timers.get(cand).expect("Safe");
                    match active_fs.len().cmp(&active_cand.len()) {
                        std::cmp::Ordering::Equal => {} // Do nothing
                        std::cmp::Ordering::Less => {
                            // Basis has extra timer(s)
                            for m in matchings {
                                // We want to look for a matching s.t. a timer in the basis
                                // cand is not present in the matching.
                                let useful =
                                    active_cand.iter().find(|x| m.get_by_right(x).is_none());
                                if let Some(cand_timer_id) = useful {
                                    let seq_to_timeout =
                                        spanning_sequence(&self.obs_tree, *cand_timer_id, *cand);
                                    let m_rev: Matching = m.iter().map(|(x, y)| (*y, *x)).collect();
                                    timer_mismatch = Some((*cand, m_rev, seq_to_timeout, *fs));
                                    break 'new_timeout;
                                }
                            }
                        }
                        std::cmp::Ordering::Greater => {
                            // Frontier has extra timer(s)
                            for m in matchings {
                                // We want to look for a matching s.t. a timer in the basis
                                // cand is not present in the matching.
                                let useful = active_fs.iter().find(|x| m.get_by_left(x).is_none());
                                if let Some(fs_timer_id) = useful {
                                    let seq_to_timeout =
                                        spanning_sequence(&self.obs_tree, *fs_timer_id, *fs);
                                    timer_mismatch = Some((*fs, m.clone(), seq_to_timeout, *cand));
                                    break 'new_timeout;
                                }
                            }
                        }
                    }
                }
            }
            if let Some((rich_state, m, seq_to_timeout, poor_state)) = timer_mismatch {
                self.replay_spanning_seq(rich_state, m, poor_state, &seq_to_timeout);
                self.update_apartness_relation();
            } else {
                break;
            }
        }
    }

    fn make_hypothesis(&mut self) -> TimedMachine {
        self._inner_make_hypothesis() // Inner just means that its an inner module.
    }

    fn make_obs_tree_adequate(&mut self) {
        let mut old_tree_size = self.observation_tree().len();
        loop {
            // Ensure that the entire basis is extended.
            self.extend_basis();
            self.update_apartness_relation();
            // We need to ensure that all the frontier and basis states have the same number of timers.
            self.ensure_bijective_matching();
            self.update_apartness_relation();
            self.replay();
            self.update_apartness_relation();

            let new_tree_size = self.observation_tree().len();
            if old_tree_size == new_tree_size {
                break;
            }
            old_tree_size = new_tree_size;
        }
    }

    pub fn learn(
        &mut self,
        input_map: &BiHashMap<String, InputSymbol>,
        output_map: &BiHashMap<String, OutputSymbol>,
    ) -> TimedMachine {
        loop {
            print!("Learning round: {}, ", self.num_rounds);
            self.make_obs_tree_adequate();
            let hyp = self.make_hypothesis();
            let z_hyp = hyp.zone_mmt();
            println!("Hypothesis size: {}", hyp.active_timers().keys().len());
            write_hypothesis(
                &format!("trees/hypothesis_{}.dot", self.num_rounds),
                &z_hyp,
                input_map,
                output_map,
            );
            if let Some(cex) = self.sul.equivalence_query(&hyp) {
                println!("CEX: {:?}", cex);
                self.process_counterexample(&cex);
            } else {
                println!("No CEX found. Stopping learning.");
                return z_hyp;
            }
            self.num_rounds += 1;
        }
    }
}

// ------------------- UTILITY IMPLS ---------------------- //
impl<'machine> Learner<'machine> {
    fn to_symbolic_sequence(&self, original: &[TransInput<InputSymbol, u16>]) -> Vec<SymInput> {
        // The regular inputs can stay as they are, but what we need to do is that the
        // timers which were identified by their names now need to be identified by the
        // index of the transition which set them.
        self.obs_tree.to_symbolic_sequence(original)
    }

    pub fn run(
        &mut self,
        cmd: LearnerCmds,
        input_map: &BiHashMap<String, InputSymbol>,
        output_map: &BiHashMap<String, OutputSymbol>,
    ) {
        let write_tree = |learner: &Self| {
            let frontier = &learner
                .frontier_basis_map
                .frontier_iter()
                .copied()
                .collect();

            let o_tree_string =
                learner
                    .obs_tree
                    .to_dot(input_map, output_map, &learner.basis, frontier);
            std::fs::write("./trees/current.dot", o_tree_string).expect("Could not write tree.");
        };

        match cmd {
            LearnerCmds::OQ(state, input_str) => {
                let i = *input_map.get_by_left(&input_str).expect("Safe");
                let i_symb = SymInput::Sym(i);
                self.symbolic_output_query(state, i_symb);
                write_tree(self);
            }
            LearnerCmds::WQ(state) => {
                self.symbolic_wait_query(state);
                write_tree(self);
            }
            LearnerCmds::ExtendBasis => {
                self.extend_basis();
                write_tree(self);
            }
            LearnerCmds::Frontier(state) => {
                self.basis.remove(&state);
                write_tree(self);
            }
            LearnerCmds::Apart => {
                self.update_apartness_relation();
                println!("Frontier-Basis map:\n{}", self.frontier_basis_map);
                write_tree(self);
            }
            LearnerCmds::Replay => {
                let mut old_size = self.observation_tree().len();
                loop {
                    self.ensure_bijective_matching();
                    self.replay();
                    let new_size = self.observation_tree().len();
                    if new_size == old_size {
                        break;
                    }
                    old_size = new_size;
                }
                write_tree(self);
            }
            LearnerCmds::Wit(s, t) => {
                let witnesss = self.get_all_witnesses_between(s, t);
                println!("Witnesses b/w t{} and t{}:", s.raw(), t.raw());
                for wit in witnesss {
                    println!("{wit}");
                }
            }
            LearnerCmds::ActTim(s) => {
                self.update_active_timers();
                let tims = if let Some(timers) = self.active_timers.get(&s) {
                    timers.iter().map(|x| x.to_string()).join(",")
                } else {
                    String::new()
                };
                println!("Timers at t{}: {{{}}}", s.raw(), tims);
            }
            LearnerCmds::Hyp => {
                let hyp = self.make_hypothesis();
                write_hypothesis("trees/hypothesis.dot", &hyp, input_map, output_map);
            }
            LearnerCmds::Eq => {
                let hyp = self.make_hypothesis();
                write_hypothesis("trees/hypothesis.dot", &hyp, input_map, output_map);
                let cex = self.sul.equivalence_query(&hyp);
                if let Some(ce) = cex {
                    println!("CEX: {:?}", ce);
                    self.process_counterexample(&ce);
                    write_tree(self);
                } else {
                    println!("No CEX found. Learning finished.");
                    println!("Press q-<Enter> to quit.");
                }
            }
            LearnerCmds::Quit => exit(0),
            LearnerCmds::Learn => {
                self.learn(input_map, output_map);
            }
        }
    }
}

fn write_hypothesis(
    file_path: &str,
    mmt: &TimedMachine,
    input_map: &BiHashMap<String, InputSymbol>,
    output_map: &BiHashMap<String, OutputSymbol>,
) {
    let mut hyp_file = File::create(file_path).expect("Could not create hypothesis file");
    let tim_map = mmt
        .active_timers()
        .into_values()
        .flatten()
        .map(|x| (format!("x{x}"), x as usize))
        .collect();
    mmt.write(input_map, output_map, &tim_map, &mut hyp_file)
        .expect("Could not write hypothesis file.");
}

#[derive(Debug, Default)]
struct LoopbackMap {
    inner: FxHashMap<State, FxHashMap<State, FnvHashSet<Matching>>>,
}

impl LoopbackMap {
    delegate! {
        to self.inner   {
            #[call(get)]
            pub fn get_candidates(&self, k: &State) -> Option<&FxHashMap<State,FnvHashSet<Matching>>>;
            #[call(get_mut)]
            pub fn get_candidates_mut(&mut self, k: &State) -> Option<&mut FxHashMap<State,FnvHashSet<Matching>>>;
            #[call(keys)]
            pub fn frontier_iter(&self) -> impl Iterator<Item = &State>;
            #[call(remove)]
            pub fn remove(&mut self, k: &State) -> Option<FxHashMap<State,FnvHashSet<Matching>>>;
            #[call(insert)]
            pub fn add_frontier(&mut self, fs: State, cand_mappings: FxHashMap<State, FnvHashSet<Matching>>) -> Option<FxHashMap<State, FnvHashSet<Matching>>>;
        }
    }

    fn iter(&self) -> impl Iterator<Item = (&State, &FxHashMap<State, FnvHashSet<Matching>>)> {
        self.inner.iter()
    }
}

impl fmt::Display for LoopbackMap {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (fs, cand_matchings) in &self.inner {
            for (bs, matchings) in cand_matchings {
                for matching in matchings {
                    writeln!(
                        f,
                        "(t{} -> t{} under matching {})",
                        fs.raw(),
                        bs.raw(),
                        matching
                    )?;
                }
            }
        }
        Ok(())
    }
}
