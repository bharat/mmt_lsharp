use lsharp_ru::definitions::mealy::State;
use thiserror::Error;

#[derive(Debug)]
pub enum LearnerCmds {
    OQ(State, String),
    WQ(State),
    ExtendBasis,
    Frontier(State),
    Wit(State, State),
    Apart,
    Replay,
    ActTim(State),
    Quit,
    Hyp,
    Eq,
    Learn,
}

#[derive(Debug, Error)]
pub enum LearnerCmdParseError {
    #[error("Unknown command.")]
    UnknownCmd,
    #[error("OQ format not recognised.")]
    OQFmt,
    #[error("WQ format not recognised.")]
    WQFmt,
    #[error("Basis format not recognised.")]
    BasisFmt,
    #[error("Frontier format not recognised.")]
    FrontierFmt,
    #[error("Apartness rule not recognised.")]
    Apartness,
    #[error("No command provided.")]
    NoCmd,
    #[error("Quit requires only `q`.")]
    Quit,
    #[error("Replay rule not recognised.")]
    Replay,
    #[error("Witness command not recognised.")]
    Witness,
    #[error("ActiveTimers format not recognised.")]
    ATFmt,
    #[error("Hypothesis command not recognised.")]
    Hyp,
    #[error("Learn command not recognised.")]
    LearnFmt,
    #[error("EQ command not recognised.")]
    EQFmt,
}

impl TryFrom<String> for LearnerCmds {
    type Error = LearnerCmdParseError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let val_str: &str = &value;
        let val_str = val_str.to_ascii_lowercase();
        let val_str = val_str.trim();

        let mut input_iter = val_str.split_ascii_whitespace();
        let cmd = input_iter.next().ok_or(LearnerCmdParseError::NoCmd)?;
        match cmd {
            "wit" => {
                // Parse witness query
                // Format: t<state-id> t<state-id>
                let s = parse_state(&mut input_iter, LearnerCmdParseError::Witness)?;
                let t = parse_state(&mut input_iter, LearnerCmdParseError::Witness)?;
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::Witness);
                }
                Ok(LearnerCmds::Wit(s, t))
            }
            "wq" => {
                // Parse wait query
                // Format: t<state-id>
                let state = parse_state(&mut input_iter, LearnerCmdParseError::WQFmt)?;
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::WQFmt);
                }
                Ok(LearnerCmds::WQ(state))
            }
            "oq" => {
                // parse output query
                // Format: t<state-id> <input_symbol> (no timeouts!!!)
                let state = parse_state(&mut input_iter, LearnerCmdParseError::OQFmt)?;
                let input_str = input_iter.next().ok_or(LearnerCmdParseError::OQFmt)?;
                let input = input_str.to_string();
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::OQFmt);
                }
                let cmd = LearnerCmds::OQ(state, input);
                Ok(cmd)
            }
            "basis" => {
                // Parse basis cmd
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::BasisFmt);
                }
                Ok(LearnerCmds::ExtendBasis)
            }
            "front" => {
                // Parse frontier cmd
                // Format: t<state-id>
                let state = parse_state(&mut input_iter, LearnerCmdParseError::WQFmt)?;
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::BasisFmt);
                }
                Ok(LearnerCmds::Frontier(state))
            }
            "apart" => Ok(LearnerCmds::Apart),
            "replay" => Ok(LearnerCmds::Replay),
            "act" => {
                // Parse wait query
                // Format: t<state-id>
                let state = parse_state(&mut input_iter, LearnerCmdParseError::ATFmt)?;
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::ATFmt);
                }
                Ok(LearnerCmds::ActTim(state))
            }
            "q" => {
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::Quit);
                }
                Ok(LearnerCmds::Quit)
            }
            "hyp" => {
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::Hyp);
                }
                Ok(LearnerCmds::Hyp)
            }
            "eq" => {
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::EQFmt);
                }
                Ok(LearnerCmds::Eq)
            }
            "learn" => {
                if input_iter.next().is_some() {
                    return Err(LearnerCmdParseError::LearnFmt);
                }
                Ok(LearnerCmds::Learn)
            }
            _ => Err(LearnerCmdParseError::UnknownCmd),
        }
    }
}

fn parse_state<'a, T: Iterator<Item = &'a str>>(
    input_iter: &mut T,
    err_typ: LearnerCmdParseError,
) -> Result<State, LearnerCmdParseError> {
    let state_str = input_iter.next().ok_or(err_typ)?;

    let state_id = &state_str[1..];
    let state: u32 = state_id.parse().map_err(|_| LearnerCmdParseError::OQFmt)?;
    let state = State::new(state);
    Ok(state)
}
