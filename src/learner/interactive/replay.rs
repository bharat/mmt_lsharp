use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, State};
use rustc_hash::FxHashSet;

use crate::apartness::{states_are_apart_via, Witness};
use crate::matching::{AllMatches, Matching};
use crate::teachers::SymInput;
use crate::timed_machine::{machine::TransInput, timers::both_updates_exist_and_are_different};

use super::Learner;

/// Local type alias for `concrete` input type.
type InternalTransInput = TransInput<InputSymbol, u16>;

// ------------------- REPLAY IMPLS ---------------------- //
impl<'machine> Learner<'machine> {
    /// Replay `witness` located at state `origin` along state `dest`.
    /// Returns `true` if successful.
    ///
    /// The `matching` relates active timers of state `dest` to
    /// active timers of state `origin`.
    pub(super) fn replay_witness(
        &mut self,
        origin: State,
        dest: State,
        witness: &Witness,
        matching: Matching,
    ) -> bool {
        // We want to do a WQ for each state along `witness` first.
        self.do_wqs_along(origin, witness);
        // We need to ensure that the active timers at `q` are correct.
        self.update_active_timers();
        let (witrun, extra_wq): WitnessRun = witness.try_into().expect("Safe");
        self.replay_rec(origin, dest, matching, &witrun, extra_wq)
    }

    /// Direct version of `replay_witness` for analysing CEs.
    /// Replay cex sequence located at state `origin` along state `dest`.
    /// Returns `true` if successful.
    ///
    /// The `matching` relates active timers of state `dest` to
    /// active timers of state `origin`.
    pub(super) fn replay_cex_seq(
        &mut self,
        origin: State,
        dest: State,
        seq: &[InternalTransInput],
        m: Matching,
    ) -> bool {
        self.replay_rec(origin, dest, m, seq, true)
    }

    pub(super) fn replay_spanning_seq(
        &mut self,
        q: State,
        m: Matching,
        p: State,
        seq: &[InternalTransInput],
    ) {
        self.replay_rec(q, p, m, seq, true);
    }

    fn do_wqs_along(&mut self, q: State, wit: &Witness) {
        let mut curr = q;
        let (witness, _): WitnessRun = wit.try_into().expect("Safe");
        log::debug!("Running WQs from {q} along {:?}", witness);
        for i in witness {
            (curr, _, _) = self
                .observation_tree()
                .get_output_and_succ(curr, i)
                .expect("Witness not present in basis?");
            self.symbolic_wait_query(curr);
        }
    }

    /// We return `false` if `q` and `p` are apart under the current matching.
    /// In other words, replaying `wit` is impossible with the given arguments.
    fn replay_rec(
        &mut self,
        q: State,
        p: State,
        m: Matching,
        wit: &[InternalTransInput],
        extra_wq: bool,
    ) -> bool {
        let i = wit
            .first()
            .expect("Witness must have at least one element.");
        if i.is_symbol() {
            self.replay_handle_basic(q, p, m, wit, extra_wq)
        } else {
            self.replay_handle_timeout(q, p, m, wit, extra_wq)
        }
    }

    fn replay_handle_basic(
        &mut self,
        q: State,
        p: State,
        matching: Matching,
        wit: &[InternalTransInput],
        extra_wq: bool,
    ) -> bool {
        let (i, witp) = wit.split_first().expect("Safe");
        let i = i.unwrap_symbol(); // Safe!
        self.symbolic_output_query(p, SymInput::Sym(i));
        // The `q` triple will never change.
        let (q_dest, q_out, q_up) = self
            .observation_tree()
            .get_output_and_succ(q, TransInput::Symbol(i))
            .expect("Safe");
        let (p_dest, p_out, p_up) = self
            .observation_tree()
            .get_output_and_succ(p, TransInput::Symbol(i))
            .expect("Safe");

        if states_are_apart_via(
            &self.obs_tree,
            q_dest,
            p_dest,
            &self.active_timers,
            &matching,
            &self.enabled_timers_known,
        )
        .is_some()
        {
            return false;
        }
        // Do the outputs match?
        if q_out != p_out {
            return false;
        }

        // If both updates exist, do the constants match?
        if both_updates_exist_and_are_different(q_up.as_ref(), p_up.as_ref()) {
            return false;
        }

        // Rest of the witness is empty, we're done.
        if witp.is_empty() {
            if extra_wq {
                self.symbolic_wait_query(p_dest);
            }
            return states_are_apart_via(
                &self.obs_tree,
                q_dest,
                p_dest,
                &self.active_timers,
                &matching,
                &self.enabled_timers_known,
            )
            .is_none();
        }

        let rec_matching = if let Some(q_up) = q_up.as_ref() {
            let fresh_timers =
                witness_has_fresh_timer(wit, self.active_timers.get(&q).expect("Safe"));
            let mut rec_matching = matching.clone();
            if fresh_timers.contains(q_up.id()) {
                let new_match = (q_dest.raw() as u16, p_dest.raw() as u16);
                let not_present = rec_matching.insert_no_overwrite(new_match.0, new_match.1);
                if let Err(old_match) = not_present {
                    if old_match != new_match {
                        println!("Old match: {:?}", old_match);
                        println!("New match: {:?}", new_match);
                        panic!("Match already existed!");
                    }
                }
            }
            rec_matching
        } else {
            matching.clone()
        };
        let replayable = self.replay_rec(q_dest, p_dest, rec_matching, witp, extra_wq);
        if !replayable {
            return false;
        } else {
            let (_, _, p_up) = self
                .observation_tree()
                .get_output_and_succ(p, TransInput::Symbol(i))
                .expect("Safe");
            if both_updates_exist_and_are_different(q_up.as_ref(), p_up.as_ref()) {
                return false;
            }
        }
        // We have replayed the suffix successfully.
        true
    }

    fn replay_handle_timeout(
        &mut self,
        q: State,
        p: State,
        matching: Matching,
        wit: &[InternalTransInput],
        extra_wq: bool,
    ) -> bool {
        let (i, witp) = wit.split_first().expect("Safe");
        let i = i.unwrap_time_out(); // Safe!
        self.symbolic_wait_query(p);
        self.update_active_timers();
        let enabled_timers_p: FxHashSet<_> = self
            .observation_tree()
            .all_transitions_at(p)
            .keys()
            .filter(|x| x.is_time_out())
            .map(|x| x.unwrap_time_out())
            .collect();
        let enabled_timers_q: FxHashSet<_> = self
            .observation_tree()
            .all_transitions_at(q)
            .keys()
            .filter(|x| x.is_time_out())
            .map(|x| x.unwrap_time_out())
            .collect();

        // Number of enabled timers is different, cannot replay.
        if enabled_timers_p.len() != enabled_timers_q.len() {
            return false;
        }

        if let Some(j) = matching.get_by_left(&i) {
            let j = *j;
            self.replay_timeouts(q, i, p, j, &matching, witp, extra_wq)
        } else {
            // Timer id `i` is not matched in either the extension or the original matching.
            let free_timers = enabled_timers_p
                .iter()
                .filter(|x| matching.get_by_right(x).is_none())
                .copied()
                .collect_vec();
            for j in free_timers {
                let mut new_matching = matching.clone();
                let ass = new_matching.insert_no_overwrite(i, j);
                assert!(ass.is_ok(), "Ended up matching one timer twice!");

                if self.replay_timeouts(q, i, p, j, &new_matching, witp, extra_wq) {
                    return true;
                }
            }
            false
        }
    }

    fn replay_timeouts(
        &mut self,
        q: State,
        xi: u16,
        p: State,
        xj: u16,
        matching: &Matching,
        witp: &[InternalTransInput],
        extra_wq: bool,
    ) -> bool {
        let (q_dest, q_out, q_up) = self
            .observation_tree()
            .get_output_and_succ(q, TransInput::TimeOut(xi))
            .expect("Safe");
        let Some((p_dest, p_out, p_up)) = self
            .observation_tree()
            .all_transitions_at(p)
            .get(&TransInput::TimeOut(xj))
            .cloned()
        else {
            return false;
        };
        if states_are_apart_via(
            &self.obs_tree,
            q_dest,
            p_dest,
            &self.active_timers,
            matching,
            &self.enabled_timers_known,
        )
        .is_some()
        {
            return false;
        }
        if q_out != p_out {
            return false;
        }
        // let timers_apart = {
        //     let mut comb_matching = matching.clone();
        //     for (x, y) in matching.clone().into_iter() {
        //         let ass = comb_matching.insert_no_overwrite(x, y);
        //         assert!(ass.is_ok(), "Ended up matching one timer twice!");
        //     }
        //     apart_by_timer_apartness(&self.obs_tree, q, p, &comb_matching).is_some()
        // };
        // if timers_apart {
        // return false;
        // }
        if both_updates_exist_and_are_different(q_up.as_ref(), p_up.as_ref()) {
            return false;
        }
        let replayable = if witp.is_empty() {
            // Rest of the witness is empty, we're done.
            if extra_wq {
                self.symbolic_wait_query(p_dest);
            }
            states_are_apart_via(
                &self.obs_tree,
                q_dest,
                p_dest,
                &self.active_timers,
                matching,
                &self.enabled_timers_known,
            )
            .is_none()
        } else {
            self.replay_rec(q_dest, p_dest, matching.clone(), witp, extra_wq)
        };
        // By now, either p_up doesn't exist or both timers have the same constant.
        if replayable {
            let Some((_, _, p_up)) = self
                .observation_tree()
                .all_transitions_at(p)
                .get(&TransInput::TimeOut(xj))
                .cloned()
            else {
                return false;
            };
            if both_updates_exist_and_are_different(q_up.as_ref(), p_up.as_ref()) {
                return false;
            }
        } else {
            return false;
        }
        // We managed to get here, thus we have replayed the suffix successfully!
        true
    }

    pub(super) fn get_all_witnesses_between(&self, s: State, t: State) -> Vec<Witness> {
        let Some(left_timers) = self.active_timers.get(&s) else {
            return Vec::new();
        };
        let Some(right_timers) = self.active_timers.get(&t) else {
            return Vec::new();
        };
        let active_timers = &self.active_timers;
        let enabled_timers = &self.enabled_timers_known;
        let apart_under =
            |m| states_are_apart_via(&self.obs_tree, s, t, active_timers, m, enabled_timers);
        let all_matches =
            AllMatches::new(left_timers.iter().copied(), right_timers.iter().copied());
        let witnesses = all_matches
            .maximal_matchings()
            .filter_map(apart_under)
            .collect();
        witnesses
    }
}

pub(crate) type WitnessRun = (Vec<TransInput<InputSymbol, u16>>, bool);

pub(crate) fn witness_has_fresh_timer(
    witness: &[InternalTransInput],
    active_timers: &FxHashSet<u16>,
) -> FxHashSet<u16> {
    witness
        .iter()
        .filter(|i| i.is_time_out())
        .map(|i| i.unwrap_time_out())
        .filter(|id| !active_timers.contains(id))
        .collect()
}

impl TryFrom<&Witness> for WitnessRun {
    type Error = &'static str;

    fn try_from(value: &Witness) -> Result<Self, Self::Error> {
        if value.is_timer_witness() {
            return Err("We cannot convert a timer-based witness to a non-timer witness.");
        }
        let mut witness = Vec::new();
        let mut extra_wq = false;
        let mut curr_wit = value;
        loop {
            match curr_wit {
                Witness::Output(x, _) => {
                    witness.push(*x);
                    break;
                }
                Witness::Constant(x, _, seq) => {
                    witness.push(*x);
                    witness.extend(seq.iter().copied());
                    break;
                }
                Witness::Recursive(x, _, z) => {
                    curr_wit = z;
                    witness.push(*x);
                }
                Witness::MissingTimeout(pair, _) => {
                    witness.push(pair.0);
                    break;
                }
                Witness::NumEnabledTimers => {
                    extra_wq = true;
                    break;
                }
                _ => {
                    unreachable!("Tried to convert a timerwitness to a non-timer witness.")
                }
            };
        }
        Ok((witness, extra_wq))
    }
}
