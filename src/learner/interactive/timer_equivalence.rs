use std::fmt::Display;

use disjoint::DisjointSet;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::State;
use rustc_hash::{FxHashMap, FxHashSet};
use thiserror::Error;

use crate::matching::Matching;

#[derive(Debug, Error)]
pub(super) enum TimerError {
    #[error("Timers {0} and {1} are apart.")]
    TimersApart(u16, u16),
}

#[derive(Debug, Clone)]
pub(super) struct TimerEquivRel<'a> {
    /// A Disjoint-Set containing the equivalence classes for the timers.
    classes: DisjointSet,
    active_timers: &'a FxHashMap<State, FxHashSet<u16>>,
}

impl<'a> Display for TimerEquivRel<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let equiv = self
            .classes
            .sets()
            .into_iter()
            .map(|class| class.into_iter().map(|x| x.to_string()).join(","))
            .map(|x| format!("{{{}}}", x))
            .join(" , ");
        writeln!(f, "{equiv}")?;
        Ok(())
    }
}

impl<'a> TimerEquivRel<'a> {
    pub fn new(active_timers: &'a FxHashMap<State, FxHashSet<u16>>) -> Self {
        let max_num = active_timers.values().flatten().max().copied().unwrap_or(1);
        let max_num = usize::from(max_num) + 1;
        let classes = DisjointSet::with_len(max_num);
        Self {
            classes,
            active_timers,
        }
    }

    /// Add matchings from an iterator into the PER.
    pub fn add_matchings<'b, I: IntoIterator<Item = &'b Matching>>(
        &mut self,
        iter: I,
    ) -> Result<(), TimerError> {
        for m in iter.into_iter() {
            self.add_matching(m)?;
        }
        Ok(())
    }

    /// Add a matching to the PER. We return a `TimerError` if a pair in the
    /// matching is apart, or addition of a pair into the PER joins two
    /// apart timers in the same equivalence class.
    pub fn add_matching(&mut self, m: &Matching) -> Result<(), TimerError> {
        for (x, y) in m.into_iter() {
            self.insert(*x, *y)?;
        }
        Ok(())
    }

    fn insert(&mut self, x: u16, y: u16) -> Result<(), TimerError> {
        // We essentially need to insert both of these symbols into the set.
        // First, we check if the matching is valid, as a sanity test.
        if self.timers_are_apart((&x, &y)) {
            return Err(TimerError::TimersApart(x, y));
        }

        // Now we can join the two.
        self.classes.join(x as usize, y as usize);
        self.check_all_timer_apartnesses()?;
        Ok(())
    }

    fn timers_are_apart(&self, (x, y): (&u16, &u16)) -> bool {
        if x == y {
            return false;
        }
        self.active_timers
            .values()
            .any(|active_set| active_set.contains(x) && active_set.contains(y))
    }

    fn check_all_timer_apartnesses(&self) -> Result<(), TimerError> {
        for active_set in self.active_timers.values() {
            for (x, y) in active_set.iter().tuple_combinations() {
                if x == y {
                    continue;
                }
                if self.classes.root_of((*x).into()) == self.classes.root_of((*y).into()) {
                    return Err(TimerError::TimersApart(*x, *y));
                }
            }
        }
        Ok(())
    }

    pub fn get_rep_timer_of(&self, x: u16) -> u16 {
        let root = self.classes.root_of(x as usize);
        root as u16
    }
}
