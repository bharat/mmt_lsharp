use bimap::BiHashMap;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, State};
use rustc_hash::{FxHashMap, FxHashSet};

use crate::{matching::Matching, teachers::SymInput, timed_machine::machine::TransInput};

use super::{timer_equivalence::TimerEquivRel, Learner};

type InternalTransInput = TransInput<InputSymbol, u16>;

impl<'machine> Learner<'machine> {
    pub(super) fn process_counterexample(&mut self, counter_example: &[SymInput]) {
        log::info!("Symbolic CEX: {:?}", counter_example);
        log::info!("FBM: {:?}", self.hyp_matchings);
        let per = self.reconstruct_per();
        log::info!("PER: {}", per);
        let (fs, concrete_seq) = self.replay_symbolic_sequence(counter_example);
        self.update_active_timers();

        // We don't need to pass along the FBM, since that is
        // now a field in the Learner.
        self.reduce_ce(fs, &concrete_seq);
    }

    fn reduce_ce(&mut self, fs: State, conc_seq: &[InternalTransInput]) {
        log::info!("fs: {}, conc_seq:{:?}", fs, conc_seq);
        self.update_active_timers();
        let (bs, fs_to_bs_m) = self.hyp_matchings.get(&fs).expect("Safe").clone();
        // We no longer have a maximal matching, so we're done.
        if self.active_timers.get(&fs).expect("Safe") != self.active_timers.get(&bs).expect("Safe")
        {
            return;
        } else {
            // Update the apartness relation and then try to replay the `conc_seq` at `fs` from `bs.`
            self.update_apartness_relation();
            if self.frontier_basis_map.get_candidates(&fs).is_none() {
                // `fs` is no longer a frontier state, return.
                return;
            } else {
                // Now we want to replay `conc_seq` from `bs`.
                let m: Matching = fs_to_bs_m.iter().map(|(x, y)| (*y, *x)).collect();
                self.replay_cex_seq(fs, bs, conc_seq, m);
                self.update_apartness_relation();
                if self.frontier_basis_map.get_candidates(&fs).is_none() {
                    return;
                } else {
                    // Now we need to recurse.
                    // So we first get the frontier state and the concrete sequence we need to use.
                    let (rec_fs, rec_seq) = self.find_frontier_and_shortened_seq(bs, conc_seq);
                    return self.reduce_ce(rec_fs, &rec_seq);
                }
            }
        }
    }

    fn find_frontier_and_shortened_seq(
        &self,
        bs: State,
        conc_seq: &[InternalTransInput],
    ) -> (State, Vec<InternalTransInput>) {
        // We are given a basis state, we just need to walk along the observation tree using `conc_seq` until we
        // get to a frontier state.
        let mut curr_state = bs;
        let mut input_iterator = conc_seq.iter().copied();
        loop {
            let i = input_iterator
                .next()
                .expect("Finished the concrete sequence before finding a frontier state");
            let (d, _, _) = self
                .observation_tree()
                .get_output_and_succ(curr_state, i)
                .expect("Observation tree missing entry.");
            if self.hyp_matchings.contains_key(&d) {
                let fs = d;
                let conc_seq = input_iterator.collect_vec();
                return (fs, conc_seq);
            } else {
                curr_state = d;
            }
        }
    }

    /// Replay the entire symbolic sequence `seq` from the initial state of the observation tree.
    /// Returns the frontier state and the concrete sequence from the frontier state.
    fn replay_symbolic_sequence(&mut self, seq: &[SymInput]) -> (State, Vec<InternalTransInput>) {
        let frontier_states: FxHashSet<_> =
            self.frontier_basis_map.frontier_iter().copied().collect();
        let mut curr_state = State::new(0);

        // We will flip this as soon as we've reached the frontier state, since we have to
        // return a "concrete" sequence later.
        let mut outside_frontier = false;
        let mut concrete_seq = Vec::new();
        // I've made this mutable, as there is no way to let the compiler know that
        // frontier_state will always be initialised to *something*, since all assignments are
        // hidden under conditionals.
        let mut frontier_state = curr_state;
        let mut idx_state_input = FxHashMap::default();
        let mut idx_tid_map = BiHashMap::new();
        for (sym_i, curr_idx) in seq.iter().zip(1u16..) {
            // We *always* do a WQ for each step of the sequence.
            // println!("Input: {:?}, index: {}", sym_i, curr_idx);
            self.symbolic_wait_query(curr_state);
            match *sym_i {
                SymInput::Sym(i) => {
                    let sym_i = SymInput::Sym(i);
                    let conc_i = InternalTransInput::Symbol(i);
                    idx_state_input.insert(curr_idx, (curr_state, conc_i));
                    self.symbolic_output_query(curr_state, sym_i);
                    let (dest_state, _, update) = self
                        .observation_tree()
                        .get_output_and_succ(curr_state, conc_i)
                        .expect(TREE_MISSING_OQ);
                    if let Some(update) = update {
                        let tim_id = *update.id();
                        idx_tid_map.insert(curr_idx, tim_id);
                    }
                    if outside_frontier {
                        concrete_seq.push(conc_i);
                    }
                    if !outside_frontier && frontier_states.contains(&dest_state) {
                        frontier_state = dest_state;
                        // from the next input, we will start recording the concrete inputs
                        // and we don't need to do the check for the frontier anymore.
                        outside_frontier = true;
                    }
                    curr_state = dest_state;
                }

                // timer location, i.e., the index of the transition which set this timer.
                SymInput::TimeOut(tim_loc) => {
                    // let tim_id = *idx_tid_map
                    //     .get_by_left(&tim_loc)
                    //     .expect(TIMER_ID_LOC_MISSING);

                    // If we have a case where the hypothesis has an extra timer, we will
                    // not do anything here (since we have already ran a WQ for `curr_state`).
                    // Just return.
                    let curr_state_timers = self
                        .observation_tree()
                        .all_transitions_at(curr_state)
                        .keys()
                        .filter(|i| i.is_time_out())
                        .count();
                    if curr_state_timers == 0 {
                        assert!(
                            outside_frontier,
                            "We should be outside the frontier by now."
                        );
                        return (frontier_state, concrete_seq);
                    }
                    let tim_id = idx_state_input
                        .get(&tim_loc)
                        .map(|(s, i)| self.observation_tree().all_transitions_at(*s).get(i))
                        .flatten()
                        .map(|(_, _, up)| up.as_ref())
                        .flatten()
                        .map(|up| *up.id())
                        .expect(TIMER_ID_LOC_MISSING);
                    let conc_i = InternalTransInput::TimeOut(tim_id);
                    idx_state_input.insert(curr_idx, (curr_state, conc_i));
                    let (dest_state, _, update) = self
                        .observation_tree()
                        .get_output_and_succ(curr_state, conc_i)
                        .expect(TREE_MISSING_OQ);
                    if let Some(update) = update {
                        let tim_id = *update.id();
                        let timer_restarted = idx_tid_map.insert(curr_idx, tim_id).did_overwrite();
                        assert!(timer_restarted, "{}", WRONG_TIMER_RESTARTED);
                    }
                    if outside_frontier {
                        concrete_seq.push(conc_i);
                    }
                    if !outside_frontier && frontier_states.contains(&dest_state) {
                        frontier_state = dest_state;
                        // from the next input, we will start recording the concrete inputs
                        // and we don't need to do the check for the frontier anymore.
                        outside_frontier = true;
                    }
                    curr_state = dest_state;
                }
            }
        }
        (frontier_state, concrete_seq)
    }

    fn reconstruct_per(&self) -> TimerEquivRel {
        let mut per = TimerEquivRel::new(&self.active_timers);
        for (_, (_, m)) in &self.hyp_matchings {
            per.add_matching(m).expect("Safe");
        }
        per
    }
}

const TREE_MISSING_OQ: &str = "Observation tree did not have results of an OQ.";
const TIMER_ID_LOC_MISSING: &str =
    "Timer location entry not found in the timer location <-> timer id map.";
const WRONG_TIMER_RESTARTED: &str = "Timeout transition update started a fresh timer!.";
