use fnv::FnvHashMap;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::State;
use rustc_hash::{FxHashMap, FxHashSet};

use crate::timed_machine::machine::{TimedMachine, TransInput};
use crate::timed_machine::timers::Update;

use super::{timer_equivalence::TimerEquivRel, Learner};

impl<'machine> Learner<'machine> {
    pub(super) fn _inner_make_hypothesis(&mut self) -> TimedMachine {
        // Get rid of the old matchings.
        self.hyp_matchings.clear();
        // First, we need to make a PER of all the timers: it is overkill for our case,
        // but its better for implementation, since the other multi-matching
        // will end up being similar and I can later generalise this function more easily.
        assert!(
            !self.frontier_basis_map.inner.is_empty(),
            "FBM should never be empty."
        );
        let single_matching_case = self
            .frontier_basis_map
            .iter()
            .map(|(fs, cand_matchings)| {
                // We can have only one candidate state, with possibly multiple matchings for it.
                if cand_matchings.is_empty() {
                    panic!("Frontier state {fs} is not mapped to any basis states");
                }
                cand_matchings
                    .iter()
                    .exactly_one()
                    .expect(FS_MAPPED_MULT_BASIS)
            })
            .map(|(_, matchings)| matchings.len())
            .all(|num_matchings| num_matchings == 1);

        if single_matching_case {
            self.hypothesis_one_matching()
        } else {
            todo!("We have not implemented the gMMT case.")
        }
    }

    fn hypothesis_one_matching(&mut self) -> TimedMachine {
        let fbm = self.make_fbm();
        let per = self.make_per();

        let basis: Vec<_> = self.basis.clone().into_iter().collect();
        let mut transition_map = FnvHashMap::default();
        for &bs in &basis {
            let bs_transitions = self.observation_tree().all_transitions_at(bs);
            for (i, (dest, out, up)) in bs_transitions {
                let input = match i {
                    TransInput::Symbol(i) => TransInput::Symbol(*i),
                    TransInput::TimeOut(x) => TransInput::TimeOut(per.get_rep_timer_of(*x)),
                };

                let dest_hyp = *fbm.get(dest).unwrap_or(dest);
                let out = *out;
                let update = if let Some(update) = up {
                    let (id, val) = update.clone().dissolve();
                    let rep_id = per.get_rep_timer_of(id);
                    Update::new(rep_id, val).into()
                } else {
                    None
                };

                let k = (bs, input);
                let v = (dest_hyp, out, update);
                transition_map.insert(k, v);
            }
        }
        let states = basis.into_iter().collect();
        let inputs = FxHashSet::default();
        let outputs = FxHashSet::default();
        let initial_state = State::new(0);
        let transitions = transition_map;
        TimedMachine::new(inputs, outputs, states, initial_state, transitions)
    }

    /// Construct the frontier-basis map we're using.
    fn make_fbm(&mut self) -> FxHashMap<State, State> {
        let mut fbm = FxHashMap::default();

        for (fs, cand_mappings) in self.frontier_basis_map.iter() {
            let (cand, m) = cand_mappings
                .iter()
                .exactly_one()
                .expect(FS_MAPPED_MULT_BASIS);
            let m = m.iter().exactly_one().expect(FS_MAPPED_MULT_MATCH).clone();
            self.hyp_matchings.insert(*fs, (*cand, m));
            fbm.insert(*fs, *cand);
        }
        fbm
    }

    /// Construct a valid Timer Equivalence Relation.
    fn make_per(&self) -> TimerEquivRel {
        let mut per = TimerEquivRel::new(&self.active_timers);
        let matchings = self.hyp_matchings.values().map(|(_, m)| m);
        per.add_matchings(matchings).expect(APART_TIMERS_MATCHED);
        per
    }
}

const FS_MAPPED_MULT_BASIS: &str = "Frontier state mapped to multiple basis states";
const FS_MAPPED_MULT_MATCH: &str = "Frontier state mapped to basis state with multiple matchings.";
const APART_TIMERS_MATCHED: &str = "Apart timers matched together when constructing the hypothesis";
