use std::collections::VecDeque;

use derive_more::Constructor;
use itertools::{Either, Itertools};
use lsharp_ru::definitions::mealy::{InputSymbol, OutputSymbol, State};
use rustc_hash::{FxHashMap, FxHashSet};

use crate::matching::Matching;
use crate::teachers::SymInput;
use crate::timed_machine::machine::{TimedMachine, TransInput};
use crate::timed_machine::timers::Update;

/// We need to remake this type alias locally.
type InternalTransInput = TransInput<InputSymbol, u16>;

/// `None` if there is no separating sequence between the Z-MMTs of the hypothesis and
/// the sul. Otherwise we return a symbolic sequence witnessing the difference between
/// the two.
pub fn find_shortest_separating_sequence(
    hypothesis: &TimedMachine,
    sul: &TimedMachine,
) -> Option<Vec<SymInput>> {
    let hypothesis = &hypothesis.zone_mmt();
    let sul = &sul.zone_mmt();

    let hyp_initial = hypothesis.initial_state();
    let sul_initial = sul.initial_state();

    let hyp_active_timers_map = hypothesis.active_timers();
    let sul_active_timers_map = sul.active_timers();

    let basic_inputs = {
        hypothesis
            .all_transitions_at(hyp_initial)
            .keys()
            .filter(|i| i.is_symbol())
            .map(|i| **i)
            .collect_vec()
    };

    let initial_var = IterVars::new(
        hyp_initial,
        sul_initial,
        vec![],
        vec![],
        Matching::default(),
    );
    let mut work_list = VecDeque::from([initial_var]);
    let mut visited_pairs = FxHashSet::<(State, State)>::default();
    while let Some(curr_var) = work_list.pop_front() {
        visited_pairs.insert(curr_var.state_pair());

        // Restrict the matching `m` to the set of active timers in the state pair.
        let m = compare_active_timers(
            hyp_active_timers_map.get(&curr_var.curr_hyp).expect("Safe"),
            sul_active_timers_map.get(&curr_var.curr_sul).expect("Safe"),
            &curr_var.m,
        );
        let m = match m {
            // No extra timers, just return the matching.
            Either::Left(n) => n,
            // Extra timer on one side, we've found a CEX.
            Either::Right(extra_timer) => match extra_timer {
                ExtraTimer::Hyp(hyp_timer) => {
                    let hyp_timeout_seq = seq_to_timeout(&hypothesis, hyp_timer, curr_var.curr_hyp);
                    let mut wit = curr_var.access_hyp.clone();
                    wit.extend(hyp_timeout_seq.into_iter());
                    let witness = hypothesis.to_symbolic_sequence(&wit);
                    return Some(witness);
                }
                ExtraTimer::Sul(sul_timer) => {
                    let sul_timeout_seq = seq_to_timeout(&sul, sul_timer, curr_var.curr_sul);
                    let mut wit = curr_var.access_sul.clone();
                    wit.extend(sul_timeout_seq.into_iter());
                    let witness = sul.to_symbolic_sequence(&wit);
                    return Some(witness);
                }
            },
        };

        if !same_number_of_timeouts(hypothesis, sul, curr_var.curr_hyp, curr_var.curr_sul) {
            // Apart, so we can simply return the sequence leading to the current state.
            unreachable!("Active timer comparison should cover this case.");
        }

        let trans_hyp = hypothesis.all_transitions_at(curr_var.curr_hyp);
        let trans_sul = sul.all_transitions_at(curr_var.curr_sul);
        // We have the same number of timeouts, so we need to look at what we can do here.
        // Now, we're expecting that all the timeouts we have are in the matching `m`.
        // Thus, we continue normally.

        // First, the basic inputs are handled, i.e., the non-timeout transitions.
        for i in &basic_inputs {
            let mut curr_m = m.clone();
            let (hyp_dest, hyp_out, hyp_update) = trans_hyp
                .get(i)
                .expect("Basic input transition missing from hypothesis.");
            let (sul_dest, sul_out, sul_update) = trans_sul
                .get(i)
                .expect("Basic input transition missing from hypothesis.");

            if !same_output(hyp_out, sul_out) {
                let mut access = curr_var.access_sul.clone();
                access.push(*i);
                // Apart, so we can simply return the sequence leading to the current state.
                return Some(sul.to_symbolic_sequence(&access));
            }
            let hyp_dest_active = hyp_active_timers_map.get(hyp_dest).expect("Safe");
            let sul_dest_active = sul_active_timers_map.get(sul_dest).expect("Safe");

            match update_comparison(
                hyp_update,
                sul_update,
                &mut curr_m,
                hyp_dest_active,
                sul_dest_active,
            ) {
                Either::Left((x, y)) => {
                    // We got new matchings to add to the destination pair.
                    let old_pair = curr_m.insert_no_overwrite(x, y);
                    no_overwrite(old_pair, (x, y));
                }
                Either::Right(up_diff) => {
                    if let Some(update_cmp) = up_diff {
                        match update_cmp {
                            UpdateCmp::Sul(sul_timer) => {
                                let sul_timeout_seq = seq_to_timeout(sul, sul_timer, *sul_dest);
                                let mut wit = curr_var.access_sul.clone();
                                wit.push(*i);
                                wit.extend(sul_timeout_seq.into_iter());
                                let witness = sul.to_symbolic_sequence(&wit);
                                return Some(witness);
                            }
                            UpdateCmp::Hyp(hyp_timer) => {
                                let hyp_timeout_seq =
                                    seq_to_timeout(hypothesis, hyp_timer, *hyp_dest);
                                let mut wit = curr_var.access_hyp.clone();
                                wit.push(*i);
                                wit.extend(hyp_timeout_seq.into_iter());
                                let witness = hypothesis.to_symbolic_sequence(&wit);
                                return Some(witness);
                            }
                        }
                    } else {
                        // No updates on either side, we can just go down normally.
                    }
                }
            }
            // Since all the outputs were equal and so were the updates, we can recurse.
            if !visited_pairs.contains(&(*hyp_dest, *sul_dest)) {
                let next_var = {
                    let mut access_sul = curr_var.access_sul.clone();
                    let mut access_hyp = curr_var.access_hyp.clone();
                    access_sul.push(*i);
                    access_hyp.push(*i);
                    IterVars {
                        curr_hyp: *hyp_dest,
                        curr_sul: *sul_dest,
                        access_hyp,
                        access_sul,
                        m: curr_m,
                    }
                };
                work_list.push_back(next_var);
            }
        }

        // Now we need to get the pairs of the matched inputs, which is not difficult at all.
        // Right now, we have a bijection from the set of active timers from the Hyp to the SUL.
        // Thus...
        let timeout_transitions = trans_hyp
            .keys()
            .filter(|i| i.is_time_out())
            .map(|hyp_timeout| {
                let sul_timer = m
                    .get_by_left(&hyp_timeout.unwrap_time_out())
                    .expect("Timer missing from matching.");
                let sul_timeout = TransInput::TimeOut(*sul_timer);
                (**hyp_timeout, sul_timeout)
            })
            .collect_vec();
        for (x_timeout, y_timeout) in timeout_transitions {
            let mut curr_m = m.clone();
            let (hyp_dest, hyp_out, hyp_update) = trans_hyp
                .get(&x_timeout)
                .expect("Timeout transition missing from hypothesis.");
            let (sul_dest, sul_out, sul_update) = trans_sul
                .get(&y_timeout)
                .expect("Timeout transition missing from hypothesis.");

            if !same_output(hyp_out, sul_out) {
                let mut access = curr_var.access_sul.clone();
                access.push(y_timeout);
                // Apart, so we can simply return the sequence leading to the current state.
                return Some(sul.to_symbolic_sequence(&access));
            }

            let hyp_dest_active = hyp_active_timers_map.get(hyp_dest).expect("Safe");
            let sul_dest_active = sul_active_timers_map.get(sul_dest).expect("Safe");
            match update_comparison(
                hyp_update,
                sul_update,
                &mut curr_m,
                hyp_dest_active,
                sul_dest_active,
            ) {
                Either::Left((x, y)) => {
                    // We got new matchings to add to the destination pair.
                    let old_pair = curr_m.insert_no_overwrite(x, y);
                    no_overwrite(old_pair, (x, y));
                }
                Either::Right(up_diff) => {
                    if let Some(update_cmp) = up_diff {
                        match update_cmp {
                            UpdateCmp::Sul(sul_timer) => {
                                let sul_timeout_seq = seq_to_timeout(&sul, sul_timer, *sul_dest);
                                let mut wit = curr_var.access_sul.clone();
                                wit.push(y_timeout);
                                wit.extend(sul_timeout_seq.into_iter());
                                let witness = sul.to_symbolic_sequence(&wit);
                                return Some(witness);
                            }
                            UpdateCmp::Hyp(hyp_timer) => {
                                let hyp_timeout_seq =
                                    seq_to_timeout(&hypothesis, hyp_timer, *hyp_dest);
                                let mut wit = curr_var.access_hyp.clone();
                                wit.push(x_timeout);
                                wit.extend(hyp_timeout_seq.into_iter());
                                let witness = hypothesis.to_symbolic_sequence(&wit);
                                return Some(witness);
                            }
                        }
                    } else {
                        // No updates on either side, we can just go down normally.
                    }
                }
            }
            // Since all the outputs were equal and so were the updates, we can recurse.
            if !visited_pairs.contains(&(*hyp_dest, *sul_dest)) {
                let next_var = {
                    let mut access_sul = curr_var.access_sul.clone();
                    let mut access_hyp = curr_var.access_hyp.clone();
                    access_hyp.push(x_timeout);
                    access_sul.push(y_timeout);
                    IterVars {
                        curr_hyp: *hyp_dest,
                        curr_sul: *sul_dest,
                        access_hyp,
                        access_sul,
                        m: curr_m,
                    }
                };
                work_list.push_back(next_var);
            }
        }
    }

    None
}

#[derive(Debug, Constructor)]
struct IterVars {
    curr_hyp: State,
    curr_sul: State,
    access_hyp: Vec<InternalTransInput>,
    access_sul: Vec<InternalTransInput>,
    m: Matching,
}

impl IterVars {
    fn state_pair(&self) -> (State, State) {
        (self.curr_hyp, self.curr_sul)
    }
}

/// `true` if both transitions have the same output symbol.
fn same_output(hyp_out: &OutputSymbol, sul_out: &OutputSymbol) -> bool {
    sul_out == hyp_out
}

type UpdateCmpResult = Either<(u16, u16), Option<UpdateCmp>>;

/// A `None` result indicates that the hypothesis and the SUL have the same constant update for the same
/// matched timer.
fn update_comparison(
    hyp_update: &Option<Update>,
    sul_update: &Option<Update>,
    m: &mut Matching,
    hyp_dest_active: &FxHashSet<u16>,
    sul_dest_activev: &FxHashSet<u16>,
) -> UpdateCmpResult {
    let dest_m = m
        .iter()
        .filter(|(x, _)| hyp_dest_active.contains(x))
        .filter(|(_, y)| sul_dest_activev.contains(y))
        .map(|(x, y)| (*x, *y))
        .collect();
    *m = dest_m;
    match (hyp_update, sul_update) {
        (None, None) => UpdateCmpResult::Right(None),
        (None, Some(sul_up)) => {
            let y = sul_up.id();
            UpdateCmpResult::Right(Some(UpdateCmp::Sul(*y)))
        }
        (Some(hyp_up), None) => {
            let x = hyp_up.id();
            UpdateCmpResult::Right(Some(UpdateCmp::Hyp(*x)))
        }
        (Some(hyp_up), Some(sul_up)) => {
            let x = *hyp_up.id();
            let y = *sul_up.id();
            if neither_in_matching(m, (x, y)) {
                // Fresh timers have been started, we need to add them to the matching.
                if sul_up.value() == hyp_up.value() {
                    UpdateCmpResult::Left((x, y))
                } else {
                    // Different values of updates, we have a CEX.
                    UpdateCmpResult::Right(Some(UpdateCmp::Sul(y)))
                }
            } else {
                // One of the timers is in the matching already.
                if pair_in_matching(m, (x, y)) {
                    if sul_up.value() == hyp_up.value() {
                        UpdateCmpResult::Left((x, y))
                    } else {
                        UpdateCmpResult::Right(Some(UpdateCmp::Sul(y)))
                    }
                } else if only_left_exists(m, (x, y)) {
                    UpdateCmpResult::Right(Some(UpdateCmp::Sul(y)))
                } else {
                    // Obviously, now only the right exists.
                    UpdateCmpResult::Right(Some(UpdateCmp::Sul(y)))
                }
            }
        }
    }
}

enum UpdateCmp {
    Sul(u16),
    Hyp(u16),
}

/// `true` if `hyp_s` and `sul_t` states have the same number of enabled timers.
fn same_number_of_timeouts(
    hypothesis: &TimedMachine,
    sul: &TimedMachine,
    hyp_s: State,
    sul_t: State,
) -> bool {
    let transitions_hyp = hypothesis.all_transitions_at(hyp_s);
    let transitions_sul = sul.all_transitions_at(sul_t);

    let num_timeouts = |trans: &FxHashMap<_, _>| {
        trans
            .keys()
            .filter(|i: &&&InternalTransInput| i.is_time_out())
            .count()
    };

    num_timeouts(&transitions_hyp) == num_timeouts(&transitions_sul)
}

fn compare_active_timers(
    active_at_hyp: &FxHashSet<u16>,
    active_at_sul: &FxHashSet<u16>,
    m: &Matching,
) -> RestrictedMatching {
    // Note, the matching `m` should have already been restricted to the set of active timers in both the
    // hypothesis and the SUL.
    let restricted_m = {
        let m_iter = m
            .iter()
            .filter(|(x, _)| active_at_hyp.contains(*x))
            .filter(|(_, y)| active_at_sul.contains(*y))
            .map(|(x, y)| (*x, *y));
        Matching::from_iter(m_iter)
    };
    if active_at_hyp.len() != active_at_sul.len() {
        // Number of active timers is different, we need to check which timer is extra.
        if restricted_m.len() < active_at_sul.len() {
            // We're missing a timeout from the SUL.
            let missing_sul_timer = active_at_sul
                .iter()
                .find(|y| restricted_m.get_by_right(y).is_none())
                .expect("Safe");
            return RestrictedMatching::Right(ExtraTimer::Sul(*missing_sul_timer));
        }
        if restricted_m.len() < active_at_hyp.len() {
            // We're missing a timeout from the hypothesis.
            let missing_hyp_timer = active_at_hyp
                .iter()
                .find(|x| restricted_m.get_by_left(x).is_none())
                .expect("Safe");
            return RestrictedMatching::Right(ExtraTimer::Hyp(*missing_hyp_timer));
        }
        unreachable!("Cannot have different number of active timers and not be missing a timeout.")
    }
    // Number of active timers is the same, just return the restricted matching.
    RestrictedMatching::Left(restricted_m)
}

/// A restricted matching also checks whether we have extra timers in the hypothesis or the SUL.
type RestrictedMatching = Either<Matching, ExtraTimer>;

enum ExtraTimer {
    Hyp(u16),
    Sul(u16),
}

fn seq_to_timeout(mmt: &TimedMachine, timer_id: u16, src: State) -> Vec<InternalTransInput> {
    let mut visited_states = FxHashSet::default();

    let timeout_wanted = InternalTransInput::TimeOut(timer_id);
    let acc_seq = vec![];
    let mut work_list = VecDeque::from([(src, acc_seq)]);
    while let Some((curr, access_seq)) = work_list.pop_front() {
        visited_states.insert(curr);
        let curr_trans = mmt.all_transitions_at(curr);

        for (i, (dest, _, update)) in &curr_trans {
            if **i == timeout_wanted {
                let mut access_seq = access_seq;
                access_seq.push(**i);
                return access_seq;
            }

            if let Some(up) = update {
                // If the timer we're looking for gets updated on this path,
                // the (possible, subsequent) timeout cannot be the reason for
                // the timer's inclusion in the set of active timers.
                if *up.id() == timer_id {
                    continue;
                }
            }

            if !visited_states.contains(dest) {
                let mut dest_access = access_seq.clone();
                dest_access.push(**i);
                work_list.push_back((*dest, dest_access));
            }
        }
    }
    unreachable!("We did not manage to find a timeout for an active timer in the MMT.")
}

fn no_overwrite(insertion_result: Result<(), (u16, u16)>, new_pair: (u16, u16)) {
    if let Err(old_pair) = insertion_result {
        if old_pair != new_pair {
            panic!("Tried to overwrite a pair in the matching.")
        }
    }
}

fn only_left_exists(m: &Matching, (x, y): (u16, u16)) -> bool {
    if m.get_by_right(&y).is_none() {
        m.get_by_left(&x).is_some()
    } else {
        false
    }
}

fn neither_in_matching(m: &Matching, (x, y): (u16, u16)) -> bool {
    m.get_by_left(&x).is_none() && m.get_by_right(&y).is_none()
}

fn pair_in_matching(m: &Matching, (x, y): (u16, u16)) -> bool {
    if let Some(z) = m.get_by_left(&x) {
        *z == y
    } else {
        false
    }
}

#[cfg(test)]
mod tests;
