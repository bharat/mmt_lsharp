use std::collections::VecDeque;
use std::fmt::Display;

use derive_more::IsVariant;
use itertools::Itertools;
use lsharp_ru::definitions::mealy::{InputSymbol, State};
use rustc_hash::{FxHashMap, FxHashSet};

use crate::matching::Matching;
use crate::obs_tree::ObservationTree;
use crate::timed_machine::machine::TransInput;

/// For the given pair of states and a matching between them,
/// return a witness (either structural or behavioral), if it exists.
pub fn states_are_apart_via(
    o_tree: &ObservationTree,
    s: State,
    t: State,
    active_timers: &FxHashMap<State, FxHashSet<u16>>,
    m: &Matching,
    enabled_timers_known: &FxHashSet<State>,
) -> Option<Witness> {
    macro_rules! ret_if_exists {
        ($wit:expr) => {
            if ($wit).is_some() {
                return $wit;
            }
        };
    }
    let witness = apart_by_timer_apartness(m, active_timers);
    ret_if_exists!(witness);

    if enabled_timers_known.contains(&s) && enabled_timers_known.contains(&t) {
        // Number of enabled timers different.
        let witness = apart_by_num_enabled_timers(o_tree, s, t);
        ret_if_exists!(witness);
        // Missing timeout.
        let witness = apart_by_missing_timeout(o_tree, s, t, m);
        ret_if_exists!(witness);
    }
    // Differing outputs.
    let witness = apart_by_output(o_tree, s, t, m);
    ret_if_exists!(witness);
    // Differing constants update.
    let witness = apart_by_update(o_tree, s, t, m);
    ret_if_exists!(witness);

    let witness = rec_apart_via(o_tree, s, t, active_timers, m, enabled_timers_known);
    ret_if_exists!(witness);
    None
}

fn rec_apart_via(
    o_tree: &ObservationTree,
    s: State,
    t: State,
    active_timers: &FxHashMap<State, FxHashSet<u16>>,
    matching: &Matching,
    enabled_timers_known: &FxHashSet<State>,
) -> Option<Witness> {
    let s_transitions = &o_tree.all_transitions_at(s);
    let t_transitions = &o_tree.all_transitions_at(t);

    // First, lets handle the "simpler" case of using the basic input symbol apartness.
    let common_inputs = s_transitions
        .keys()
        .copied()
        .filter(|i| i.is_symbol())
        .filter(|i| t_transitions.contains_key(i));

    for i in common_inputs {
        let &(sp, _, _) = s_transitions.get(&i).expect("Safe");
        let &(tp, _, _) = t_transitions.get(&i).expect("Safe");

        let mut rec_matching = matching.clone();
        let old = rec_matching.insert_no_overwrite(sp.raw() as u16, tp.raw() as u16);
        assert!(old.is_ok(), "Created a clashing matching.");

        let rec_witness = states_are_apart_via(
            o_tree,
            sp,
            tp,
            active_timers,
            &rec_matching,
            enabled_timers_known,
        );
        if let Some(rec_witness) = rec_witness {
            let witness = Witness::Recursive(i, matching.clone(), Box::new(rec_witness));
            return Some(witness);
        }
    }

    // In the current matching, which timers in `s_timeouts`
    // are matched against timeouts in `t_timeouts`?
    // We need to pursue back propagation only for those timers.
    // We can ignore the rest, as we will essentially be checking the
    // other matchings in an outer loop.

    let transitions_are_defined = |si, ti| {
        let s_dest = s_transitions.get(&si).map(|(sp, _, _)| sp);
        let t_dest = t_transitions.get(&ti).map(|(tp, _, _)| tp);
        s_dest.zip(t_dest)
    };
    let matching_timeouts = matching
        .iter()
        .map(|(x, y)| (*x, *y))
        .map(|(x, y)| (TransInput::TimeOut(x), TransInput::TimeOut(y)));

    for (to_x, to_y) in matching_timeouts {
        // Now, is there a matching under which the destinations of both timeout
        // inputs are apart?

        // The destination matching must be a flat matching.
        let Some((s_prime, t_prime)) = transitions_are_defined(to_x, to_y) else {
            continue;
        };

        // Given that we have timeout transitions, there won't be any change to the matching.
        let rec_wit = states_are_apart_via(
            o_tree,
            *s_prime,
            *t_prime,
            active_timers,
            matching,
            enabled_timers_known,
        );
        if let Some(rec_wit) = rec_wit {
            let witness = Witness::Recursive(to_x, matching.clone(), Box::new(rec_wit));
            return Some(witness);
        }
    }
    None
}

/// Timer apartness rule.
pub fn apart_by_timer_apartness(
    m: &Matching,
    active_timers: &FxHashMap<State, FxHashSet<u16>>,
) -> Option<Witness> {
    let cands = m.iter().filter(|(x, y)| x != y);
    for (x, y) in cands {
        let apart = active_timers
            .values()
            .any(|timers| timers.contains(x) && timers.contains(y));
        if apart {
            return Some(Witness::Timer(*x, *y, m.clone()));
        }
    }
    None
}

/// Apart if the number of enabled timers in both states are unequal.
fn apart_by_num_enabled_timers(o_tree: &ObservationTree, s: State, t: State) -> Option<Witness> {
    let num_timeouts = |q| {
        o_tree
            .all_transitions_at(q)
            .keys()
            .filter(|i| i.is_time_out())
            .count()
    };
    // s and t are apart if the number of timeouts are different.
    (num_timeouts(s) != num_timeouts(t)).then_some(Witness::NumEnabledTimers)
}

/// Some timeout's matched timeout cannot be found in the other state.
fn apart_by_missing_timeout(
    o_tree: &ObservationTree,
    s: State,
    t: State,
    matching: &Matching,
) -> Option<Witness> {
    let s_trans = o_tree.all_transitions_at(s);
    let t_trans = o_tree.all_transitions_at(t);
    // We want to find a pair of two matched timers where one is defined, but the other is not.
    matching
        .iter()
        .map(|(x, y)| (TransInput::TimeOut(*x), TransInput::TimeOut(*y)))
        .find(|(x, y)| s_trans.contains_key(x) ^ t_trans.contains_key(y))
        .map(|x| Witness::MissingTimeout(x, matching.clone()))
}

/// States `s` and `t` have a differing output to some input symbol.
fn apart_by_output(
    o_tree: &ObservationTree,
    s: State,
    t: State,
    matching: &Matching,
) -> Option<Witness> {
    let s_transitions = &o_tree.all_transitions_at(s);
    let t_transitions = &o_tree.all_transitions_at(t);

    // Get a tuple of the original and the matched *left* symbol/timeout, if it exists.
    let input_pair = |i| {
        let m_i = map_over_matching_from_left(matching, i);
        Some(*i).zip(m_i)
    };

    // We will only consider transitions defined both on `s` and `t`.
    let transitions_are_defined = |p @ (si, ti)| {
        let s_trans = s_transitions.get(&si).map(|(_, o, _)| o);
        let t_trans = t_transitions.get(&ti).map(|(_, o, _)| o);
        Some(p).zip(s_trans.zip(t_trans))
    };

    // We want to iterate over the symbols/timeouts which are defined.
    let inputs_to_check = s_transitions.keys();
    inputs_to_check
        .filter_map(input_pair)
        .filter_map(transitions_are_defined)
        .find(|(_, (so, to))| so != to)
        .map(|((si, _), _)| Witness::Output(si, matching.clone()))
}

/// Provide a witness if states `s` and `t` are apart by update.
fn apart_by_update(
    o_tree: &ObservationTree,
    s: State,
    t: State,
    matching: &Matching,
) -> Option<Witness> {
    let s_transitions = &o_tree.all_transitions_at(s);
    let t_transitions = &o_tree.all_transitions_at(t);

    // Get a tuple of the original and the matched *left* symbol/timeout, if it exists.
    let input_pair = |i| {
        let m_i = map_over_matching_from_left(matching, i);
        Some(*i).zip(m_i)
    };

    // We will only consider transitions defined both on `s` and `t`.
    let transitions_pair = |p @ (si, ti)| {
        let s_trans = s_transitions.get(&si);
        let t_trans = t_transitions.get(&ti);
        Some(p).zip(s_trans.zip(t_trans))
    };

    let inputs_to_check = s_transitions.keys();
    // We want to iterate over the symbols/timeouts which are defined.
    for ((i, _m_i), ((s_dest, _, s_update), (_t_dest, _, t_update))) in inputs_to_check
        .filter_map(input_pair)
        .filter_map(transitions_pair)
    {
        // We don't declare apartness just because there's no update in one or another.
        let updates = Option::zip(s_update.as_ref(), t_update.as_ref());
        if updates.is_some_and(|(x, y)| x.value() != y.value()) {
            // That will be used as a suffix.
            let s_tim = *s_update.as_ref().expect("Safe").id();
            let suffix = seq_to_timeout(o_tree, s_tim, *s_dest);
            // Constant rule triggered.
            let witness = Witness::Constant(i, matching.clone(), suffix);
            return Some(witness);
        }
    }
    None
}

/// A spanning sequence for a timer `timer_id` from state `s` is a sequence ending with
/// a timeout transition for `timer_id`.
///
/// ### Panics
/// If such a sequence is not found.
pub fn spanning_sequence(
    o_tree: &ObservationTree,
    timer_id: u16,
    s: State,
) -> Vec<TransInput<InputSymbol, u16>> {
    seq_to_timeout(o_tree, timer_id, s)
}

/// Given root node `s_dest`, return a sequence ending with timeout of `s_tim`.
///
/// ### Panics
/// If such a sequence is not found.
fn seq_to_timeout(
    o_tree: &ObservationTree,
    s_tim: u16,
    s_dest: State,
) -> Vec<TransInput<InputSymbol, u16>> {
    // So we essentially need to do search for the path leading to a timeout of s_tim here.
    let initial_pair = (s_dest, vec![]);
    let target_timeout = TransInput::TimeOut(s_tim);
    let mut work_list = VecDeque::from([initial_pair]);
    while let Some((s, acc)) = work_list.pop_front() {
        // Now we get all the successors of the current states.
        let s_transitions = o_tree.all_transitions_at(s);
        if s_transitions.get(&target_timeout).is_some() {
            // We got the timeout.
            // So now just return everything back.
            let mut witness = acc;
            witness.push(target_timeout);
            return witness;
        }
        for (i, (s_dest, _, _)) in s_transitions {
            let (i, s_dest) = (*i, *s_dest);
            // If `i` is not equal to the s_tim, keep searching.
            let mut acc_new = acc.clone();
            acc_new.push(i);
            work_list.push_back((s_dest, acc_new));
        }
    }
    unreachable!("Did not find a timeout for the corresponding constants update!");
}

/// Return the right pair of an input symbol, if defined in the matching.
/// This operation is an identity function over basic [`InputSymbol`]s.
fn map_over_matching_from_left(
    m: &Matching,
    i: &TransInput<InputSymbol, u16>,
) -> Option<TransInput<InputSymbol, u16>> {
    match i {
        TransInput::Symbol(i) => Some(TransInput::Symbol(*i)),
        TransInput::TimeOut(x) => m.get_by_left(x).copied().map(TransInput::TimeOut),
    }
}

#[derive(Debug, Clone, IsVariant)]
pub enum Witness {
    /// For the given input symbol, we differ by the output.
    Output(TransInput<InputSymbol, u16>, Matching),
    /// For the given input symbol, we differ by the update.
    Constant(
        TransInput<InputSymbol, u16>,
        Matching,
        Vec<TransInput<InputSymbol, u16>>,
    ),
    /// For the given input symbol, we have a timer defined on the left, but not the right (or vice-versa).
    MissingTimeout(
        (TransInput<InputSymbol, u16>, TransInput<InputSymbol, u16>),
        Matching,
    ),
    /// Timers are apart.
    Timer(u16, u16, Matching),
    /// Number of enabled timers are different.
    NumEnabledTimers,
    /// Recursion rule.
    /// By convention, if the transition input is a timeout, we only store the
    /// timeout of the LHS in the container. The RHS can be obtained from the Matching.
    Recursive(TransInput<InputSymbol, u16>, Matching, Box<Witness>),
}

impl Display for Witness {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut curr_wit = self;
        write!(f, "[")?;
        loop {
            match curr_wit {
                Witness::Output(x, m) => {
                    write!(f, "Out {} under {}", x, m)?;
                    break;
                }
                Witness::Constant(x, m, seq) => {
                    let seq_str = seq.iter().map(|x| x.to_string()).join(",");
                    write!(f, "Const {} under {} with suffix {}", x, m, seq_str)?;
                    break;
                }
                Witness::Recursive(x, m, z) => {
                    curr_wit = z;
                    write!(f, "Rec {} under {} ->,", x, m)?;
                }
                Witness::MissingTimeout(pair, m) => {
                    write!(f, "MTO ({},{}) under {}", pair.0, pair.1, m)?;
                    break;
                }
                Witness::NumEnabledTimers => {
                    write!(f, "NTO")?;
                    break;
                }
                Witness::Timer(x, y, m) => {
                    write!(f, "TA({},{} under {})", *x, *y, m)?;
                    break;
                }
            };
        }
        write!(f, "]")?;
        Ok(())
    }
}

impl Witness {
    pub fn is_timer_witness(&self) -> bool {
        // We want to recursively check if any witness in the witness-chain is a timer witness.
        match self {
            Witness::Timer(..) => true,
            Witness::Recursive(_, _, rec_wit) => rec_wit.as_ref().is_timer_witness(),
            _ => false,
        }
    }
}

const _UNEQUAL_MISSING_TIMEOUTS: &str = "Number of unmatched timeouts is not equal.";
