use bimap::BiBTreeMap;
use delegate::delegate;
use derive_more::{Display, From, IsVariant, Unwrap};
use itertools::Itertools;
use rayon::prelude::{IntoParallelIterator, ParallelIterator};

use core::fmt;

#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Matching {
    inner: BiBTreeMap<u16, u16>,
}

// We have delegated the work of fetching from left and right.
impl Matching {
    delegate! {
        to self.inner{
            pub fn get_by_left(&self, left: &u16) -> Option<&u16>;
            pub fn get_by_right(&self, left: &u16) -> Option<&u16>;

            /// Insert a new match without overwriting anything.
            pub fn insert_no_overwrite(&mut self, left: u16, right: u16) -> Result<(), (u16, u16)>;

        }
    }
}

impl FromIterator<(u16, u16)> for Matching {
    fn from_iter<T: IntoIterator<Item = (u16, u16)>>(iter: T) -> Self {
        let inner = BiBTreeMap::from_iter(iter);
        Self { inner }
    }
}

impl Matching {
    /// Number of matches in the matching.
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    pub fn iter(&self) -> MatchingIterator {
        self.into_iter()
    }
}

/// Iterator over a reference of [`Matching`].
pub struct MatchingIterator<'a> {
    inner: bimap::btree::Iter<'a, u16, u16>,
}

impl<'a> Iterator for MatchingIterator<'a> {
    type Item = (&'a u16, &'a u16);

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next()
    }
}

impl IntoIterator for Matching {
    type Item = (u16, u16);

    type IntoIter = bimap::btree::IntoIter<u16, u16>;

    fn into_iter(self) -> Self::IntoIter {
        self.inner.into_iter()
    }
}

impl<'a> IntoIterator for &'a Matching {
    type Item = (&'a u16, &'a u16);

    type IntoIter = MatchingIterator<'a>;

    fn into_iter(self) -> Self::IntoIter {
        MatchingIterator {
            inner: self.inner.iter(),
        }
    }
}

impl fmt::Display for Matching {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let matches = self
            .iter()
            .map(|(i, j)| format!("(x{},x{})", *i, *j))
            .join(",");
        write!(f, "{{{}}}", matches)
    }
}

/// All possible matchings for a given pair of sets of timers.
pub struct AllMatches {
    inner: Vec<Matching>,
}

impl AllMatches {
    pub fn new<I>(left: I, right: I) -> Self
    where
        I: IntoIterator<Item = u16>,
    {
        let l_timers: Vec<_> = left.into_iter().collect();
        let r_timers: Vec<_> = right.into_iter().collect();
        // If either set of timers is empty, there can only be an empty matching.
        if l_timers.is_empty() || r_timers.is_empty() {
            let inner = vec![Matching::default()];
            return Self { inner };
        }
        let inner = solve_recurrence(l_timers, r_timers);
        let mut inner: Vec<_> = inner
            .into_par_iter()
            .map(|matching| {
                matching
                    .into_iter()
                    .filter(|x| x.is_rel())
                    .map(|x| x.unwrap_rel())
                    .collect_vec()
            })
            .map(Matching::from_iter)
            .collect();
        inner.sort_by_key(|x| x.len());
        Self { inner }
    }

    /// Iterate over all the maximal matchings.
    pub fn maximal_matchings(&self) -> impl Iterator<Item = &Matching> {
        let mut matchings = self.inner.iter().collect_vec();
        if matchings.len() == 1 {
            // We will always have at least one matching: the empty matching.
            return matchings.into_iter();
        }
        matchings.sort_by_key(|x| std::cmp::Reverse(x.len())); // Descending order.
        let max_len = matchings
            .first()
            .map(|x| x.len())
            .expect("There must be at least one matching.");
        let matchings = matchings
            .into_iter()
            .filter(|x| x.len() == max_len)
            .collect_vec();
        matchings.into_iter()
    }
}

/// For the given pair of timers, generate a vector of vector of single matches.
/// Each inner vector will be later combined into a single [`Matching`].
fn solve_recurrence(l_timers: Vec<u16>, r_timers: Vec<u16>) -> Vec<Vec<SingleMatch>> {
    // Early exit if either set of timers is empty.
    if l_timers.is_empty() || r_timers.is_empty() {
        return vec![vec![SingleMatch::default()]];
    }
    let mut matchings;
    // In this case, we do not add a timer from `l_timers` to the matching.
    {
        let rec_l_timers = l_timers[1..].to_vec();
        matchings = solve_recurrence(rec_l_timers, r_timers.clone());
    }
    // Now we want to make a matching between the first element of `l_timers` and
    // each element of `r_timers` and then recurse.
    {
        let (curr_l, rec_l_timers) = l_timers
            .split_first()
            .expect("We should have at least one timer here.");
        let curr_l = *curr_l;
        let rec_l_timers = rec_l_timers.to_vec();
        let mut r_timers = r_timers;
        for _ in 0..r_timers.len() {
            // We need to get curr_r and the rest of the elements of the vector.
            r_timers.rotate_left(1); // We will rotate one element each time.
                                     // In the last loop we will get the original vec.
            let (curr_r, rec_r_timers) = r_timers
                .split_first()
                .expect("We should have at least one timer here.");
            let curr_match = (curr_l, *curr_r);
            let curr_match = curr_match.into();
            let mut rec_matches = solve_recurrence(rec_l_timers.clone(), rec_r_timers.to_vec());
            // For each set of recursive matches, we add the curr_match.
            for rec_match in &mut rec_matches {
                rec_match.push(curr_match);
            }
            // Now just add all the rec_matches to the matchings at this level.
            matchings.append(&mut rec_matches);
        }
    }
    matchings
}

#[derive(
    From, Default, Clone, Copy, Display, PartialEq, Eq, PartialOrd, Ord, IsVariant, Unwrap,
)]
enum SingleMatch {
    /// Either we match nothing, ...
    #[default]
    #[from(ignore)]
    #[display(fmt = "∅ ")]
    Empty,
    /// or else we have a single match.
    #[display(fmt = "(l{},r{})", _0, _1)]
    Rel(u16, u16),
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use super::solve_recurrence;

    #[rstest]
    #[case(vec![4,5], vec![1,2,3])]
    #[case(vec![4,5,6], vec![1,2,3])]
    fn recurrence_test(#[case] l_timers: Vec<u16>, #[case] r_timers: Vec<u16>) {
        let mut matchings = solve_recurrence(l_timers, r_timers);
        matchings.sort();
        for x in matchings {
            for m in x {
                print!("{} ", m);
            }
            println!();
        }
    }
}
